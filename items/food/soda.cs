datablock itemData(CityRPGSodaItem)
{
		category		= "Weapon";
		className		= "Weapon";
		
		shapeFile		= "Add-Ons/GameMode_CityRPG/shapes/bonussoda.dts";
		mass			= 1;
		density 		= 0.2;
		elasticity		= 0.2;
		friction		= 0.6;
	
		uiName			= "Bonus Soda";
		iconName		= "Add-Ons/GameMode_CityRPG/shapes/ItemIcons/pickaxe";
		doColorShift	= false;
	
		image			= CityRPGSodaImage;
		canDrop			= true;
		
		
		// CityRPG Properties
		noSpawn			= true;
};

datablock shapeBaseImageData(CityRPGSodaImage)
{
		shapeFile		= "Add-Ons/GameMode_CityRPG/shapes/bonussoda.dts";
		mountPoint		= 0;
		eyeOffset		= "0.7 1.2 -0.9";
		offset			= "0 0 0";
		correctMuzzleVector = false;
		className		= "WeaponImage";
		
		item			= CityRPGSodaItem;
		ammo			= " ";
		
		melee			= true;
		doRetraction	= false;
		armReady		= true;
		
		doColorShift	= true;
		colorShiftColor = ".54 .27 .07 1";
	
		stateName[0]					= "Activate";
		stateTimeoutValue[0]			= 0.5;
		stateTransitionOnTimeout[0]		= "Ready";
	
		stateName[1]					= "Ready";
		stateTransitionOnTriggerDown[1]	= "PreFire";
		stateAllowImageChange[1]		= true;
	
		stateName[2]					= "PreFire";
		stateScript[2]					= "onPreFire";
		stateAllowImageChange[2]		= false;
		stateTimeoutValue[2]			= 0.1;
		stateTransitionOnTimeout[2]		= "Fire";
	
		stateName[3]					= "Fire";
		stateTransitionOnTimeout[3]		= "CheckFire";
		stateTimeoutValue[3]			= 0.5;
		stateFire[3]					= true;
		stateAllowImageChange[3]		= false;
		stateSequence[3]				= "Fire";
		stateScript[3]					= "onFire";
		stateWaitForTimeout[3]			= true;
	
		stateName[4]					= "CheckFire";
		stateTransitionOnTriggerUp[4]	= "StopFire";
		stateTransitionOnTriggerDown[4]	= "Fire";
	
		stateName[5]					= "StopFire";
		stateTransitionOnTimeout[5]		= "Ready";
		stateTimeoutValue[5]			= 0.2;
		stateAllowImageChange[5]		= false;
		stateWaitForTimeout[5]			= true;
		stateSequence[5]				= "StopFire";
		stateScript[5]					= "onStopFire";
};

function CityRPGSodaImage::onPreFire(%this, %obj, %slot)
{
	%obj.playThread(2, "armAttack");
}

function CityRPGSodaImage::onStopFire(%this, %obj, %slot)
{
	%obj.playThread(2, "root");
}

function CityRPGSodaImage::onFire(%this, %obj, %slot, %col, %pos, %normal)
{
	%client = %obj.client;

	%raycast = containerRayCast(%obj.getEyePoint(),vectorAdd(%obj.getEyePoint(),vectorScale(%obj.getEyeVector(),3)),$TypeMasks::PlayerObjectType,%obj);
	%thing = firstWord(%raycast);
	
	if(isObject(%thing))
	{
		if (%thing.client.getHunger() >= 0)
		{
			parent::onFire(%this, %obj, %slot, %col, %pos, %normal);
			%thing.client.subtractHunger(1);
			messageClient(%thing.client, '', "" @ %client.name @ "fed you his soda.");
			%currSlot = %obj.currTool;
			%obj.tool[%currSlot] = 0;
			%obj.weaponCount--;
			messageClient(%obj.client,'MsgItemPickup','',%currSlot,0);
			serverCmdUnUseTool(%obj.client);
		}
	}


	if (%client.getHunger() >= 0)
	{
		parent::onFire(%this, %obj, %slot, %col, %pos, %normal);
		%client.subtractHunger(1);
		messageClient(%client, '', "You drunk your soda.");
		%currSlot = %obj.currTool;
		%obj.tool[%currSlot] = 0;
		%obj.weaponCount--;
		messageClient(%obj.client,'MsgItemPickup','',%currSlot,0);
		serverCmdUnUseTool(%obj.client);
	}
}