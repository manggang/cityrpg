function CitySO::addTaxMoney(%amount)
{
	%amount = mFloor(%amount);
	if (%amount > 0)
	{
		CitySO.taxMoney += %amount;
		return true;
	}
	return false;
}

function CitySO::subtractTaxMoney(%amount)
{
	%amount = mFloor(%amount);
	if (%amount > 0 && CitySO.taxMoney - %amount > 0)
	{
		CitySO.taxMoney -= %amount;
		return true;
	}
	return false;
}

function CitySO::getSalesTaxAmount(%so, %money)
{
	if (%money < 1)
	{
		return 0;
	}
	
	%salesTax = (%money / 100) * %so.salesTaxPercentage;
	return mFloor(%salesTax);
}

function CitySO::takeSalesTax(%so, %amount)
{
	%salesTax = CitySO.getSalesTaxAmount(%amount);
	CitySO::addTaxMoney(%amount);
	
	return %amount -= %salesTax;
}

function CitySO::loadData(%so)
{
	if(isFile("config/server/CityRPG/CityRPG/City.cs"))
	{
		exec("config/server/CityRPG/CityRPG/City.cs");
		%so.name		= $CityRPG::citydata::name;
		%so.taxMoney = $CityRPG::citydata::taxMoney;
		%so.salesTaxPercentage = $CityRPG::citydata::salesTaxPercentage;
		%so.policeAllotmentPercentage = $CityRPG::citydata::policeAllotmentPercentage;
		%so.governmentWorkerAllotmentPercentage = $CityRPG::citydata::governmentWorkerAllotmentPercentage;
	}
	else
	{
		%so.name = "Townsville";
		%so.taxMoney = 0;
		%so.salesTaxPercentage = 10;
		%so.policeAllotmentPercentage = 5;
		%so.governmentWorkerAllotmentPercentage = 10;
	}
}

function CitySO::saveData(%so)
{
	$CityRPG::citydata::name	= %so.name;
	$CityRPG::citydata::taxMoney		= %so.taxMoney;
	$CityRPG::citydata::salesTaxPercentage = %so.salesTaxPercentage;
	$CityRPG::citydata::policeAllotmentPercentage = %so.policeAllotmentPercentage;
	$CityRPG::citydata::governmentWorkerAllotmentPercentage = %so.governmentWorkerAllotmentPercentage;
	export("$CityRPG::temp::citydata::*", "config/server/CityRPG/CityRPG/City.cs");
}

if(!isObject(CitySO))
{
	new scriptObject(CitySO) { };
	CitySO.loadData();
}

function serverCmdCity(%client)
{
	messageClient(%client, '', "\c6" @ CitySO.name);
	messageClient(%client, '', "\c6Tax Money: " @ mFloor(CitySO.taxMoney));
	messageClient(%client, '', "\c6Sales Tax: \c2" @ mFloor(CitySO.salesTaxPercentage) @ "%");
	messageClient(%client, '', "\c2Budget: ");
	messageClient(%client, '', "\c6Police Allotment: " @ mFloor(CitySO.policeAllotmentPercentage) @ "%");
	messageClient(%client, '', "\c6Government Worker Allotment: " @ CitySO.governmentWorkerAllotmentPercentage @ "%");
}