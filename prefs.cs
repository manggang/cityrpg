// ============================================================
// Section 1 : Random Preferences
// ============================================================
$CityRPG::pref::giveDefaultTools = true;
$CityRPG::pref::defaultTools = "hammerItem wrenchItem printGun";

$CityRPG::pref::moneyDieTime = 60000;
$CityRPG::pref::players::startingCash = 50;
// ============================================================
// Section 2 : Price Preferences
// ============================================================
$CityRPG::prices::vehicleSpawn = 2500;
$CityRPG::prices::reset = 500;
$CityRPG::prices::resourcePrice = 1;
// ============================================================
// Section 3 : Tick Preferences
// ============================================================
$CityRPG::tick::interest = 1.05;
$CityRPG::tick::creditInterest = 1.055;
$CityRPG::tick::interestTick = 12;
$CityRPG::tick::speed = 3;
$CityRPG::tick::promotionLevel = 24;
// ============================================================
// Section 4 : Vehicle Preferences
// ============================================================
$CityRPG::vehicles::allowSpawn = true;

$CityRPG::vehicles::banned[0] = "FlyingWheeledJeepVehicle";
$CityRPG::vehicles::banned[1] = "MiniJetVehicle";
$CityRPG::vehicles::banned[2] = "StuntPlaneVehicle";
$CityRPG::vehicles::banned[3] = "BiplaneVehicle";
$CityRPG::vehicles::banned[4] = "MagicCarpetVehicle";
$CityRPG::vehicles::banned[5] = "TankVehicle";
$CityRPG::vehicles::banned[6] = "horseArmor";
$CityRPG::vehicles::banned[7] = "BlackhawkVehicle";