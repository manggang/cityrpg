
function gameConnection::setGameBottomPrint(%client)
{
	// Font
	%mainFont = "<font:Comic Sans MS:21>";
	%client.CityRPGPrint = %mainFont;
	
	// Job
	if(%client.getDaysLeftInJail())
		%jobName = "Convict";
	else if(%client.isStudent())
		%jobName = "Student" SPC %client.getJobSO().name;
	else
		%jobName = %client.getJobSO().name;
	
	%client.CityRPGPrint = %client.CityRPGPrint @ "\c6Job: \c3" @ %jobName;
	
	
	if(%client.brickGroup.totaldrugs > 0)
		%client.CityRPGPrint = %client.CityRPGPrint SPC "\c6Drugs: \c3" @ %client.brickGroup.totaldrugs;

	// Money
	%client.CityRPGPrint = %client.CityRPGPrint SPC "\c6Money:" SPC %client.getCashString();
			
	%hexColor = $CityRPG::hunger::color[%client.getHunger()];
	%hungerName = $CityRPG::hunger::state[%client.getHunger()];
	%client.CityRPGPrint = %client.CityRPGPrint SPC "\c6Hunger: <color:" @ %hexColor @ ">" @ %hungerName;
	
	// Salary
	if(%client.getSalary() > 0)
		%client.CityRPGPrint = %client.CityRPGPrint SPC "\c6Income: \c3$" @ %client.getSalary();
	
	// Taxes
	if(%client.brickGroup.taxes > 0)
		%client.CityRPGPrint = %client.CityRPGPrint SPC "\c6Taxes: \c3$" @ %client.brickGroup.taxes;
	
	// Resources
	if(CityRPGData.getData(%client.bl_id).lumber > 0 || CityRPGData.getData(%client.bl_id).ore > 0 || CityRPGData.getData(%client.bl_id).fish > 0)
	{
		%client.CityRPGPrint = %client.CityRPGPrint SPC "\c6Lumber: \c3" @ CityRPGData.getData(%client.bl_id).lumber;
		%client.CityRPGPrint = %client.CityRPGPrint SPC "\c6Ore: \c3" @ CityRPGData.getData(%client.bl_id).ore;
		%client.CityRPGPrint = %client.CityRPGPrint SPC "\c6Fish: \c3" @ CityRPGData.getData(%client.bl_id).fish;
	}
	
	// Evidence
	if(%client.brickGroup.evidence > 0)
		%client.CityRPGPrint = %client.CityRPGPrint SPC "\c6Evidence: \c3" @ %client.brickGroup.evidence;
	
	// Wanted Level
	if(CityRPGData.getData(%client.bl_id).valueDemerits >= $CityRPG::pref::demerits::wantedLevel)
	{
		%client.CityRPGPrint = %client.CityRPGPrint SPC "<color:ffffff>Wanted:";
		%stars = %client.getWantedLevel();
		%client.CityRPGPrint = %client.CityRPGPrint SPC "<color:ffff00>";
		
		for(%a = 0; %a < %stars; %a++)
			%client.CityRPGPrint = %client.CityRPGPrint @ "*";
		
		%client.CityRPGPrint = %client.CityRPGPrint @ "<color:888888>";
		for(%a = %a; %a < 6; %a++)
			%client.CityRPGPrint = %client.CityRPGPrint @ "*";
		
		%client.CityRPGPrint = %client.CityRPGPrint @ %mainFont;
	}
	
	// Lot Info
	if(isObject(%client.CityRPGLotBrick))
		%client.CityRPGPrint = %client.CityRPGPrint SPC "<just:right><font:palatino linotype:16><color:fff000>" @ getBrickGroupFromObject(%client.CityRPGLotBrick).name;	
	
	// Printing
	commandToClient(%client, 'bottomPrint', %client.CityRPGPrint, 0, true);
	
	return %client.CityRPGPrint;
}

function gameConnection::updateHUD(%client)
{
	if(isObject(%client.player))
	{		
		%client.setGameBottomPrint();
	}
}

function serverCmdWelcomeBox(%client)
{
	%message = "Check out these webpages:" NL "<a:cityrpg.site50.net>CityRPG Changelog</a>" NL " <a:cityrpg.site50.net\help.php>CityRPG First-Time Guide</a>" NL "" NL "Please do not place Gun-vending-Machines or mediocre food stands.";
	
	commandToClient(%client, 'messageBoxOK', "Welcome Message", %message);
}

function gameConnection::MessageBoxOK(%client, %header, %text)
{
	commandToClient(%client, 'MessageBoxOK', %header, %text);
} 