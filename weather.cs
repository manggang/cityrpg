// ============================================================
// Project				:	CityRPG
// Author				:	Iban
// Description			:	Weather
// ============================================================
// Table of Contents
// 1. WeatherBoxes and Misc. Forced Downloads
// 2. Functions
// ============================================================

// ============================================================
// Section 1 : WeatherBoxes and Misc. Forced Downloads
// ============================================================

// Rain
if(!isObject(CityRPG_Rain))
{
	datablock PrecipitationData(CityRPG_Rain)
	{
	   soundProfile = "RainSound";
	   dropTexture = "Add-Ons/GameMode_CityRPG/shapes/Skies/Rain/droplet";
	   splashTexture = "Add-Ons/GameMode_CityRPG/shapes/Skies/Rain/splash";
	   dropSize = 0.50;
	   splashSize = 0.2;
	   useTrueBillboards = false;
	   splashMS = 250;
	};
}

if(!isObject(CityRPG_Rain_Texture))
{
	datablock decalData(CityRPG_Rain_Texture)
	{
		textureName = "Add-Ons/GameMode_CityRPG/shapes/Skies/Rain/droplet";
		preload = true;
	};
}

if(!isObject(CityRPG_Splash_Texture))
{
	datablock decalData(CityRPG_Splash_Texture)
	{
		textureName = "Add-Ons/GameMode_CityRPG/shapes/Skies/Rain/splash";
		preload = true;
	};
}

return;

// WeatherBoxes
if(!isObject(WeatherBox_Clear))
{
	new InteriorInstance(WeatherBox_Clear)
	{
		position = "0 0 0";
		rotation = "1 0 0 0";
		scale = "1 1 1";
		interiorFile = "Add-Ons/GameMode_CityRPG/shapes/Skies/Clear/Sky_ClearSky.dif";
		useGLLighting = "0";
		showTerrainInside = "0";
	};
}

function WeatherSO::loadForecast(%so)
{
	// Rain Probability
	%so.rainProb[0] = 5; //"January";
	%so.rainProb[1] = 10; //"February";
	%so.rainProb[2] = 15; //"March";
	%so.rainProb[3] = 20; //"April";
	%so.rainProb[4] = 30; //"May";
	%so.rainProb[5] = 40; //"June";
	%so.rainProb[6] = 50; //"July";
	%so.rainProb[7] = 40; //"August";
	%so.rainProb[8] = 30; //"September";
	%so.rainProb[9] = 20; //"October";
	%so.rainProb[10] = 15; //"November";
	%so.rainProb[11] = 10; //"December";
	
	// Snow Probability
	%so.snowProb[0] = 30; //"January";
	%so.snowProb[1] = 15; //"February";
	%so.snowProb[2] = 5; //"March";
	%so.snowProb[3] = 0; //"April";
	%so.snowProb[4] = 0; //"May";
	%so.snowProb[5] = 0; //"June";
	%so.snowProb[6] = 0; //"July";
	%so.snowProb[7] = 0; //"August";
	%so.snowProb[8] = 0; //"September";
	%so.snowProb[9] = 5; //"October";
	%so.snowProb[10] = 15; //"November";
	%so.snowProb[11] = 30; //"December";
}

function WeatherSO::todaysForecast(%so)
{
	%month = CalendarSO.getMonth();
	
	%snowProb = %so.snowProb[%month];
	%rainProb = %so.rainProb[%month];
	%clearProb = 100 - (%so.snowProb[%month] + %so.rainProb[%month]);
	
	%snowPow = getRandom(0, %snowProb);
	%rainPow = getRandom(0, %rainProb);
	if(%clearProb > 0)
		%clearPow = getRandom(0, %clearProb);
	
	if(%snowPow > %rainPow && %snowPow > %clearPow)
		%forecast = "snow";
	else if(%rainPow > %snowPow && %rainPow > %clearPow)
		%forecast = "rain";
	else
		%forecast = "";
	
	if(%forecast !$= "")
	{
		%intensity = getRandom(0, 15);
		
		if(%forecast $= %so.currWeather)
			%verb = "remains";
		else
		{
			%so.currWeather = %forecast;
			%verb = "is";
		}
		
		%intensity = getRandom(5, 15);
		
		switch$(%forecast)
		{
			case "snow":
				%so.generateWeather("snow", %intensity);
				messageAll('', '\c6 - Today\'s forecast %1 Snowy.', %verb);
			case "rain":
				%so.generateWeather("rain", %intensity);
				messageAll('', '\c6 - Today\'s forecast %1 Rainy.', %verb);
			default:
				%so.generateWeather("", 0);
				messageAll('', '\c6 - Today\'s forecast %1 \c0INVALID\c6.', %verb);
		}
	}
	else
	{
		messageAll('', "\c6 - Today's forecast is Clear.");
		%so.generateWeather("", 0);
	}
}

function WeatherSO::generateWeather(%so, %weather, %intensity)
{
	warn("WEATHER ADJUSTING TO:" SPC "[" @ %weather @ "] WITH INTENSITY OF [" @ %intensity @ "]");
	
	if(isObject(Precipitation))
		Precipitation.delete();
	
	$CityRPG_Temp::CurrWeather = %weather;
	
	if(%weather !$= "" && (%intensity = mFloor(%intensity)) > 0)
	{		
		switch$(%weather)
		{
			case "snow":
				%db = "SnowA";
				%mutliplier = 0.15;
			case "rain":
				%db = "CityRPG_Rain";
				%mutliplier = 0.5;
			default:
				%db = "SnowA";
				%mutliplier = 0.15;
		}
		
		%intensity = %intensity * %mutliplier;
		// Default Properties
		%numDrops = 500 * %intensity;
		%minSpeed = 0.5 * %intensity;
		%maxSpeed = 1 * %intensity;
		%minMass = 0.75 * %intensity;
		%maxMass = 0.85 * %intensity;
		%maxTurbulence = 0.1 * %intensity;
		%turbulenceSpeed = 0.5 * %intensity;
		
		new Precipitation(Precipitation)
		{
			dataBlock = %db;
			numDrops = %numDrops;
			
			position = "191.844 621.068 566.484";
			rotation = "1 0 0 0";
			scale = "1 1 1";
			doCollision = "1";
			
			boxWidth = "100";
			boxHeight = "100";
			
			minSpeed = %minSpeed;
			maxSpeed = %maxSpeed;
			
			minMass = %minMass;
			maxMass = %maxMass;
			
			useTurbulence = "false";
			maxTurbulence = %maxTurbulence;
			turbulenceSpeed = %turbulenceSpeed;
			rotateWithCamVel = "false";
		};
		
		Sky.setName("Sky_Old");
		switch$(%weather)
		{
			case "rain":
				new Sky(Sky)
				{
					materialList = "Add-Ons/GameMode_CityRPG/shapes/Skies/Rain/resource.dml";
					position = "336 136 0";
					rotation = "1 0 0 0";
					scale = "1 1 1";
					cloudHeightPer[0] = "0.6";
					cloudHeightPer[1] = "0.3";
					cloudHeightPer[2] = "0.4";
					cloudSpeed1 = "0.003";
					cloudSpeed2 = "0.001";
					cloudSpeed3 = "0.0001";
					visibleDistance = "140";
					fogDistance = "40";
					fogColor = "0.250000 0.250000 0.300000 1.000000";
					fogStorm1 = "0";
					fogStorm2 = "0";
					fogStorm3 = "0";
					fogVolume1 = "0 0 0";
					fogVolume2 = "0 0 0";
					fogVolume3 = "0 0 0";
					fogVolumeColor1 = "128.000000 128.000000 128.000000 -222768174765569860000000000000000000000.000000";
					fogVolumeColor2 = "128.000000 128.000000 128.000000 0.000000";
					fogVolumeColor3 = "128.000000 128.000000 128.000000 -170698929442160050000000000000000000000.000000";
					windVelocity = "0.5 0 0";
					windEffectPrecipitation = "1";
					SkySolidColor = "0.000000 0.000000 0.100000 1.000000";
					useSkyTextures = "1";
					renderBottomTexture = "0";
					noRenderBans = "0";
					locked = "true";
				};
			case "snow":
				new Sky(Sky)
				{
					position = "312 96 0";
					rotation = "1 0 0 0";
					scale = "1 1 1";
					materialList = "base/data/skies/Sky_Spooky3/resource.dml";
					cloudHeightPer[0] = "0.349971";
					cloudHeightPer[1] = "0.3";
					cloudHeightPer[2] = "0.199973";
					cloudSpeed1 = "0.0005";
					cloudSpeed2 = "0.001";
					cloudSpeed3 = "0.0003";
					visibleDistance = "1500";
					fogDistance = "1000";
					fogColor = "0.900000 0.900000 0.900000 1.000000";
					fogStorm1 = "0";
					fogStorm2 = "0";
					fogStorm3 = "0";
					fogVolume1 = "0 0 0";
					fogVolume2 = "0 0 0";
					fogVolume3 = "0 0 0";
					fogVolumeColor1 = "0.000000 0.000000 0.000000 1.000000";
					fogVolumeColor2 = "0.000000 0.000000 0.000000 1.000000";
					fogVolumeColor3 = "0.000000 0.000000 0.000000 1.000000";
					windVelocity = "0.25 0.25 0";
					windEffectPrecipitation = "1";
					SkySolidColor = "0.600000 0.600000 0.600000 1.000000";
					useSkyTextures = "1";
					renderBottomTexture = "0";
					noRenderBans = "0";
				};
			
		}
		Sky_Old.delete();
	}
	else
	{
		Sky.setName("Sky_Old");
		new Sky(Sky)
		{
			position = "336 136 0";
			rotation = "1 0 0 0";
			scale = "1 1 1";
			materialList = "Add-Ons/GameMode_CityRPG/shapes/Skies/Clear/resource.dml";
			cloudHeightPer[0] = "0.349971";
			cloudHeightPer[1] = "0.3";
			cloudHeightPer[2] = "0.199973";
			cloudSpeed1 = "0.0005";
			cloudSpeed2 = "0.001";
			cloudSpeed3 = "0.0003";
			visibleDistance = "5000";
			fogDistance = "800";
			fogColor = "0.900000 0.900000 1.000000 1.000000";
			fogStorm1 = "0";
			fogStorm2 = "0";
			fogStorm3 = "0";
			fogVolume1 = "0 0 0";
			fogVolume2 = "0 0 0";
			fogVolume3 = "0 0 0";
			fogVolumeColor1 = "128.000000 128.000000 128.000000 -222768174765569860000000000000000000000.000000";
			fogVolumeColor2 = "128.000000 128.000000 128.000000 0.000000";
			fogVolumeColor3 = "128.000000 128.000000 128.000000 -170698929442160050000000000000000000000.000000";
			windVelocity = "0 0 0";
			windEffectPrecipitation = "0";
			SkySolidColor = "0.000000 0.000000 0.100000 1.000000";
			useSkyTextures = "1";
			renderBottomTexture = "0";
			noRenderBans = "0";
			locked = "true";
		};	
		Sky_Old.delete();		
	}
}

if(!isObject(WeatherSO))
{
	new scriptObject(WeatherSO) { };
	WeatherSO.loadForecast();
}