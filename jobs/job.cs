$jobCount = 0;

%baseJob = new ScriptObject(Job)
{
	name = "";
	category = "civilian";
	initialInvestment = 0;
	pay = 0;
	tools = "";
	requiredEducation = 0;
	datablock = PlayerCityArmor;
	outfit = "";
	maxPlayers = -1;
	
	canSellItems = false;
	canSellFood = false;
	canSellServices = false;
	canSellClothes = false;
	canSellGuns = false;
	
	canPardon	= false;

	hideJobName = false;
	
	canOfferBounties = false;
	canClaimBounties = false;
	
	tmHexColor	= "0000CC";
	description = "A generic job.";
};

function populateJobs()
{
	for(%a = 1; isObject($jobs[%a]); %a++)
	{
		$jobs[%a].delete();
		$jobs[%a] = "";
	}
	
	// NOTE: Order is incredibly important. Jobs are referenced by ID, which is determined by order.
	// Mixing up the order of these professions will cause save data to reference the wrong job.
	addJobFromFile("civilian");
	addJobFromFile("laborer");
	addJobFromFile("grocer");
	addJobFromFile("armsdealer");
	addJobFromFile("tailor");
	addJobFromFile("shopowner");
	addJobFromFile("medic");
	addJobFromFile("ceo");
	addJobFromFile("policeman");
	addJobFromFile("policechief");
	addJobFromFile("bountyhunter");
	addJobFromFile("councilmember");
	addJobFromFile("mafiamember");
	addJobFromFile("mafiaboss");
	addJobFromFile("hitman");
}

function addJobFromFile(%file)
{
	if(isFile("./" @ %file @ ".cs"))
	{
		exec("./" @ %file @ ".cs");
		
		if(!isObject("CityRPGJob" @ $jobCount @ "SpawnBrickData"))
		{
			datablock fxDtsBrickData(CityRPGSpawnBrickData : brickSpawnPointData)
			{
				category = "CityRPG";
				subCategory = "CityRPG Spawns";
				
				uiName = $jobs[$jobCount].name SPC "Spawn";
				
				specialBrickType = "";
				
				CityRPGBrickType = 3;
				CityRPGBrickAdmin = true;
				
				spawnData = "jobSpawn" SPC $jobCount;
			};
			
			CityRPGSpawnBrickData.setName("CityRPGJob" @ $jobCount @ "SpawnBrickData");
		}
	}
}

populateJobs();

function getJobAlias(%so)
{
	%jobCount = 0;
	
	for(%a = 1; isObject($jobs[%a]); %a++)
	{
		if(!$jobs[%a].hideJobName)
		{
			%jobCount++;
			%jobName[%jobCount] = $jobs[%a].name;
		}
	}
	
	if(%jobCount)
		return %jobName[getRandom(1, %jobCount)];
}

function getNumberOfPlayersWithJob(%name)
{
	%number = 0;
	
	%clientCount = ClientGroup.getCount();
	for (%i = 0; %i < %clientCount; %i++)
	{
		if (ClientGroup.getObject(%i).getJobSO().name $= %name)
		{
			%number++;
		}
	}

	return %number;
}

function getNumberOfPlayersInJobCategory(%name)
{
	%number = 0;
	
	%clientCount = ClientGroup.getCount();
	for (%i = 0; %i < %clientCount; %i++)
	{
		if (ClientGroup.getObject(%i).getJobSO().category $= %name)
		{
			%number++;
		}
	}

	return number;
}

function serverCmdjob(%client, %job, %job2, %job3, %job4, %job5) { serverCmdjobs(%client, %job, %job2, %job3, %job4, %job5); }

function serverCmdjobs(%client, %job, %job2, %job3, %job4, %job5)
{
	if(%job $= "")
	{
		messageClient(%client, '', "\c6Type \c3/help list jobs\c6 to see a list of jobs.");
	}
	
	// Concat Job Words
	%job = %job @ (%job2 !$= "" ? " " @ %job2 @ (%job3 !$= "" ? " " @ %job3 @ (%job4 !$= "" ? " " @ %job4 @ (%job5 !$= "" ? " " @ %job5 : "") : "") : "") : "");
		
	%wantedJobIndex = -1;
	for(%currentJobIndex = 1; %currentJobIndex <= $jobCount; %currentJobIndex++)
	{
		if(strlwr(%job) $= strLwr($jobs[%currentJobIndex].name))
		{
			%wantedJobIndex = %currentJobIndex;
		}
	}
	
	if(%wantedJobIndex == -1)
	{
		messageClient(%client, '', "\c6No such job as \c3" @ %job @ "\c6. Type \c3/help list jobs\c6 to see a list of the jobs.");
		return;
	}
				
	if(%wantedJobIndex == CityRPGData.getData(%client.bl_id).valueJobID)
	{
		messageClient(%client, '', "\c6You are already" SPC CityRPG_DetectVowel($jobs[%wantedJobIndex].name) SPC "\c3" @ $jobs[%wantedJobIndex].name @ "\c6!");
		return;
	}

	if($jobs[%wantedJobIndex].law && !%client.hasCleanCriminalRecord())
	{
		messageClient(%client, '', "\c6You don't have a clean criminal record. You can't become" SPC CityRPG_DetectVowel($jobs[%wantedJobIndex].name) SPC "\c3" @ $jobs[%wantedJobIndex].name @ "\c6.");
		return;
	}

	if(%client.getEducationLevel() < $jobs[%wantedJobIndex].requiredEducation)
	{
		messageClient(%client, '', "\c6You are not educated enough to get this job.");
		return;
	}

	if(%client.getMoney() < $jobs[%wantedJobIndex].initialInvestment)
	{
		messageClient(%client, '', "\c6It costs \c3$" @ $jobs[%wantedJobIndex].initialInvestment SPC "\c6to become" SPC CityRPG_DetectVowel($jobs[%wantedJobIndex].name) SPC $jobs[%wantedJobIndex].name @ "\c6.");
		return;
	}
	
	if (getNumberOfPlayersWithJob($jobs[%wantedJobIndex].name) >= $jobs[%wantedJobIndex].maxPlayers)
	{
		messageClient(%client, '', "\c6There are too many players with this job already!");
		return;
	}

	CityRPG_OnGetJob(%client, %wantedJobIndex);
}

function CityRPG_OnGetJob(%client, %jobIndex)
{
	messageClient(%client, '', "\c6You have made your own initiative to become" SPC CityRPG_DetectVowel($jobs[%jobIndex].name) SPC "\c3" @ $jobs[%jobIndex].name @ "\c6.");
	%client.subtractMoney($jobs[%jobIndex].initialInvestment);
	CityRPGData.getData(%client.bl_id).valueJobID = %jobIndex;
	CityRPGData.getData(%client.bl_id).valueJobEXP = 0;
				
	if(isObject(%client.player))
	{
		%client.player.giveDefaultEquipment();
		%client.applyForcedBodyColors();
		%client.applyForcedBodyParts();
	}
			
	%client.updateHUD();
}

function CityRPG_OnLoseJob(%client)
{
	if (isObject(%client.player))
	{	
		%client.player.giveDefaultEquipment();
	}
	%client.updateHUD();
}

// Get Functions

function gameConnection::getJobSO(%client)
{
	return $jobs[%client.getJobID()];
}

function gameConnection::getJobID(%client)
{
	return CityRPGData.getData(%client.bl_id).valueJobID;	
}

function gameConnection::getSalary(%client)
{
	return mFloor(%client.getJobSO().pay * (((%client.getEducationLevel() - %client.getJobSO().requiredEducation) / 8) + 1));
}

function messageAllOfJob(%job, %type, %message)
{
	%clientCount = ClientGroup.getCount();
	for(%a = 0; %a < %clientCount; %a++)
	{
		%subClient = ClientGroup.getObject(%a);
		if(%subClient.getJobSO().id == %job)
		{
			messageClient(%subClient, %type, %message);
			%sent++;
		}
	}
	
	return (%sent !$= "" ? %sent : 0);
}