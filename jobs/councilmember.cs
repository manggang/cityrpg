$jobs[$jobCount++] = new ScriptObject(CouncilMember : Job)
{
	name = "Council Member";
	category = "government";
	tools = "CityRPGBatonItem";
	datablock = PlayerCityArmor;
	requiredEducation = 6;

	canOfferBounties = true;

	tmHexColor = "0000CC";
	description = "\c6Council Members help manage the city and are paid using tax money.";

	outfit = "keep keep keep keep blueshirt blueshirt keep bluePants blackshoes default Mod-Suit";
};