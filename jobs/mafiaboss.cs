$jobs[$jobCount++] = new ScriptObject(MafiaBoss : Job)
{
	name = "Mafia Boss";
	category = "criminal";
	initialInvestment = 75;
	requiredEducation = 4;
	maxPlayers = 1;

	canSellItems = true;
	canSellGuns = true;

	hideJobName = true;

	tmHexColor = "FF0000";
	description = "\c6The mafia boss controls all mafia members and makes profits based on their accomplishments.";

	outfit = "keep keep keep keep blackshirt blackshirt skin blackPants blackshoes default hoodie";
};