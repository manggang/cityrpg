$jobs[$jobCount++] = new ScriptObject(CouncilMember : Job)
{
	name = "Arms Dealer";
	initialInvestment = 150;
	pay = 30;
	requiredEducation = 2;

	canSellGuns = true;

	tmHexColor = "AAAAAA";
	description = "\c6Arms Dealers can sell items (specifically weapons) to other people.";
};