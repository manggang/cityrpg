$jobs[$jobCount++] = new ScriptObject(Tailor : Job)
{
	name = "Tailor";
	initialInvestment = 25;
	pay = 25;
	requiredEducation = 1;

	canSellClothes = true;

	tmHexColor = "CCCCCC";
	description = "\c6Tailors weave clothes for people to wear.";
};