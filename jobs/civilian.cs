$jobs[$jobCount++] = new ScriptObject(Civilian : Job)
{
	name = "Civilian";
	pay = 25;

	tmHexColor = "CCCCCC";
	description = "\c6Civilians are the default 'profession'. They have no perks and collect welfare.";
};