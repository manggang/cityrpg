$jobs[$jobCount++] = new ScriptObject(Grocer : Job)
{
	name = "Grocer";
	initialInvestment = 25;
	pay = 25;
	requiredEducation = 1;

	canSellFood = true;

	tmHexColor = "00FF00";
	description = "\c6Grocers can sell food to other people.";
};