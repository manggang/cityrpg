$jobs[$jobCount++] = new ScriptObject(CEO : Job)
{
	name = "CEO";
	initialInvestment = 150;
	pay = 175;
	tools = "GunItem";
	requiredEducation = 6;

	canSellItems = true;
	canSellFood = true;
	canSellServices = true;
	canSellClothes = true;
	canSellGuns = true;

	tmHexColor = "0066CC";
	description = "\c6CEOs can sell food and items to other people, and spawn with a gun, because they are badass.";
};