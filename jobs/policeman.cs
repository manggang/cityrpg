$jobs[$jobCount++] = new ScriptObject(Policeman : Job)
{
	name = "Policeman";
	category = "police";
	initialInvestment = 50;
	tools = "CityRPGBatonItem GunItem";
	requiredEducation = 2;

	canClaimBounties = true;

	tmHexColor = "99FF00";
	description = "\c6Police can arrest criminals, kill wanted men, and bust down doors, but require a clean record and only get paid if the city has money.";

	outfit = "none copHat none none copShirt copShirt skin blackPants blackShoes default Mod-Police";
};