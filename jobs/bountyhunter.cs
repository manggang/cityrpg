$jobs[$jobCount++] = new ScriptObject(BountyHunter : Job)
{
	name = "Bounty Hunter";
	category = "bountyhunter";
	initialInvestment = 50;
	pay = 25;
	tools = "CityRPGBatonItem";
	requiredEducation = 1;

	canClaimBounties = true;

	tmHexColor = "FF7F00";
	description = "\c6Bounty Hunters can arrest criminals and claim bounties, but don't need a good record.";
};