$jobs[$jobCount++] = new ScriptObject(Medic : Job)
{
	name = "Medic";
	initialInvestment = 50;
	pay = 100;
	tools = "CityRPGDefibrillatorItem";
	requiredEducation = 4;

	tmHexColor = "99FF00";
	description = "\c6Medics can revive players that are bleeding out, see players' health at a distance and heal people.";

	outfit = "none none none none blueShirt blueShirt skin blackPants blackShoes default Mod-Suit";
};