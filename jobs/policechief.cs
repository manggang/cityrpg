$jobs[$jobCount++] = new ScriptObject(PoliceChief : Job)
{
	name = "Police Chief";
	category = "police";
	initialInvestment = 100;
	tools = "CityRPGBatonItem GunItem";
	requiredEducation = 4;
	maxPlayers = 1;

	canOfferBounties = true;
	canClaimBounties = true;
	canPardon = true;

	tmHexColor = "99FF00";
	description = "\c6Police Chiefs can do everything policemen can, and additionally pardon criminals and place/claim bounties.";

	outfit = "none copHat none none copShirt copShirt skin blackPants blackShoes default Mod-Police";
};