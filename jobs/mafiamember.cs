$jobs[$jobCount++] = new ScriptObject(MafiaMember : Job)
{
	name = "Mafia Member";
	category = "criminal";
	initialInvestment = 25;
	pay = 45;
	tools = "CityRPGPicklockItem";
	requiredEducation = 2;

	hideJobName = true;

	tmHexColor = "FF0000";
	description = "\c6Criminals can pickpocket people and break open doors.";

	outfit = "keep keep keep keep blackshirt blackshirt skin blackPants blackshoes default hoodie";
};