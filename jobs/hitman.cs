$jobs[$jobCount++] = new ScriptObject(Hitman : Job)
{
	name = "Hitman";
	category = "gangster";
	initialInvestment = 100;
	requiredEducation = 6;

	hideJobName = true;

	tmHexColor = "FF0000";
	description = "\c6Hitmen receive no pay but can make money by completing randomly assigned hits on other players.";

	outfit = "keep keep keep keep blueshirt blueshirt skin Bluejeans blueshoes default Mod-Suit";
};

function checkHitTimeout()
{
	%clientCount = ClientGroup.getCount();
	for(%i = 0;%i < %clientCount;%i++)
	{
		%currentClient = ClientGroup.getObject(%i);
		if(%currentClient.getJobSO().name $= "Hitman")
		{
			if (getSimTime() - %currentClient.currentHitTimeStarted >= 60000 * %currentClient.currentHitTimeInMinutes)
			{
				%currentClient.onAssassinationFailure();
			}
		}
	}
}

function gameConnection::assignRandomHit(%client)
{
	%clientCount = ClientGroup.getCount();
	if (%clientCount > 2)
	{
		%client.assassinationTarget = ClientGroup.getObject(getRandom(0, %clientCount));
		while (%client.assassinationTarget == %client)
		{
			%client.assassinationTarget = ClientGroup.getObject(getRandom(0, %clientCount));
		}		
	}
	else
	{
		messageClient(%client, '', "\c6There aren't any targets available!");
		return;
	}
	
	%client.currentHitValue = mFloor(getRandom(200, 2000));
	%client.currentHitTimeInMinutes = mFloor(getRandom(3,10));
	%client.currentHitTimeStarted = getSimTime();
	
	messageClient(%client, '', "\c6You have a new target: " @ %client.assassinationTarget.name @ ". I'll pay you \c2$" @ %client.currentHitValue @ "\c6 if you succeed in killing them in less than " @ %client.currentHitTimeInMinutes @ " minutes." );
}

function gameConnection::onAssassinationFailure(%client)
{
	messageClient(%client, '', "Hit Failed: Your target has escaped.");
	%client.assignRandomHit();
}

function gameConnection::onAssassinationSuccess(%client)
{
	messageClient(%client, '', "\c2Hit Succeded: You've successfully killed your target. Wiring the funds now.");
	%client.addMoneyToBankAccount(%client.currentHitValue, "N/A");
	%client.assignRandomHit();
}

function serverCmdrequestNewHit(%client)
{
	if (getSimTime() - %client.timeSinceLastHitReset >= 15 * 60000)
	{
		%client.assignRandomHit();
		return;
	}
	messageClient(%client, '', "You can only request a new hit once every 15 minutes!");
}

package CityRPGHitmanPackage
{
	function CityRPG_OnGetJob(%client, %jobIndex)
	{
		parent::CityRPG_OnGetJob(%client, %jobIndex);
		if (%client.getJobSO().name $= "Hitman")
		{
			%client.assignRandomHit();
			messageClient(%client, '', "You can request a new target once every 15 minutes if you decide that you don't like the current one by typing /requestnewhit.");
			%client.timeSinceLastHitReset = getSimTime();
		}		
	}
	
	function gameConnection::onDeath(%client, %killerPlayer, %killer, %damageType, %unknownA)
	{
		if (%killer.getJobSO().name $= "Hitman" && %client == %killer.assassinationTarget)
		{
			%killer.onAssassinationSuccess();
		}
		
		parent::onDeath(%client, %killerPlayer, %killer, %damageType, %unknownA);
	}
	
	function gameConnection::onClientLeaveGame(%client)
	{
		%clientCount = ClientGroup.getCount();
		for(%i = 0;%i < %clientCount;%i++)
		{
			%currentClient = ClientGroup.getObject(%i);
			if(%currentClient.getJobSO().name $= "Hitman" && %client == %currentClient.assassinationTarget)
			{
			
				%currentClient.onAssassinationFailure();
			}
		}
		
		parent::onClientLeaveGame(%client);
	}
	
	function gameConnection::onClientEnterGame(%client)
	{
		%clientCount = ClientGroup.getCount();
		for(%i = 0;%i < %clientCount;%i++)
		{
			%currentClient = ClientGroup.getObject(%i);
			if(%currentClient.getJobSO().name $= "Hitman" && !%currentClient.assassinationTarget)
			{
				%currentClient.assignRandomHit();
			}
		}
		parent::onClientEnterGame(%client);
	}
};
activatePackage(CityRPGHitmanPackage);