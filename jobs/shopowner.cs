$jobs[$jobCount++] = new ScriptObject(ShopOwner : Job)
{
	name = "Shop Owner";
	initialInvestment = 100;
	pay = 75;
	requiredEducation = 4;

	canSellItems = true;
	canSellFood = true;
	canSellServices = true;
	canSellClothes = true;

	tmHexColor = "0066CC";
	description = "\c6Shop Owners can sell food and items to other people.";
};