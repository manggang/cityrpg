$jobs[$jobCount++] = new ScriptObject(Laborer : Job)
{
	name = "Laborer";
	initialInvestment = 50;
	tools = "CityRPGPickaxeItem CityRPGLumberjackItem";
	
	tmHexColor = "802A2A";
	description = "\c6Laborers do not have a base salary and earn based on how hard they work. They carry both Picks and Lumberjack Axes.";
};