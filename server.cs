// ============================================================
// Section 2 : LAN Test
// ============================================================

if($server::lan)
{
	error("CityRPG cannot support LAN servers because of conflicts within the Saving system.\nThis will most likely never be added due to the severity of this incompatibility/nTerminating...");
	return;
}

// ============================================================
// Section 3 : File Execution
// ============================================================

//Core Files
exec("./jobs/job.cs");
exec("./gui.cs");
exec("./prefs.cs");
exec("./common.cs");
exec("./package.cs");
exec("./saving.cs");
exec("./help.cs");
exec("./playertypes.cs");
exec("./cash.cs");
exec("./reincarnation.cs");
exec("./health.cs");

exec("./items/food/soda.cs");

// Modules
exec("./modules/spacecasts.cs");
exec("./modules/drugs/drugs.cs");
exec("./modules/hunger/hunger.cs");
exec("./modules/crime/crime.cs");
exec("./modules/building/building.cs");
exec("./modules/admins/admins.cs");
exec("./modules/date/date.cs");
exec("./modules/clothes/clothes.cs");
exec("./modules/education/education.cs");
exec("./modules/transactions/transactions.cs");
exec("./modules/resourcegathering/resourcegathering.cs");
exec("./modules/farming/farming.cs");

exec("./events.cs");
exec("./city.cs");

CityRPG_BootUp();

// ============================================================
// Section 2 : Required Add-on loading
// ============================================================