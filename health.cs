%unconsciousnessTick = schedule(20000, false, "CityRPG_UnconsciousnessTick");

AddDamageType("Bloodloss", '<bitmap:Add-Ons/GameMode_CityRPG/shapes/ci/droplet> %1',    '%2 <bitmap:Add-Ons/GameMode_CityRPG/shapes/ci/droplet> %1', 0.5, 1);

function CityRPG_UnconsciousnessTick()
{
	cancel(%unconsciousnessTick);
	
	%clientCount = ClientGroup.getCount();
	for(%i = 0;%i < %clientCount;%i++)
	{
		%currentClient = ClientGroup.getObject(%i);
		if (%currentClient.isUnconscious && getSimTime() - %currentClient.unconsciousnessTime >= 20000)
		{
			%currentClient.player.damage("", "0 0 0", %currentClient.player.getDatablock().maxDamage, $DamageType::Bloodloss);
			%currentClient.isUnconscious = 0;
		}
	}
	
	%unconsciousnessTick = schedule(20000, false, "CityRPG_UnconsciousnessTick");
}

package CityRPGHealthPackage
{
	function servercmdSuicide(%client)
	{
		if (%client.isUnconscious)
		{
			%client.player.damage("", "0 0 0", %client.player.getDatablock().maxDamage, $DamageType::Bloodloss);
			%client.isUnconscious = 0;
			return;
		}
		return parent::servercmdSuicide(%client);
	}
};
activatePackage(CityRPGHealthPackage);

datablock itemData(CityRPGDefibrillatorItem : hammerItem)
{
	category = "Weapon";
	uiName = "Defibrillator";
	image = CityRPGDefibrillatorImage;
	shapeFile = "Add-Ons/GameMode_CityRPG/shapes/defibrillator.dts";
	
	// CityRPG Properties
	noSpawn = true;
};

datablock shapeBaseImageData(CityRPGDefibrillatorImage : hammerImage)
{
	item = CityRPGDefibrillatorItem;
	shapeFile = "Add-Ons/GameMode_CityRPG/shapes/defibrillator.dts";
	projectile = hammerProjectile;
	showBricks = 0;
};

function CityRPGDefibrillatorImage::onPreFire(%this, %obj, %slot)
{
	%obj.playThread(2, "armAttack");
}

function CityRPGDefibrillatorImage::onStopFire(%this, %obj, %slot)
{
	%obj.playThread(2, "root");
}

function CityRPGDefibrillatorImage::onHitObject(%this, %obj, %slot, %col, %pos, %normal)
{
	if(%col.getClassName() $= "Player" && %col.client.isUnconscious)
	{
		if(%col.getType() & $typeMasks::playerObjectType)
		{
			%col.client.isUnconscious = false;
			%col.setDatablock(%client.getJobSO().getDatablock());
			%col.playthread(2, root);
		}
	}
}

function CityRPGDefibrillatorItem::onPickup(%this, %item, %obj)
{
	%item.delete();
}