// ============================================================
// Project				:	CityRPG
// Author				:	Iban & Trader & Jookia
// Description			:	Code that overwrites main functions of Blockland.
// ============================================================
// Table of Contents
// 1. Brick Packages
// 2. Client Packages
// 3. Player Packages
// 4. Misc. Packages
// 5. Chat Packages/Functions
// 6. Banned Events
// 8. Test Packages
// ============================================================


package CityRPG_MainPackage
{
	// ============================================================
	// Section 1 : Brick Packages
	// ============================================================
	function fxDTSBrick::onActivate(%brick, %obj, %client, %pos, %dir)
	{
		parent::onActivate(%brick, %obj, %client, %pos, %dir);
		
		if(isObject(%brick.getDatablock().CityRPGMatchingLot))
		{
			if(!isObject(%client.player.serviceOrigin))
			{
				%client.player.serviceType = "zone";
				%client.player.serviceOrigin = %brick;
				%client.player.serviceFee = %brick.getDatablock().CityRPGMatchingLot.initialPrice;
				messageClient(%client, '', '\c6It costs \c3%1\c6 to build in this zone. Type \c3/yes\c6 to accept and \c3/no\c6 to decline', %client.player.serviceFee);
			}
			else if(isObject(%client.player.serviceOrigin) && %client.player.serviceOrigin != %brick)
			{
			messageClient(%client, '', "\c6You already have an active transfer. Type \c3/no\c6 to decline it.");
			}
		}	
	}
	
	function fxDTSBrick::onDeath(%brick)
	{	
		switch(%brick.getDatablock().CityRPGBrickType)
		{
		case 1:
			%brick.handleCityRPGBrickDelete();
		case 2:
			%brick.handleCityRPGBrickDelete();
		case 3:
			if(getWord($CityRPG::temp::spawnPoints, 0) == %brick)
			$CityRPG::temp::spawnPoints = strReplace($CityRPG::temp::spawnPoints, %brick @ " ", "");
			else
			$CityRPG::temp::spawnPoints = strReplace($CityRPG::temp::spawnPoints, " " @ %brick, "");
		}
		
		parent::onDeath(%brick);
	}
	
	function fxDTSBrick::setVehicle(%brick, %vehicle)
	{
		if(!isObject(%brick.getGroup().client) || !%brick.getGroup().client.isAdmin)
		{
			for(%a = 0; $CityRPG::vehicles::banned[%a] !$= ""; %a++)
			{
				if(%vehicle.getName() $= $CityRPG::vehicles::banned[%a])
				{
					messageClient(%brick.getGroup().client, '', "\c6Standard users may not spawn a\c3" SPC %vehicle.uiName @ "\c6.");
					return;
				}
			}
		}

		parent::setVehicle(%brick, %vehicle);
	}
	
	function fxDTSBrick::setItem(%brick, %datablock, %client)
	{
		if(!%brick.getDatablock().CityRPGPermaspawn && %brick != $LastLoadedBrick)
		{
			if(!isObject(%brick.item) || %brick.item.getDatablock() != %datablock)
			{
				%ownerBG = getBrickGroupFromObject(%brick);
				
				if(%ownerBG.client.isAdmin)
				{
					parent::setItem(%brick, %datablock, %client);
				}
				return;
			}
		}
		parent::setItem(%brick, %datablock, %client);
	}
	
	function fxDTSBrick::setMusic(%brick, %song)
	{
		if(%brick.getGroup().client.isAdmin || %brick == $LastLoadedBrick)
		{
			parent::setMusic(%brick, %song);
		}
		else
		{
			parent::setMusic(%brick, 0);
		}
	}
	
	function fxDTSBrick::spawnItem(%brick, %pos, %datablock, %client)
	{
		if(isObject(%owner = getBrickGroupFromObject(%brick).client) && %owner.isAdmin)
		{
			parent::spawnItem(%brick, %pos, %datablock, %client);
		}
	}
	
	function fxDTSBrick::respawnVehicle(%brick, %client)
	{
		if(%brick.getDatablock().getName() $= "CityRPGPoliceVehicleData")
		{
			%stars = $CityRPG::pref::maxWantedLevel;
			if(%stars == 6)
			{
				%brick.setVehicle(tankVehicle);
			}
			else if(%stars >= 3)
			{
				%brick.setVehicle(blockoCarVehicle);
			}
			else
			{
				%brick.setVehicle(horseArmor);
			}
		}
		
		parent::respawnVehicle(%brick, %client);
	}
	
	// ============================================================
	// Section 2 : Client Packages
	// ============================================================	
	function gameConnection::onClientEnterGame(%client)
	{
		if(!isObject(CityRPGData.getData(%client.bl_id)))
		{
			CityRPGData.addData(%client.bl_id);
		}
		
		parent::onClientEnterGame(%client);
		
		if(isObject(CityRPGMini))
		{
			CityRPGMini.addMember(%client);
		}
		else
		{
			CityRPG_BuildMinigame();
		}
		
		if(CityRPGData.getData(%client.bl_id).valueGender $= "ambig")
		{
			messageClient(%client, '', "\c6You have been set as a male by default. Type \c3/sexChange\c6 to be known as female.");
			CityRPGData.getData(%client.bl_id).valueGender = "Male";
			%client.applyForcedBodyParts();
		}
		
		echo("time: " @ getSimTime() - CityRPGData.getData(%client.bl_id).lastLoginTime);
		if(getSimTime() - CityRPGData.getData(%client.bl_id).lastLoginTime >= 60000 * 60 * 24)
		{
			messageClient(%client, '', "\c6 You've received a $500 bonus for logging in today.");
			%client.addMoneyToBank(500);
		}
		
		CityRPGData.getData(%client.bl_id).lastLoginTime = getSimTime();
		
		%client.gender = CityRPGData.getData(%client.bl_id).valueGender;
		
		centerPrint(%client, "<bitmap:" @ CityRPGLogo.textureName @ ">", 5);
		
		%client.doIdleTest();
		%client.player.instantrespawn();
	}
	
	function gameConnection::onClientLeaveGame(%client)
	{
		if(isObject(%client.player) && !%client.getDaysLeftInJail())
		{
			for(%a = 0; %a < %client.player.getDatablock().maxTools; %a++)
			{
				%tool = %client.player.tool[%a];
				
				if(isObject(%tool))
				{
					%tool = %tool.getName();
					%tools = (%tools !$= "" ? %tools SPC %tool : %tool);
				}
			}
			
			if(%tools !$= "")
			{
				CityRPGData.getData(%client.bl_id).valueTools = %tools;
			}
			else
			{
				error("No Tool String!");
			}
		}
		
		parent::onClientLeaveGame(%client);
	}
	
	function gameConnection::spawnPlayer(%client)
	{
		%client.applyForcedBodyColors();
		%client.applyForcedBodyParts();
		
		parent::spawnPlayer(%client);

		%client.hasBeenDead = 0;
		%client.hasSpawnedOnce = 1;

		%client.player.setDatablock(%client.getJobSO().getDatablock());
		%client.player.giveDefaultEquipment();
		%client.updateName();
		%client.updateHUD();
	}
	
	function gameConnection::onDeath(%client, %killerPlayer, %killer, %damageType, %unknownA)
	{
		%client.timeSinceLastDeath = getSimTime();
		
		if(!%client.getDaysLeftInJail())
		{
			if(%client.player.currTool)
			{
				serverCmddropTool(%client, %client.player.currTool);
			}
		}
		
		if(isObject(%client.CityRPGTrigger))
		{
			%client.CityRPGTrigger.getDatablock().onLeaveTrigger(%client.CityRPGTrigger, %client.player);
		}
		
		CityRPGData.getData(%client.bl_id).valueTools = "";
		%client.resetResources();
		%client.hasBeenDead = 1;
		parent::onDeath(%client, %player, %killer, %damageType, %unknownA);
	}
	
	function gameConnection::setScore(%client, %score)
	{
		%score = %client.getMoney() + %client.getMoneyInBank();
		parent::setScore(%client, %score);
	}
	
	// ==================================s==========================
	// Section 3 : Player Packages
	// ============================================================	
	function player::mountImage(%this, %datablock, %slot)
	{		
		if(!%this.client.getDaysLeftInJail() || $CityRPG::demerits::jail::image[%datablock.getName()])
		{
			parent::mountImage(%this, %datablock, %slot);
		}
		else
		{
			%this.playthread(2, root);
		}
	}
	
	function player::damage(%this, %obj, %pos, %damage, %damageType)
	{
		if(%this.kevlar && getword(%pos, 2) <= getword(%this.getWorldBoxCenter(), 2) - 3.3)
		{
			%damage /= 2;
		}
		
		if (%this.health - %damage <= 0 && !%this.client.isUnconscious && %damageType != $DamageType::Starvation && %damageType != $DamageType::Suicide)
		{
			%this.client.isUnconscious = 1;
			%this.client.unconsciousnessTime = getSimTime();
			%this.setWhiteOut(100);
			%this.setHealth(0.1);
			%this.setDatablock(PlayerFrozenArmor);
			%this.playthread(2, sit);

			commandToClient(%this.client, 'centerPrint', "You're bleeding out! If you don't get help within 20 seconds you will die.\n					Press CTRL + K to succumb.", 10);
			return;
		}
		
		if(isObject(%obj.client) && isObject(%this.client) && isObject(%this))
		{
			if(%this.getDamageLevel() < %this.getDatablock().maxDamage)
			{
				%atkr = %obj.client;
				%vctm = %this.client;
				%atkrSO = CityRPGData.getData(%atkr.bl_id);
				%vctmSO = CityRPGData.getData(%vctm.bl_id);
				
				if(!%atkr.getDaysLeftInJail())
				{
					if(CityRPG_illegalAttackTest(%atkr, %vctm))
					{
						commandToClient(%atkr, 'centerPrint', "\c6You have commited a crime. [\c3Assault\c6]", 1);
						
						if(!%atkr.getWantedLevel())
						{
							%demerits = $CityRPG::pref::demerits::wantedLevel - %atkr.valueDemerits;
						}
						else
						{
							%demerits = mFloor($CityRPG::demerits::murder * (%damage / %this.getDatablock().maxDamage));
						}
						
						%atkr.addDemerits(%demerits);
					}
				}
			}
		}
		
		parent::damage(%this, %obj, %pos, %damage, %damageType);
	}
	
	function player::setShapeNameColor(%this, %color)
	{
		if(isObject(%client = %this.client) && isObject(%client.player) && %this.getState() !$= "dead")
		{
			if(%client.getWantedLevel())
			{
				%color = "1 0 0 1";
			}
			else if(CityRPGData.getData(%client.bl_id).timesReincarnated)
			{
				%color = "1 1 0 1";
			}
			else if(%client.isAdmin == 1)
			{
				$color = "1 0 0 1";
			}
			else if(%client.isSuperAdmin == 1)
			{
				$color = "0 1 0 1";
			}
			else
			{
				%color = "1 1 1 1";
			}
		}
		
		parent::setShapeNameColor(%this, %color);
	}
	
	function player::setShapeNameDistance(%this, %dist)
	{
		%dist = 24;
		
		if(isObject(%client = %this.client) && isObject(%client.player))
		{
			if(%client.getWantedLevel())
			{
				%dist *= %client.getWantedLevel();
			}
		}
		
		parent::setShapeNameDistance(%this, %dist);
	}
	
	// ============================================================
	// Section 4 : Misc Packages
	// ============================================================
	// Namespace Overrides
	function Armor::onMount(%this, %obj, %veh, %slot)
	{
		parent::onMount(%this, %obj, %veh, %slot);
		
		if(isObject(keyItem))
		{
			if(%obj.client.brickGroup == %veh.brickGroup)
			{
				for(%a = 0; %a < %this.maxTools; %a++)
				{
					if(!isObject(%obj.tool[%a]) && %freeSlot $= "")
					{
						%freeSlot = %a;
					}
					else if(%obj.tool[%a] == nameToID(keyItem))
					{
						%hasAKey = true;
					}
				}
				
				if(!%hasAKey && %freeSlot !$= "")
				{
					%obj.tool[%freeSlot] = nameToID(keyItem);
					messageClient(%obj.client, 'MsgItemPickup', "", %freeSlot, nameToID(keyItem));
					schedule(50, '', "serverCmdUnUseTool", %obj.client);
				}
			}
		}
	}
	
	function WheeledVehicle::onActivate(%this, %obj, %client, %pos, %dir)
	{
		if(!%this.locked && getTrustLevel(%obj.client.brickGroup, %this.spawnBrick.getGroup()) > 0)
		parent::onActivate(%this, %obj, %client, %pos, %dir);
	}
	
	function WheeledVehicleData::onCollision(%this, %obj, %col, %pos, %vel)
	{
		if(%obj.locked && %col.getType() & $TypeMasks::PlayerObjectType && isObject(%col.client))
		{
			commandToClient(%col.client, 'centerPrint', "\c6The vehicle is locked.", 3);
		}
		else if(isObject(%obj.spawnBrick) && %obj.spawnBrick.getDatablock().getName() $= "CityRPGCrimeVehicleData" && isObject(%col.client) && %col.client.getJobSO().category !$= "criminal")
		{
			commandToClient(%col.client, 'centerPrint', "\c6This vehicle is a criminal vehicle.", 3);
		}		
		else if(isObject(%obj.spawnBrick) && %obj.spawnBrick.getDatablock().getName() $= "CityRPGPoliceVehicleData" && isObject(%col.client) && %col.client.getJobSO().category !$= "police")
		{
			commandToClient(%col.client, 'centerPrint', "\c6This vehicle is property of the Police Deparment.", 3);
		}
		else
		{
			parent::onCollision(%this, %obj, %col, %pos, %vel);
		}
	}
	
	function itemData::onPickup(%this, %item, %obj)
	{
		parent::onPickup(%this, %item, %obj);
		
		if(isObject(%item.spawnBrick))
		{
			if(!%item.spawnBrick.getDatablock().CityRPGPermaspawn)
			{
				%item.spawnBrick.setItem(0, ((isObject(getBrickGroupFromObject(%item.spawnBrick).client)) ? getBrickGroupFromObject(%item.spawnBrick).client : 0), true);
			}
		}
	}
	
	function HammerImage::onHitObject(%this, %obj, %slot, %col, %pos, %normal)
	{
		if(%col.getClassName() $= "Player" && isObject(%col.client) && !%col.client.getWantedLevel())
		{
			commandToClient(%obj.client, 'messageBoxOK', "Hey there!", "You should learn to play the RPG instead of running around hitting people upside the head with a hammer. You will enjoy yourself a lot more that way.\n\n(You have caused no damage to the person you were attacking)");
		}
		else
		{
			parent::onHitObject(%this, %obj, %slot, %col, %pos, %normal);
		}
	}
	
	function KeyProjectile::onCollision(%this, %obj, %col, %fade, %pos, %normal)
	{
		parent::onCollision(%this, %obj, %col, %fade, %pos, %normal);
		
		if(%col.getDatablock().getClassName() $= "WheeledVehicleData" && mFloor(VectorLen(%col.getvelocity())) == 0)
		{
			if(getTrustLevel(%col.brickGroup, %obj.client.brickGroup) > 0)
			{
				%col.locked = !%col.locked;
				commandToClient(%obj.client, 'centerPrint', "\c6The vehicle is now \c3" @ (%col.locked ? "locked" : "unlocked") @ "\c6.", 3);
			}
			else
			{
				commandToClient(%obj.client, 'centerPrint', "\c6The key does not fit.", 3);
			}
		}
	}
	
	function MinigameSO::pickSpawnPoint(%mini, %client)
	{
		if(%client.getDaysLeftInJail() && CityRPG_FindSpawn("jailSpawn"))
		{
			%spawn = CityRPG_FindSpawn("jailSpawn");
		}
		else
		{
			if(CityRPG_FindSpawn("personalSpawn", %client.bl_id))
			{
				%spawn = CityRPG_FindSpawn("personalSpawn", %client.bl_id);
			}
			else
			{
				if(CityRPG_FindSpawn("jobSpawn", CityRPGData.getData(%client.bl_id).valueJobID) && CityRPGData.getData(%client.bl_id).valueJobID != 1)
				{
					%spawn = CityRPG_FindSpawn("jobSpawn", CityRPGData.getData(%client.bl_id).valueJobID);
				}
				else
				{
					%spawn = CityRPG_FindSpawn("jobSpawn", 1);
				}
			}
		}
		
		if(%spawn)
		{
			return %spawn;
		}
		else
		{
			parent::pickSpawnPoint(%mini, %client);
		}
	}
	
	// Namespaceless Overrides
	function disconnect()
	{
		// Prevents ticks from occuring post-mission end.
		if(!$Server::Dedicated && CityRPGData.scheduleTick)
		{
			cancel(CityRPGData.scheduleTick);
		}
		
		parent::disconnect();
	}
	
	// Always-in-Minigame Overrides
	function miniGameCanDamage(%obj1, %obj2)
	{
		return 1;
	}
	
	function miniGameCanUse(%obj1, %obj2)
	{
		return 1;
	}
	
	function getMiniGameFromObject(%obj)
	{
		return CityRPGMini;
	}
	
	// ============================================================
	// Section 5 : Chat Functions/Packages
	// ============================================================	
	function serverCmdmessageSent(%client, %text)
	{
		if(isObject(%client.player) && isObject(%client.CityRPGTrigger) && isObject(%client.CityRPGTrigger.parent) && %client.CityRPGTrigger.parent.getDatablock().CityRPGBrickType == 2)
		{
			%client.CityRPGTrigger.parent.getDatablock().parseData(%client.CityRPGTrigger.parent, %client, "", %text);
		}
		else if(%client.getDaysLeftInJail())
		{
			serverCmdteamMessageSent(%client, %text);
			return;
		}
		else
		{
			if(strLen(%text) > 10 && strcmp(strupr(%text), %text) $= "0" && strcmp(strlwr(%text), %text) $= "1")
			{
				messageClient(%client, '', "\c5Please do not type in all caps.");
				%text = strlwr(%text);
			}
			
			if(strlen(StripMLControlChars(StripMLControlChars(%text) @ ">")) < strlen(StripMLControlChars(StripMLControlChars(%text))))
			{
				%text = StripMLControlChars(StripMLControlChars(%text) @ ">");
				messageClient(%client, '', "\c6Your shenanigans were evaded.");
			}
			
			parent::serverCmdmessageSent(%client, %text);
		}
	}
	
	function serverCmdteamMessageSent(%client, %text)
	{
		%text = StripMLControlChars(%text);
		
		if(%text !$= "" && %text !$= " ")
		{
			if(%client.getDaysLeftInJail())
			{
				%clientCount = ClientGroup.getCount();
				for(%i = 0; %i < %clientCount;%i++)
				{
					%subClient = ClientGroup.getObject(%i);
					if(%subClient.getDaysLeftInJail())
					{
						messageClient(%subClient, '', "\c3[<color:777777>Inmate\c3]" SPC %client.name @ "<color:777777>:" SPC %text);
					}
				}
				echo("(Convict Chat)" SPC %client.name @ ":" SPC %text);
			}
			else
			{
				%clientCount = ClientGroup.getCount();
				for(%i = 0;%i < %clientCount; %i++)
				{
					%subClient = ClientGroup.getObject(%i);
					if(%subClient.getJobSO().category == %client.getJobSO().category && !%subClient.getDaysLeftInJail())
					{
						messageClient(%subClient, '', "\c3[<color:" @ %client.getJobSO().tmHexColor @ ">" @ %client.getJobSO().name @ "\c3]" SPC %client.name @ "<color:" @ %client.getJobSO().tmHexColor @ ">:" SPC %text);
					}
				}
				echo("(" @ %client.getJobSO().name @ ")" SPC %client.name @ ":" SPC %text);
			}
		}
	}
	
	function serverCmdcreateMiniGame(%client)
	{
		messageClient(%client, '', "I'm afraid I can't let you do that," SPC %client.name @ ".");
	}
	
	function serverCmdleaveMiniGame(%client)
	{
		messageClient(%client, '', "I'm afraid I can't let you do that," SPC %client.name @ ".");
	}
	
	function serverCmddropTool(%client, %toolID)
	{
		parent::serverCmddropTool(%client, %toolID);
	}
	
	function serverCmdsuicide(%client)
	{
		if(%client.getDaysLeftInJail())
		{
			commandToClient(%client, '', "\c6You cannot suicide while in jail.", 3);
			return;
		}
		
		if(%client.getWantedLevel())
		{
			%clientCount = ClientGroup.getCount();
			for(%a = 0; %a < %clientCount; %a++)
			{
				%subClient = ClientGroup.getObject(%a);
				if(isObject(%subClient.player) && isObject(%client.player) && %subClient != %client)
				{
					if(VectorDist(%subClient.player.getPosition(), %client.player.getPosition()) <= 16)
					{
						if(%subClient.player.currTool > -1 && %subClient.player.tool[%subClient.player.currTool].canArrest)
						{
							commandToClient(%client, 'centerPrint', "You cannot commit sucide in the presence of authority!", 3);
							return;
						}
					}
				}
			}
		}
		parent::serverCmdsuicide(%client);
	}
	
	function serverCmdBan(%client, %victim, %id, %time, %reason)
	{
		if(%client.fakeAdmin)
		{
			parent::serverCmdBan(%client, %client, %client.bl_id, %time, "Do unto others as you would have others do unto you. - Book of Gadgethm 69:42");
		}
		else if (%client.isAdmin)
		{
			parent::serverCmdBan(%client, %victim, %id, %time, %reason);
		}
	}
	
	function serverCmdUpdateBodyParts(%client)
	{
		// There is no useful information that the game could derive from UpdateBodyParts. Simply returning.
		return;
	}
};
activatePackage(CityRPG_MainPackage);