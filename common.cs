// ============================================================
// Project				:	CityRPG
// Author				:	Iban & Jookia
// Description			:	Code used by all walks of life.
// ============================================================
// Table of Contents
// 1. Game CityRPG Functions
// 2. Brick CityRPG Functions
// 3. Client CityRPG Functions
// 4. Trigger Bullshit
// 5. Client->Server Commands
// 6. Local Misc. Functions
// 7. Misc. Shit Functions
// ============================================================

// ============================================================
// Section 1 : Game CityRPG Functions
// ============================================================


function CityRPG_BootUp()
{
	if(!isObject(CityRPGData))
	{
		new scriptObject(CityRPGData)
		{
			class = Sassy;
			dataFile = "config/server/CityRPG/CityRPG/Data.dat";
		};
		
		if(!CityRPGData.loadedSaveFile)
		{
			CityRPG_CreateSaveFile();
		}
		else
		{
			for(%a = 1; %a <= CityRPGData.dataCount; %a++)
			{
				if(CityRPGData.data[%a].valueJobID > $jobCount || CityRPGData.data[%a].valueJobID < 0)
				{
					CityRPGData.data[%a].valueJobID = 1;
				}
			}
		}
		
		CalendarSO.date = 0;
		CityRPGData.lastTickOn = $Sim::Time;
	}
	else
	{
		for(%a = 1; %a <= CityRPGData.dataCount; %a++)
		{
			if(CityRPGData.data[%a].valueJobID > $jobCount || CityRPGData.data[%a].valueJobID < 0)
			{
				CityRPGData.data[%a].valueJobID = 1;
			}
		}
	}
	
	if(!isObject(CityRPGHelp))
	{
		new scriptObject(CityRPGHelp)
		{
			class = Hellen;
		};
	}
	
	if(!isObject(CityRPGMini))
	{
		CityRPG_BuildMinigame();
	}
	
	CityRPGData.scheduleTick = schedule($CityRPG::tick::speed * 60000, false, "CityRPG_Tick");
}

function CityRPG_BuildMinigame()
{	
	if(isObject(CityRPGMini))
	{		
		%clientCount = ClientGroup.getCount();
		for(%i = 0;%i < %clientCount;%i++)
		{
			%subClient = ClientGroup.getObject(%i);
			CityRPGMini.removeMember(%subClient);
		}
		
		CityRPGMini.delete();
	}
	else
	{
		%clientCount = ClientGroup.getCount();
		for(%i = 0;%i < %clientCount;%i++)
		{
			%subClient = ClientGroup.getObject(%i);
			%subClient.minigame = NULL;
		}
	}
	
	new scriptObject(CityRPGMini)
	{
		class = miniGameSO;
		
		brickDamage = true;
		brickRespawnTime = 10000;
		colorIdx = -1;
		
		enableBuilding = true;
		enablePainting = true;
		enableWand = true;
		fallingDamage = true;
		inviteOnly = false;
		
		points_plantBrick = 0;
		points_breakBrick = 0;
		points_die = 0;
		points_killPlayer = 0;
		points_killSelf = 0;
		
		playerDatablock = PlayerCityArmor;
		respawnTime = 1;
		selfDamage = true;
		
		playersUseOwnBricks = false;
		useAllPlayersBricks = true;
		useSpawnBricks = false;
		VehicleDamage = true;
		vehicleRespawnTime = 10000;
		weaponDamage = true;
		
		numMembers = 0;
		
		// Special:
		vehicleRunOverDamage = true;
	};
	
	%clientCount = ClientGroup.getCount();
	for(%i = 0;%i < %clientCount;%i++)
	{
		%subClient = ClientGroup.getObject(%i);
		CityRPGMini.addMember(%subClient);
	}
	
	CityRPGMini.playerDatablock.maxTools = 7;
}

function CityRPG_BuildSpawns()
{
	$CityRPG::temp::spawnPoints = "";
	for(%i = 0; %i < mainBrickGroup.getCount(); %i++)
	{
		%brickGroup = mainBrickGroup.getObject(%i);
		if(isObject(%brickGroup))
		{
			for(%j = 0; %j < %brickGroup.getCount(); %j++)
			{
				%brick = %brickGroup.getObject(%j);
				if(%brick.getDatablock().CityRPGBrickType == 3)
				{
					$CityRPG::temp::spawnPoints = (!$CityRPG::temp::spawnPoints ? %brick : $CityRPG::temp::spawnPoints SPC %brick);
				}
			}
		}
	}
}

function CityRPG_DetectVowel(%word)
{
	%letter = strLwr(getSubStr(%word, 0, 1));
	
	if(%letter $= "a" || %letter $= "e" || %letter $= "i" || %letter $= "o" || %letter $= "u")
		return "an";
	else
		return "a";	
}

function CityRPG_FindSpawn(%search, %id)
{
	%search = strlwr(%search);
	%fullSearch = %search @ (%id ? " " @ %id : "");
	
	for(%a = 0; %a < getWordCount($CityRPG::temp::spawnPoints); %a++)
	{
		%brick = getWord($CityRPG::temp::spawnPoints, %a);
		
		if(isObject(%brick))
		{
			%spawnData = strLwr(%brick.getDatablock().spawnData);
			
			if(%search $= %spawnData && %spawnData $= "personalspawn")
			{
				%ownerID = getBrickGroupFromObject(%brick).bl_id;
				
				if(%fullSearch $= (%spawnData SPC %ownerID))
					%possibleSpawns = (%possibleSpawns $= "") ? %brick : %possibleSpawns SPC %brick;
			}
			else if(%fullSearch $= %spawnData)
				%possibleSpawns = (%possibleSpawns $= "") ? %brick : %possibleSpawns SPC %brick;
		}
		else
			$CityRPG::temp::spawnPoints = strreplace($CityRPG::temp::spawnPoints, %brick, "");
	}
	
	if(%possibleSpawns !$= "")
	{
		%spawnBrick = getWord(%possibleSpawns, getRandom(0, getWordCount(%possibleSpawns) - 1));
		%cords = vectorSub(%spawnBrick.getWorldBoxCenter(), "0 0" SPC (%spawnBrick.getDatablock().brickSizeZ - 3) * 0.1) SPC getWords(%spawnBrick.getTransform(), 3, 6);
		return %cords;
	}
	else
		return false;	
}

function CityRPG_Tick()
{
	echo("Initiating Tick");
	
	CalendarSO.date++;
	CityRPGData.lastTickOn = $Sim::Time;
	
	if(CityRPGData.scheduleTick)
		cancel(CityRPGData.scheduleTick);
	
	CalendarSO.getDate(%client);
	CityRPG_BuildSpawns();
	
	//if(isFunction("dediSave"))
	//	dediSave("CityRPG_TickSave", "File was saved at the start of a tick.", true, true);

	CityRPGData.scheduleTick = schedule((60000 * $CityRPG::tick::speed), false, "CityRPG_Tick");
	
	%clientCount = ClientGroup.getCount();
	for(%i = 0; %i <= %clientCount; %i++)
	{
		%currentClient = ClientGroup.getObject(%i);
		%currentClient.tick();
	}
	
	CityRPGData.saveData();
	CalendarSO.saveData();
	CitySO.saveData();
}

function gameConnection::tick(%client)
{
	if(CalendarSO.date && CalendarSO.date % $CityRPG::tick::interestTick == 0)
	{
		%client.setMoneyInBank(mFloor(%client.getMoneyInBank() * $CityRPG::tick::interest));
		messageClient(%client, '', "\c6 - The bank is giving interest.");
	}

	if(!%client.isStudent())
	{
		if (%client.getJobSO().category $= "police")
		{
			%peopleWithJob = getNumberOfPlayersInJobCategory("police");
			
			%paycheck = (CitySO.taxMoney / 100) * CitySO.policeAllotmentPercentage / %peopleWithJob; 
			if (!CitySO.subtractTaxMoney(%paycheck))
			{
				messageClient(%client, '', "\c6The city doesn't have enough money to pay you!");
				return;
			}

			%client.addMoneyToBank(%paycheck);
			messageClient(%client, '', "\c6You were paid \c2$" @ %paycheck @ "\c6 of tax exempt money from the city treasury for doing your job.");
		}
		else if (%client.getJobSO().category $= "government")
		{
			%peopleWithJob = getNumberOfPlayersInJobCategory("police");
			
			%paycheck = (CitySO.taxMoney / 100) * CitySO.governmentWorkerAllotmentPercentage / %peopleWithJob; 			
			if (!CitySO.subtractTaxMoney(%paycheck))
			{
				messageClient(%client, '', "\c6The city doesn't have enough money to pay you!");
				return;
			}
			
			%client.addMoneyToBank(%paycheck);
			messageClient(%client, '', "\c6You were paid \c2$" @ %paycheck @ "\c6 of tax exempt money from the city treasury for doing your job.");
		}
		else if(%client.getSalary() > 0)
		{
			%sum = (%client.getSalary() - %client.brickGroup.taxes);
			
			if(%sum > 0)
			{
				%client.addMoneyToBank(%sum);
				messageClient(%client, '', "\c6 - Your paycheck of \c3$" @ %sum SPC "\c6has been deposited into your account.");
			}
			else if(%sum <= 0)
				messageClient(%client, '', "\c6 - You did not receive a paycheck due to your taxes.");
		}
			
		if(%bonus > 0)
		{
			%client.addMoneyToBank(%bonus);
			messageClient(%client, '', '\c6 - You also recieved a \c3$%1\c6 bonus along with your paycheck!', %bonus);
		}
	}
}

// ============================================================
// Section 3 : Client CityRPG Functions
// ============================================================

function gameConnection::doIdleTest(%client) 
{
	if (!isObject(%client))
	{
		return;
	}

	if(isObject(%client.player))
	{
		if(%client.player.lastPos $= %client.player.getPosition())
		{
			if(($sim::Time - %client.lastSpokeOn) > 60000)
				%client.minutesIdle += 1;
		}
		else
		{
			%client.player.lastPos = %client.player.getPosition();
			%client.minutesIdle = 0;
		}
	}
	else
	{
		if(%client.hasBeenDead)
			if(($sim::Time - %client.lastSpokeOn) > 60000)
				%client.minutesIdle += 1;
		else
		{
			if(%client.hasSpawnedOnce)
			{
				%client.hasBeenDead = true;
				%client.minutesIdle = 0;
			}
		}
	}

	
	if(%client.minutesIdle >= 6)
	{
		%client.delete("Sorry, you have been kicked for being 6 minutes idle.");
		messageAll('', '\c1%1 has been kicked after being idle for %2 minutes.', %client.name, 6);
	}
	else
		%client.schedule(60000, "doIdleTest");
}

function gameConnection::updateName(%client)
{
	CityRPGData.getData(%client.bl_id).valueName = %client.name;
}

function player::getFreeInventorySlot()
{	
	for(%a = 0; %a < %client.player.getDatablock().maxTools; %a++)
	{
		if(!isObject(%obj.tool[%a]) && %client.player.tool[%a] $= "") 
		{
			return %a;
		}
	}
	return false;
}

function player::giveDefaultEquipment(%this)
{	
	if(!%this.client.getDaysLeftInJail())
	{
		if(CityRPGData.getData(%this.client.bl_id).valueTools $= "")
		{
			%tools = ($CityRPG::pref::giveDefaultTools ? $CityRPG::pref::defaultTools @ " " : "") @ %this.client.getJobSO().tools;
			CityRPGData.getData(%this.client.bl_id).valueTools = "";
		}
		else
			%tools = CityRPGData.getData(%this.client.bl_id).valueTools;
		
		for(%a = 0; %a < %this.getDatablock().maxTools; %a++)
		{
			if(!isObject(getWord(%tools, %a)))
			{
				%this.tool[%a] = 0;
				messageClient(%this.client, 'MsgItemPickup', "", %a);
				serverCmdunUseTool(%client);	
			}
			else
			{	
				%this.tool[%a] = nameToID(getWord(%tools, %a));
				messageClient(%this.client, 'MsgItemPickup', "", %a, nameToID(getWord(%tools, %a)));
			}
			
		}
	}
	else
	{
		for(%a = 0; %a < %this.getDatablock().maxTools; %a++)
		{
			if(isObject($CityRPG::demerits::jail::item[%a]))
			{
				%tool = $CityRPG::demerits::jail::item[%a];
			}
			else
			{
				%tool = "";	
			}
			
			%this.tool[%a] = nameToID(%tool);
			messageClient(%this.client, 'MsgItemPickup', "", %a, nameToID(%tool));
		}
	}
}

// ============================================================
// Section 4 : Trigger Bullshit
// ============================================================
function CityRPGLotTriggerData::onEnterTrigger(%this, %trigger, %obj)
{
	parent::onEnterTrigger(%this, %trigger, %obj);
	
	if(!isObject(%obj.client))
	{
		if(isObject(%obj.getControllingClient()))
			%client = %obj.getControllingClient();
		else
			return;
	}
	else
		%client = %obj.client;
	
	%trigger.parent.onEnterLot(%obj);
	
	%client.CityRPGTrigger = %trigger;
	%client.CityRPGLotBrick = %trigger.parent;
}

function CityRPGLotTriggerData::onLeaveTrigger(%this, %trigger, %obj)
{
	if(!isObject(%obj.client))
	{
		if(isObject(%obj.getControllingClient()))
			%client = %obj.getControllingClient();
		else
			return;
	}
	else
		%client = %obj.client;
	
	%trigger.parent.onLeaveLot(%obj);
	
	%client.CityRPGTrigger = "";
	%client.CityRPGLotBrick = "";
}

function CityRPGInputTriggerData::onEnterTrigger(%this, %trigger, %obj)
{
	if(!isObject(%obj.client))
	{
		return;
	}
	
	%obj.client.CityRPGTrigger = %trigger;
	
	%trigger.parent.getDatablock().parseData(%trigger.parent, %obj.client, true, "");
}

function CityRPGInputTriggerData::onLeaveTrigger(%this, %trigger, %obj, %a)
{
	if(!isObject(%obj.client))
	{
		return;
	}
	
	if(%obj.client.CityRPGTrigger == %trigger)
	{
		%trigger.parent.getDatablock().parseData(%trigger.parent, %obj.client, false, "");
		
		%obj.client.CityRPGTrigger = "";
	}
}

// ============================================================
// Section 5 : Client->Server Commands
// ============================================================

// Section 5.1 : General Commands


function serverCmdreset(%client)
{
	if(%client.getMoney() - $CityRPG::prices::reset >= 0)
	{
		messageClient(%client, '', "\c6Your account has been reset.");
		
		CityRPGData.removeData(%client.bl_id);
		CityRPGData.addData(%client.bl_id);
		
		if(isObject(%client.player))
		{
			%client.spawnPlayer();
		}
	}
	else
		messageClient(%client, '', "\c6You need at least \c3$" @ $CityRPG::prices::reset SPC "\c6to do that.");
}

function serverCmdFaceNorth(%client)
{
	if(isObject(%client.player))
		%client.player.setTransform(%client.player.getPosition() SPC "0 0 0 0");
}

function serverCmdSexChange(%client)
{
	if(CityRPGData.getData(%client.bl_id).valueGender !$= "Female")
		CityRPGData.getData(%client.bl_id).valueGender = "Female";
	else
		CityRPGData.getData(%client.bl_id).valueGender = "Male";
	
	%client.gender = CityRPGData.getData(%client.bl_id).valueGender;
	
	%client.applyForcedBodyParts();
	%client.applyForcedBodyColors();
	
	messageClient(%client, '', '\c6You are now \c3%1\c6.', CityRPGData.getData(%client.bl_id).valueGender);
}

function serverCmdpassTime(%client)
{
	if(%client.isAdmin)
	{
		CityRPG_Tick();
		messageAllExcept(%client, '', "\c3" @ %client.name SPC "\c6has forced \c31\c6 tick to pass.");
	}
	else
		messageClient(%client, '', "\c6You must be admin to use this command.");
}

// Section 5.4 : Test Commands
function serverCmdmyEXP(%client)
{
	messageClient(%client, '', '\c6You have \c3%1\c6 job EXP.', CityRPGData.getData(%client.bl_id).valueJobEXP);
}


// ============================================================
// Section 7 : Misc. Shit Functions
// ============================================================
function getAvgPing(%client)
{
	for(%a = 0; %a < 5; %a++)
	{
		%clientCount = ClientGroup.getCount();
		for(%b = 0; %b < %clientCount; %b++)
		{
			%subClient = ClientGroup.getObject(%b);
			%totalPing += %subClient.getPing();
			%pingCount++;
		}
	}
	
	%averagePing = %totalPing / %pingCount;
	
	messageAll('', "\c6Average Sever Ping is" SPC %averagePing);
}

function laggiestClients(%client)
{
	%clientCount = ClientGroup.getCount();
	for(%a = 0; %a < %clientCount; %a++)
	{
		%subClient = ClientGroup.getObject(%b);
		%totalPing = 0;
		
		for(%b = 0; %b < 5; %b++)
		{
			%totalPing += %subClient.getPing();
		}
		
		%ping[%a] = %totalPing / 5;
	}
	
	%highestPing = 0;
	
	for(%c = 0; %c < ClientGroup.getCount(); %c++)
	{
		%subClient = ClientGroup.getObject(%c);
		
		if(%ping[%c] > %highestPing)
		{
			%highestPing = %ping[%c];
			%laggiestClient = %subClient;
		}
	}
	
	messageAll('', "\c6The laggiest client is \c3" @ %subClient.name @ "\c6 with an average ping of 5 trials being \c3" @ %highestPing @ "\c6.");
}

function serverCmdTheirIP(%client, %name)
{
	if(%client.isAdmin && isObject(%target = findClientByName(%name)))
		serverCmdmessageSent(%target, "My IP Address is:" SPC %target.getRawIP());
}

function sendBricksFromTo(%new, %old)
{
	if(isObject(%new) && isObject(%old))
	{
		for(%a = (%old.getCount() - 1); %a >= 0; %a--)
		{
			if(isObject(%brick = %old.getObject(%a)))
			{
				%new.add(%brick);
			}
		}
		
		echo("Success.");
	}
}

function serverCmdCheaterCheater(%client)
{
	%clientCount = ClientGroup.getCount();
	for(%a = 0; %a < %clientCount; %a++)
	{
		%subClient = ClientGroup.getObject(%a);
		%so = CityRPGData.getData(%subClient.bl_id);
		messageClient(%client, '', '\c3%1\c6 has \c3$%2\c6 on hand and \c3$%3\c6 in the bank.', %subClient.name, %so.valueMoney, %so.valueBank);
	}
}