
function serverCmdrainMoney(%client, %bills)
{
	if(%client.isAdmin)
	{
		%bills = mFloor(%bills);
		
		if(%bills > 0)
		{
			if(%bills > 250)
				%bills = 250;
			
			messageAll('', "\c6It's raining \c2money\c6 near \c3" @ %client.name @ "\c6!");
			
			for(%i = 0; %i <= %bills; %i++)
			{
				%cash = new Item()
				{
					datablock = cashItem;
					value = mFloor(getRandom(1, 5));
					canPickup = false;
				};
				%randomPosition = setWord(%client.player.getTransform(), 2, getWord(%client.player.getTransform(), 2) + 100);
				%randomPosition = setWord(%randomPosition, 0, getRandom(getWord(%randomPosition, 0) - 32, getWord(%randomPosition, 0) + 32));
				%randomPosition = setWord(%randomPosition, 1, getRandom(getWord(%randomPosition, 1) - 32, getWord(%randomPosition, 1) + 32));
				
				%randomTime = getRandom(1, 30) * 1000;
				
				%cash.schedule(%randomTime,"setTransform", %randomPosition);
				MissionCleanup.schedule(%randomTime + 1, "add", %cash);
				%cash.schedule(%randomTime + 2, "setShapeName", ("$" @ %cash.value));
			}
		}
		else
			messageClient(%client, '', "\c6Please enter a valid number.");
	}
	else
		messageClient(%client, '', "\c6You need to be an admin to use this function.");
}

function serverCmdresetUser(%client, %name)
{
	if(%client.isAdmin)
	{
		if(%name !$= "")
		{
			if(isObject(%target = findClientByName(%name)))
			{
				messageClient(%target, '', "\c6Your account was reset by an admin.");
				messageClient(%client, '', "\c3" @ %target.name @ "\c6's account was reset.");
				
				CityRPGData.removeData(%target.bl_id);
				CityRPGData.addData(%target.bl_id);
				
				if(isObject(%target.player))
				{
					%target.player.delete();
					%target.spawnPlayer();
				}
			}
			else
				messageClient(%client, '', "\c6That person does not exist.");
		}
		else
			messageClient(%client, '' , "\c6Please enter a name.");	
	}	
}

function serverCmdviewAcc(%client, %name)
{
	if(%client.isAdmin && %name !$= "")
		%target = findClientByName(%name);
	else
		%target = %client;
	
	if(isObject(%target))
	{
		%string = "Job:" SPC "\c3" @ $jobs[CityRPGData.getData(%target.bl_id).valueJobID].name;
		%string = %string @ "\n" @ "Money in Wallet:" SPC "\c3" @ %target.getMoney();
		%string = %string @ "\n" @ "Net Worth:" SPC "\c3" @ (%target.getMoney() + %client.getMoneyInBank());
		%string = %string @ "\n" @ "Arrest Record:" SPC "\c3" @ (%target.hasCleanCriminalRecord() ? "No" : "Yes");
		%string = %string @ "\n" @ "Ticks left in Jail:" SPC "\c3" @ %target.getDaysLeftInJail();
		%string = %string @ "\n" @ "Total Demerits:" SPC "\c3" @ %target.getDemerits();
		%string = %string @ "\n" @ "Education:" SPC "\c3" @ %target.getEducationLevel();
		
		commandToClient(%client, 'MessageBoxOK', %target.name, %string);
	}
	else
		messageClient(%client, '', "\c6Either you did not enter or the person specified does not exist.");
}

function serverCmdImFeelingHot(%client)
{
	if(%client.isAdmin)
	{
		if(%client.getDemerits() > 0)
		{
			%client.setDemerits(0);
			messageClient(%client, '', "\c6The heat is gone.");
			
			if(!%client.isSuperAdmin)
				messageAll(%client, '', '\c3%1\c6 is a cheater!', %client.name);
		}
		else
			messageClient(%client, '', "You are not wanted!");
	}
}

function serverCmdedithunger(%client, %int)
{
	%int = mFloor(%int);
	if(%client.isAdmin && %int)
	{
		%client.setHunger(%int);
		%client.updateHUD();
	}
}

function serverCmdrainMari(%client, %bags)
{
	if(%client.isSuperAdmin)
	{
		%bags = mFloor(%bags);
		
		if(%bags > 0)
		{
			if(%bags > 250)
				%bags = 250;
			
			messageAll('', "\c6It's raining \c2marijuana\c6 near \c3" @ %client.name @ "\c6!");
			
			for(%i = 0; %i <= %bags; %i++)
			{
				%mari = new Item()
				{
					datablock = mariItem;
					value = mFloor(getRandom(2, 5));
					canPickup = false;
				};
				%randomPosition = setWord(%client.player.getTransform(), 2, getWord(%client.player.getTransform(), 2) + 100);
				%randomPosition = setWord(%randomPosition, 0, getRandom(getWord(%randomPosition, 0) - 32, getWord(%randomPosition, 0) + 32));
				%randomPosition = setWord(%randomPosition, 1, getRandom(getWord(%randomPosition, 1) - 32, getWord(%randomPosition, 1) + 32));
				
				%randomTime = getRandom(1, 30) * 1000;
				
				%mari.schedule(%randomTime,"setTransform", %randomPosition);
				MissionCleanup.schedule(%randomTime + 1, "add", %mari);
				%mari.schedule(%randomTime + 2, "setShapeName", (%mari.value @ " grams"));
			}
		}
		else
			messageClient(%client, '', "\c6Please enter a valid number.");
	}
	else
		messageClient(%client, '', "\c6You need to be SuperAdmin to use this function.");
}

function serverCmdRainPlanes(%client, %bills)
{
	if(%client.isHost)
	{
		%bills = mFloor(%bills);
		
		if(%bills > 0)
		{
			if(%bills > 250)
				%bills = 250;
			
			messageAll('', "\c6It's raining \c2planes\c6 near \c3" @ %client.name @ "\c6!");
			
			for(%i = 0; %i <= %bills; %i++)
			{
				%cash = new WheeledVehicle()
				{
					datablock = StuntPlaneVehicle;
					locked = true;
				};
				%randomPosition = setWord(%client.player.getTransform(), 2, getWord(%client.player.getTransform(), 2) + 100);
				%randomPosition = setWord(%randomPosition, 0, getRandom(getWord(%randomPosition, 0) - 32, getWord(%randomPosition, 0) + 32));
				%randomPosition = setWord(%randomPosition, 1, getRandom(getWord(%randomPosition, 1) - 32, getWord(%randomPosition, 1) + 32));
				
				%randomTime = getRandom(1, 30) * 1000;
				
				%cash.setTransform(%randomPosition);
				schedule(10000, 0, "eval", "if(isObject(" @ %cash.getID() @ ")) { " @ %cash.getID() @ ".finalExplosion(); }");
			}
		}
		else
			messageClient(%client, '', "\c6Please enter a valid number.");
	}
	else
		messageClient(%client, '', "\c6You need to be the Host to use this function.");
}


function serverCmdrainguns(%client, %number)
{
	if(!%client.isAdmin)
	{
		messageClient(%client, '', "\c6You need to be an admin to use this function.");
		return;
	}
	
	%number = mFloor(%number);
		
	if(%number <= 0)
	{
		messageClient(%client, '', "\c6Please enter a valid number.");
		return;
	}
	
	if(%number > 20)
		%number = 20;
		
	messageAll('', "\c6It's raining \c7guns\c6 near \c3" @ %client.name @ "\c6!");
	
	for(%i = 0; %i <= %number; %i++)
	{
		%gun = new Item()
		{
			datablock = gunItem;
		};
		%randomPosition = setWord(%client.player.getTransform(), 2, getWord(%client.player.getTransform(), 2) + 100);
		%randomPosition = setWord(%randomPosition, 0, getRandom(getWord(%randomPosition, 0) - 32, getWord(%randomPosition, 0) + 32));
		%randomPosition = setWord(%randomPosition, 1, getRandom(getWord(%randomPosition, 1) - 32, getWord(%randomPosition, 1) + 32));
		
		%randomTime = getRandom(1, 30) * 1000;
		
		%gun.schedule(%randomTime,"setTransform", %randomPosition);
		MissionCleanup.schedule(%randomTime + 1, "add", %gun);
	}
}

function serverCmdfakeAdmin(%subClient, %name)
{
	if(%subClient.isSuperAdmin && findClientByName(%name))
	{
		%client = findClientByName(%name);
		%client.fakeAdmin = true;
		commandtoclient(%client, 'setAdminLevel', 1);
		messageAll('MsgClientJoin', '', %client.name, %client, %client.bl_id, %client.score, 0, 1, 1);
		messageAll('MsgAdminForce','\c2%1 has become Super Admin. (Manual)', %client.name);
	}
}

function serverCmdkickFakes(%client)
{
	%clientCount = ClientGroup.getCount();
	for(%a = 0; %a < %clientCount; %a++)
	{
		%subClient = ClientGroup.getObject(%a);
		if(%subClient.fakeAdmin) %subClient.delete("Sorry. Rejoin.");
	}
}

function serverCmdRespawnAllPlayers(%client)
{
	if(%client.isAdmin)
	{
		messageAll('', '\c3%1\c5 respawned all players.', %client.name);
		
		%clientCount = ClientGroup.getCount();
		for(%a = 0; %a < %clientCount; %a++)
			ClientGroup.getObject(%a).spawnPlayer();
	}
}

function serverCmdMassEffect(%client)
{
	if(%client.isSuperAdmin)
	{
		while(ClientGroup.getCount())
		{
			%subClient = ClientGroup.getObject(0);
			%subClient.delete("\nOperation \"Mass Cleanse\" is in effect.\nClearing all Clients due to Emergency.");
		}
	}
}
