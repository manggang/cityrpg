datablock itemData(CityRPGBatonItem : hammerItem)
{
	category = "Weapon";
	uiName = "Baton";
	image = CityRPGBatonImage;
	colorShiftColor = "0.898039 0.898039 0.000000 1.000000";
	
	// CityRPG Properties
	noSpawn = true;
	canArrest = true;
};

datablock shapeBaseImageData(CityRPGBatonImage : hammerImage)
{
	// SpaceCasts
	raycastWeaponRange = 6;
	raycastWeaponTargets = $TypeMasks::All;
	raycastDirectDamage = 25;
	raycastDirectDamageType = $DamageType::HammerDirect;
	raycastExplosionProjectile = hammerProjectile;
	raycastExplosionSound = hammerHitSound;
	
	item = CityRPGBatonItem;
	projectile = hammerProjectile;
	colorShiftColor = "0.898039 0.898039 0.000000 1.000000";
	showBricks = 0;
};

function CityRPGBatonImage::onPreFire(%this, %obj, %slot)
{
	%obj.playThread(2, "armAttack");
}

function CityRPGBatonImage::onStopFire(%this, %obj, %slot)
{
	%obj.playThread(2, "root");
}

function CityRPGBatonImage::onHitObject(%this, %obj, %slot, %col, %pos, %normal)
{
	if(%col.getClassName() $= "fxDTSBrick")
	{
		%client = %obj.client;
		%brickData = %col.getDatablock();
		
		// Way out of date.. doesn't work.
		if(isObject(%col.door) && %col.door.closed)
		{
			%col.fakeKillBrick(%vec, 3, %client);
			%col.doorDestroy();
		}
		else if(%brickData.isDrug)
		{
			%col.bagPlant(%col);
			commandToClient(%client,'centerPrint',"\c6You have recieved \c3$100 \c6for this plant, in your Bank Account.",3);
			%client.addMoneyToBank(100);
		}
	}
	else if(%col.getClassName() $= "Player")
	{
		%client = %obj.client;
		if((%col.getType() & $typeMasks::playerObjectType) && isObject(%col.client))
		{
			if(%col.client.getWantedLevel())
			{
				if(%col.getDatablock().maxDamage - (%col.getDamageLevel() + 25) < %this.raycastDirectDamage)
				{
					%col.setDamageLevel(%this.raycastDirectDamage + 1);
					%col.client.arrest(%client);
				}
				else
					commandToClient(%client, 'CenterPrint', "\c3" @ %col.client.name SPC "\c6has resisted arrest!", 3);
			}
			else if(((CityRPGData.getData(%col.client.bl_id).valuetotaldrugs) * $CityRPG::drug::demWorth) >= $CityRPG::pref::demerits::wantedLevel)
			{
				%col.client.getWantedLevel();
				if(%col.getDatablock().maxDamage - (%col.getDamageLevel() + 25) < %this.raycastDirectDamage)
				{
					CityRPGData.getData(%col.client.bl_id).valueDemerits += ((CityRPGData.getData(%col.client.bl_id).valuetotaldrugs) * $CityRPG::drug::demWorth);
					%col.setDamageLevel(%this.raycastDirectDamage + 1);
					%col.client.arrest(%client);
					CityRPGData.getData(%col.client.bl_id).valuemarijuana -= CityRPGData.getData(%col.client.bl_id).valuemarijuana;
					CityRPGData.getData(%col.client.bl_id).valuetotaldrugs -= CityRPGData.getData(%col.client.bl_id).valuetotaldrugs;
				}
				else
				{
					commandToClient(%client, 'CenterPrint', "\c3" @ %col.client.name SPC "\c6is carrying drugs, and is resisting arrest!", 3);
					if(CityRPGData.getData(%col.client.bl_id).valueDemerits < $CityRPG::pref::demerits::wantedLevel)
					{
						CityRPGData.getData(%col.client.bl_id).valueDemerits += ((CityRPGData.getData(%col.client.bl_id).valuetotaldrugs) * $CityRPG::drug::demWorth);
					}
				}
			}
			else if(CityRPGData.getData(%col.client.bl_id).valueBounty > 0)
				commandToClient(%client, 'CenterPrint', "\c3" @ %col.client.name SPC "\c6is not wanted alive.", 3);
			else
				%doNoEvil = true;
		}
	}
	
	if(%doNoEvil) { %this.raycastDirectDamage = 10; }
	parent::onHitObject(%this, %obj, %slot, %col, %pos, %normal);
	if(%doNoEvil) { %this.raycastDirectDamage = 25; }
}

function CityRPGBatonItem::onPickup(%this, %item, %obj)
{
	%item.delete();
}