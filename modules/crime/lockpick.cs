// ============================================================
// Project				:	CityRPG
// Author				:	Iban
// Description			:	Lockpick Code file
// ============================================================
// Table of Contents
// 1. Datablocks
// 2. Functions
// ============================================================

// ============================================================
// Section 1 : Datablocks
// ============================================================
datablock itemData(CityRPGLockpickItem : hammerItem)
{
	category = "Weapon";
	uiName = "Lockpick";
	image = CityRPGLockpickImage;
	colorShiftColor = "0.650000 0.000000 0.000000 1.000000";
	
	// CityRPG Properties
	noSpawn = true;
};

datablock shapeBaseImageData(CityRPGLockpickImage : hammerImage)
{
	// SpaceCasts
	raycastWeaponRange = 6;
	raycastWeaponTargets = $TypeMasks::All;
	raycastDirectDamage = 0;
	raycastDirectDamageType = $DamageType::HammerDirect;
	raycastExplosionProjectile = hammerProjectile;
	raycastExplosionSound = hammerHitSound;
	
	item = CityRPGLockpickItem;
	projectile = hammerProjectile;
	colorShiftColor = "0.650000 0.000000 0.000000 1.000000";
	showBricks = 0;
};

// ============================================================
// Section 2 : Functions
// ============================================================

// Section 2.1 : Visual Functionality
function CityRPGLockpickImage::onPreFire(%this, %obj, %slot)
{
	%obj.playThread(2, "armAttack");
}

function CityRPGLockpickImage::onStopFire(%this, %obj, %slot)
{
	%obj.playThread(2, "root");
}

function CityRPGLockpickImage::onHitObject(%this, %obj, %slot, %col, %pos, %normal)
{
	if(%col.getType() & $TypeMasks::FxBrickObjectType)
	{
		%client = %obj.client;
		%brickData = %col.getDatablock();
		if(isObject(%col.door) && %col.door.closed)
		{
			if(getRandom(0, 3) > 2)
			{
				for(%i = 0; %col.eventInput[%i] !$= "" && !%doorPath; %i++)
				{
					if(%col.eventOutput[%i] $= "doorOpen")
						%doorPath = %col.eventOutputParameter[%i, 2];
				}
				
				switch(%doorPath)
				{
					case 0: %doorResult = %col.doorOpenCW();
					case 1: %doorResult = %col.doorOpenCCW();
					default: %doorResult = %col.doorOpen();
				}
				
				%col.schedule(3000, "doorClose");
				
				if(%doorResult == 1)
				{
					%col.schedule(3000, "doorClose");
					CityRPG_OnCrimeCommitted(%client, "Breaking and Entering");
				}
				else
				{
					CityRPG_OnCrimeCommitted(%client, "Attempted Breaking and Entering");
				}
			}
			else
				commandToClient(%client, 'centerPrint', "\c6Lockpicking attempt failed", 1);	
		}
	}
	else if(%col.getType() & $TypeMasks::VehicleObjectType)
	{
		if(%col.locked)
		{
			%col.locked = false;
			CityRPG_OnCrimeCommitted(%client, "Grand Theft Auto");
		}
	}
	
	parent::onHitObject(%this, %obj, %slot, %col, %pos, %normal);
}

function CityRPGLockpickItem::onPickup(%this, %item, %obj)
{
	%item.delete();
}