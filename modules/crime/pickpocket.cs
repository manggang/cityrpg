package CityRPG_PickpocketPackage
{	
	function player::activateStuff(%this)
	{
		parent::activateStuff(%this);
		
		if(%this.client.getJobSO().category $= "criminal")
		{
			%target = containerRayCast(%this.getEyePoint(), vectorAdd(vectorScale(vectorNormalize(%this.getEyeVector()), 2.5), %this.getEyePoint()), $TypeMasks::PlayerObjectType);
			
			if(%this.lastPickpocket + 5 <= $sim::time && %this.client != %target.client && isObject(%target.client))
			{
				if(%target.client.getJobSO().category $= "criminal")
				{	
					messageClient(%this.client, '', "\c6Your target seems very aware of what you're doing...");
					messageClient(%target.client, '', "\c6You feel something bump you from behind.");
				}				
							
				%this.lastPickpocket = $sim::time;
				%pRotate = getWord(%this.rotation, 3);
				%tRotate = getWord(%target.rotation, 3);
				
				if(%target.client.getMoney())
				{
					%amountStolen = mFloor(getRandom(1, %target.client.getMoney() / 5));
					%target.client.subtractMoney(%amountStolen);
					
					%this.client.addMoney(%amountStolen);
					
					messageClient(%this.client, '', "\c6You have stolen \c3$" @ %amountStolen @ "\c6 from\c3" SPC  %target.client.name @ "\c6.");
					
					if(%tRotate + 45 < %pRotate || %tRotate - 45 > %pRotate)
					{
						if(%this.client.getDemerits() + $CityRPG::demerits::pickpocketing < $CityRPG::pref::demerits::wantedLevel)
						{
							%demerits = $CityRPG::pref::demerits::wantedLevel;
							serverCmdmessageSent(%target.client, "Police!" SPC %this.client.name SPC "is a thief!");
						}
						else
							%demerits = $CityRPG::demerits::pickpocketing;
						
						commandToClient(%this.client, 'centerPrint', "\c6You have commited a crime. [\c3Pickpocketing\c6]", 3);
						%this.client.addDemerits(%demerits);
					}
				}
				else
				{
					messageClient(%this.client, '', "\c6They have no money!");
					
					if(%tRotate + 45 < %pRotate || %tRotate - 45 > %pRotate)
					{
						if(%this.client.getDemerits() + $CityRPG::demerits::pickpocketing < $CityRPG::pref::demerits::wantedLevel)
						{
							%demerits = $CityRPG::pref::demerits::wantedLevel;
							serverCmdAlarm(%target.client);
							messageClient(%target.client, '', "\c6You have been pick-pocketed by \c3" @ (%this.client.getWantedLevel() ? %this.client.name : "an unknown thief") @ "\c6!");
						}
						else
							%demerits = ($CityRPG::demerits::pickpocketing / 2);
						
						commandToClient(%this.client, 'centerPrint', "\c6You have commited a crime. [\c3Attempted Pickpocketing\c6]", 3);
						%this.client.addDemerits(%demerits);
					}
				}
			}
		}
	}
};
activatePackage(CityRPG_PickpocketPackage);