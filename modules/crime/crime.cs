exec("./baton.cs");
exec("./taser.cs");
exec("./lockpick.cs");
exec("./pickpocket.cs");

$CityRPG::pref::maxWantedLevel = 6;

$CityRPG::pref::demerits::pardonCost = 100;
$CityRPG::pref::demerits::recordShredCost = 5000;
$CityRPG::pref::demerits::demeritCost = 2;

$CityRPG::pref::demerits::demoteLevel = 100;
$CityRPG::pref::demerits::wantedLevel = 50;

$CityRPG::pref::demerits::reducePerTick = 30;
$CityRPG::pref::demerits::minBounty = 200;

$CityRPG::demerits::hittingInnocents = 10;
$CityRPG::demerits::attemptedMurder = 25;
$CityRPG::demerits::murder = 75;
$CityRPG::demerits::breakingAndEntering = 10;
$CityRPG::demerits::attemptedBnE = 5;
$CityRPG::demerits::bountyPlacing = 250;
$CityRPG::demerits::bountyClaiming = 500;
$CityRPG::demerits::pickpocketing = 25;
$CityRPG::demerits::bankRobbery = 3000;
$CityRPG::demerits::tasingBros = 25;
$CityRPG::demerits::grandTheftAuto = 75;

$CityRPG::prices::jailingBonus = 100;

// ATM Hacking
$CityRPG::pref::hack::requiredEducation = 3;
$CityRPG::pref::hack::demerits = 1000;
$CityRPG::pref::hack::stealmin = 100;
$CityRPG::pref::hack::stealmax = 1000;
$CityRPG::pref::hack::revivetime = 5; //minutes

// ============================================================
// Allowed Inmate Items
// ============================================================
$CityRPG::demerits::jail::image["CityRPGLumberjackImage"] = true;
$CityRPG::demerits::jail::image["CityRPGShovelImage"] = true;
$CityRPG::demerits::jail::image["CityRPGPickaxeImage"] = true;

// Inmate Spawn Items
$CityRPG::demerits::jail::item[0] = "CityRPGPickaxeItem";
$CityRPG::demerits::jail::item[1] = "CityRPGLumberjackItem";

function gameConnection::getDaysLeftInJail(%client)
{
	return CityRPGData.getData(%client.bl_id).valuedaysLeftInJail;
}

function gameConnection::hasCleanCriminalRecord(%client)
{
	if(CityRPGData.getData(%client.bl_id).valuehasCleanCriminalRecord == 1)
	{
		return true;
	}
	return false;
}

function gameConnection::reduceDaysLeftInJail(%client, %amount)
{
	CityRPGData.getData(%client.bl_id).valuedaysLeftInJail -= %amount;
	if (%client.getDaysLeftInJail() < 0)
	{
		CityRPGData.getData(%client.bl_id).valuedaysLeftInJail = 0;
	}
}

function gameConnection::setHasCleanCriminalRecord(%client, %status)
{
	CityRPGData.getData(%client.bl_id).valuehasCleanCriminalRecord = %status;
}

function gameConnection::setDaysLeftInJail(%client, %number)
{
	CityRPGData.getData(%client.bl_id).valuedaysLeftInJail = %number;
}

package CityRPG_CrimePackage
{
	function gameConnection::tick(%client)
	{
		CityRPG_UpdateJailTimes(%client);
		
		if(%client.getDaysLeftInJail())
		{
			if(%ticks = %client.getDaysLeftInJail() > 1)
			{
				%daysLeft = %client.getDaysLeftInJail() - 1;
				
				if(%daysLeft > 1)
					%daySuffix = "s";
					
				messageClient(%client, '', '\c6 - You have \c3%1\c6 day%2 left in Prison.', %daysLeft, %daySuffix);
			}
			
			if(%client.getHunger() < 3)
				%client.addHunger(1);
			else
				%client.setHunger(3);
		}
			
			// ==== Statute of Limitations ==== \\
		if(%so.valueDemerits > 0)
		{
			if(%client.getDemerits() >= $CityRPG::pref::demerits::reducePerTick)
				%client.subtractDemerits($CityRPG::pref::demerits::reducePerTick);
			else
				%client.setDemerits(0);
			
			messageClient(%client, '', '\c6 - You have had your demerits reduced to \c3%1\c6 due to <a:en.wikipedia.org/wiki/Statute_of_limitations>Statute of Limitations</a>\c6.', %so.valueDemerits);
		}
		parent::tick(%client);
	}
	
	function gameConnection::onDeath(%client, %killerPlayer, %killer, %damageType, %unknownA)
	{
		if(isObject(%killer) && %killer != %client)
		{
			if(CityRPGData.getData(%client.bl_id).valueBounty > 0 && !%killer.getJobSO().canClaimBounty)
			{
				CityRPG_OnCrimeCommitted(%killer, "Claiming a Hit");
			}
			else if(CityRPG_illegalAttackTest(%killer, %client))
			{
				if(%killer.lastKill + 15 >= $sim::time)
				{
					CityRPG_OnCrimeCommitted(%killer, %client, "Killing Spree");
				}
				else
				{
					CityRPG_OnCrimeCommitted(%killer, %client, "Murder");
				}
				%killer.lastKill = $sim::time;
			}
		}
		parent::onDeath(%client, %player, %killer, %damageType, %unknownA);
	}
};
activatePackage(CityRPG_CrimePackage);

//todo: replace with polymorphic crime object to make defining new crimes less clunky
function CityRPG_OnCrimeCommitted(%criminalClient, %victimClient, %type)
{
	if (%type $= "Murder")
	{
		%criminalClient.addDemerits($CityRPG::demerits::murder);
	}
	else if (%type $= "Killing Spree")
	{
		%criminalClient.addDemerits($CityRPG::demerits::murder * 1.5);
	}
	else if (%type $= "Claiming a Hit")
	{
		%criminalClient.addDemerits($CityRPG::demerits::bountyClaiming);
						
		messageClient(%criminalClient, '', "\c6Hit was completed successfully. The money has been wired to your bank account.");
		%criminalClient.addMoneyToBank(CityRPGData.getData(%victimClient.bl_id).valueBounty);
		CityRPGData.getData(%victimClient.bl_id).valueBounty = 0;
	}
	else if (%type $= "Breaking and Entering")
	{
		%criminalClient.addDermits($CityRPG::demerits::breakingAndEntering);
	}
	else if (%type $= "Attempted Breaking and Entering")
	{
		%criminalClient.addDemerits($CityRPG::demerits::attemptedBnE);
	}
	else if (%type $= "Grand Theft Auto")
	{
		%criminalClient.addDemerits($CityRPG::demerits::grandTheftAuto);
	}
	
	CityRPG_PrintCrimeMessage(%criminalClient, %type);
}

function CityRPG_PrintCrimeMessage(%criminalClient, %type)
{
	%crimeTypeString = "[\c3]" @ %type @ "\c6]";
	commandToClient(%criminalClient, 'centerPrint', "\c6You have committed a crime." @ crimeTypeString, 1);
}

function gameConnection::getDemerits(%client)
{
	return CityRPGData.getData(%client.bl_id).valueDemerits;
}

function gameConnection::addDemerits(%client, %amount)
{
	%amount = mFloor(%amount);
	if (%amount >= 0)
	{
		CityRPGData.getData(%client.bl_id).valueDemerits += %amount;
		
		if(%client.getDemerits() >= $CityRPG::pref::demerits::demoteLevel && $jobs[CityRPGData.getData(%blid).valueJobID].law == true)
		{		
			CityRPGData.getData(%blid).valueJobID = 1;
			%client.setHasCleanCriminalRecord(0);
			
			messageClient(%client, '', "\c6You have been demoted to" SPC CityRPG_DetectVowel($jobs[1].name) SPC "\c3" @ $jobs[1].name @ "\c6.");
			CityRPG_OnLoseJob(%client);
		}

		if(%client.getWantedLevel() == 3 || %client.getWantedLevel() == 6)
			messageAll('', "\c6Criminal \c3" @ %client.name @ "\c6 has obtained a level \c3%2\c6 wanted level. Police vehicles have been upgraded.");
		
		return true;
	}
	return false;
}

function gameConnection::subtractDemerits(%client, %amount)
{
	%amount = mFloor(%amount);
	if (%amount >= 0)
	{
		CityRPGData.getData(%client.bl_id).valueDemerits -= %amount;
		return true;
	}
	return false;
}

function CityRPG_GetMostWanted()
{
	%clientCount = ClientGroup.getCount();
	for(%a = 0; %a < %clientCount; %a++)
	{
		%subClient = ClientGroup.getObject(%a);
		if(%subClient.getWantedLevel() == $CityRPG::prefs::maxWantedLevel)
			%mostWanted = %subClient;
	}
	
	return (isObject(%mostWanted) ? %mostWanted : 0);
}

function gameConnection::getWantedLevel(%client)
{
	if(CityRPGData.getData(%client.bl_id).valueDemerits >= $CityRPG::pref::demerits::wantedLevel)
	{
		%div = CityRPGData.getData(%client.bl_id).valueDemerits / $CityRPG::pref::demerits::wantedLevel;
		
		if(%div <= 3)
			return 1;
		else if(%div <= 8)
			return 2;
		else if(%div <= 14)
			return 3;
		else if(%div <= 21)
			return 4;
		else if(%div <= 29)
			return 5;
		else
			return 6;
	}
	else
		return 0;
}


function CityRPG_illegalAttackTest(%atkr, %vctm)
{
	if(isObject(%atkr) && isObject(%vctm) && %atkr.getClassName() $= "GameConnection" && %vctm.getClassName() $= "GameConnection")
	{
		if(%atkr != %vctm)
		{				
			if(CityRPGData.getData(%vctm.bl_id).valueBounty && %atkr.getJobSO().canClaimBounty)
				return false;
			else if(!%vctm.getWantedLevel())
				return true;
		}
	}
	
	return false;
}

function CityRPG_UpdateJailTimes(%client)
{
	if(%client.getDaysLeftInJail())
	{
		%client.setHasCleanCriminalRecord(0);
		%target.reduceDaysLeftInJail(1);
		
		if(isObject(%client))
		{
			if(!%client.getDaysLeftInJail())
			{
				messageClient(%client, '', "\c6 - You got out of prison.");
				%client.buyResources();
				%client.spawnPlayer();
			}
		}
	}
}

function gameConnection::arrest(%client, %cop)
{
	if(%ticks = %client.getWantedLevel())
	{
		%copSO = CityRPGData.getData(%cop.bl_id);
		%robSO = CityRPGData.getData(%client.bl_id);
		
		if(!%client.getDaysLeftInJail())
		{
			if(%client.player.currTool)
				serverCmddropTool(%client, %client.player.currTool);
		}
		
		%ticks += %client.getDaysLeftInJail();
		%reward = mFloor($CityRPG::prices::jailingBonus * %ticks);
		%cop.addMoney(%reward);
		
		commandToClient(%client, 'messageBoxOK', "You've been Jailed by" SPC %cop.name @ "!", 'You have been jailed for %1 tick%2.\nToo bad, so sad.\n\nYou may either wait out your jail time in game and possibly earn money by laboring, or you may leave the server and return when your time is up.\nThe choice is yours.', %ticks, %ticks == 1 ? "" : "s");
		commandToClient(%cop, 'centerPrint', "\c6You have jailed \c3" @ %client.name SPC "\c6for \c3" @ %ticks SPC"\c6tick" @ ((%ticks == 1) ? "" : "s") @ ". You were rewarded \c3$" @ %reward @ "\c6.", 5);
		
		%client.setHasCleanCriminalRecord(0);
		%client.setDaysLeftInJail(%ticks);
		%client.setDemerits(0);
		
		%cop.updateHUD();
		
		if(%client.getJobSO().category $= "police")
		{
			messageClient(%client, '', "\c6You have been demoted to" SPC CityRPG_DetectVowel($jobs[1].name) SPC "\c3" @ $jobs[1].name SPC "\c6due to your jailing.");
			%robSO.valueJobID = 1;
		}
		
		if(%cop.getJobSO().category $= "police")
			%copSO.valueJobEXP++;
		
		if(%robSO.valueBounty > 0)
		{
			messageClient(%cop, '', "\c6Wanted man was apprehended successfully. His bounty money has been wired to your bank account.");
			%cop.client.addMoneyToBank(%robSO.valueBounty);
			%robSO.valueBounty = 0;
		}
		
		if(%client.getHunger() > 3)
			%client.setHunger(3);
		
		if(isObject(%client.player.tempBrick))
			%client.player.tempBrick.delete();
		
		%client.spawnPlayer();
		
		if(%ticks == $CityRPG::prefs::maxWantedLevel)
		{
			%maxWanted = CityRPG_GetMostWanted();
			if(%maxWanted)
				messageAll('', '\c6The \c3%1-star\c6 criminal \c3%2\c6 was arrested by \c3%5\c6, but \c3%3-star\c6 criminal \c3%4\c6 is still at large!', %ticks, %client.name, %maxWanted.getWantedLevel(), %maxWanted.name, %cop.name);
			else
				messageAll('', '\c6With the apprehension of \c3%1-star\c6 criminal \c3%2\c6 by \c3%3\c6, the City returns to a peaceful state.', %ticks, %client.name, %cop.name);
		}
		else
			messageAll('', '\c3%1\c6 was jailed by \c3%2\c6 for \c3%3\c6 ticks.', %client.name, %cop.name, %ticks);
	}
}


function serverCmderaseRecord(%client, %name)
{
	if (!%client.getJobSO().canPardon && !%client.isHost)
	{
		messageClient(%client, '', "\c6You can't erase people's record!");
		return;
	}
	
	if (%name $= "")
	{
		messageClient(%client, '' , "\c6Please enter a name.");	
		return;
	}
	
	%target = findClientByName(%name);
	if(!isObject(%target))
	{
		messageClient(%client, '', "\c6That person does not exist.");
		return;
	}
			
	if(%target.hasCleanCriminalRecord())
	{
		messageClient(%client, '', "\c6That person does not have a criminal record.");
		return;
	}
			
	%cost = $CityRPG::pref::demerits::recordShredCost;
	if(!%client.getMoney() >= %cost)
	{
		messageClient(%client, '', "\c6You need at least \c3$" @ %cost SPC "\c6to erase someone's record.");
		return;
	}
	
	if(%client.isHost || %target != %client)
	{
		%target.setHasCleanCriminalRecord(1);
		if(%target != %client)
		{
			messageClient(%client, '', "\c6You have ran\c3" SPC %target.name @ "\c6's criminal record through a paper shredder.");
			messageClient(%target, '', "\c3It seems your criminal record has simply vanished...");

			%client.subtractMoney(%cost);
			CitySO.addTaxMoney(%cost);
		}
		else
			messageClient(%client, '', "\c6You have erased your criminal record.");
	}
	else
	{
		messageClient(%client, '', "\c6The extent of your legal corruption only goes so far. You cannot pardon yourself.");
	}
}

function serverCmdpardon(%client, %name)
{
	if (!%client.getJobSO().canPardon && !%client.isHost)
	{
		messageClient(%client, '', "\c6You can't pardon people.");
		return;
	}
	
	if (%name $= "")
	{
		messageClient(%client, '' , "\c6Please enter a name.");	
		return;
	}
	
	%target = findClientByName(%name);
	if(!isObject(%target))
	{
		messageClient(%client, '', "\c6That person does not exist.");
		return;
	}
			
	if(!%target.getDaysLeftInJail())
	{
		messageClient(%client, '', "\c6That person is not a convict.");
		return;
	}
			
	%cost = $CityRPG::pref::demerits::pardonCost * %target.getDaysLeftInJail();
	if(!%client.getMoney() >= %cost)
	{
		messageClient(%client, '', "\c6You need at least \c3$" @ %cost SPC "\c6to pardon someone.");
		return;
	}
	
	if(%client.isHost || %target != %client)
	{
		%client.subtractMoney((%client.isAdmin ? 0 : %cost));
		CitySO.addTaxMoney(%cost);
		%target.setDaysLeftInJail(0);
							
		if(%target != %client)
		{
			messageClient(%client, '', "\c6You have let\c3" SPC %target.name SPC "\c6out of prison.");
			messageClient(%target, '', "\c3" @ %client.name SPC "\c6has issued you a pardon.");
		}
		else
		{
			messageClient(%client, '', "\c6You have pardoned yourself.");
		}
					
		%target.buyResources();
		%target.spawnPlayer();
	}
	else
	{
		messageClient(%client, '', "\c6The extent of your legal corruption only goes so far. You cannot pardon yourself.");
	}
}


function serverCmdcrims(%client)
{
	if(%client.getJobSO().category $= "police" && isObject(%client.player))
	{
		%clientCount = ClientGroup.getCount();
		for(%i = 0; %i < %clientCount; %i++)
		{
			%subClient = ClientGroup.getObject(%i);
			if(%client != %subClient)
			{
				if(isObject(%subClient.player) && %subClient.getWantedLevel())
				{
					%sCpos = %subClient.player.getPosition();
					%cPos = %client.player.getPosition() ;
					
					%dist[%count++] = VectorDist(%subClient.player.getPosition(), %client.player.getPosition());
					%target[%count] = %subClient;
				}
			}
		}
		
		if(%count)
		{
			%cPos = %client.player.getPosition();
			%cX = getWord(%cPos, 0);
			%cY = getWord(%cPos, 1);
			
			for(%a = 1; %a <= %count; %a++)
			{
				%scPos = %target[%a].player.getPosition();
				%scX = getWord(%scPos, 0);
				%scY = getWord(%scPos, 1);
				
				%xDif = mFloor(%cX - %scX);
				%yDif = mFloor(%cY - %scY);
				
				if(%xDif > 0 && %yDif < 0)
					%loc = "Northwest";
				else if(%xDif == 0 && %yDif < 0)
					%loc = "North";
				else if(%xDif < 0 && %yDif < 0)
					%loc = "Northeast";
				else if(%xDif > 0 && %yDif == 0)
					%loc = "West";
				else if(%xDif < 0 && %yDif == 0)
					%loc = "somewhere";
				else if(%xDif > 0 && %yDif == 0)
					%loc = "East";
				else if(%xDif > 0 && %yDif > 0)
					%loc = "Southwest";
				else if(%xDif == 0 && %yDif > 0)
					%loc = "South";
				else if(%xDif < 0 && %yDif > 0)
					%loc = "Southeast";
				
				messageClient(%client, '', "\c3" @ %target[%a].name SPC "\c6is\c3" SPC mFloor(%dist[%a] * 2) SPC "\c6bricks\c3" SPC %loc SPC "\c6of you.");
			}
		}
		else
		{
			messageClient(%client, '', "\c6No criminals!");
		}
	}
}

function serverCmdaddDemerits(%client, %dems, %name)
{
	if(%client.isAdmin)
	{
		%dems = mFloor(%dems);
		
		if(%dems > 0)
		{
			if(%name !$= "")
			{
				if(isObject(%target = findClientByName(%name)))
				{
					commandToClient(%target, 'centerPrint', "\c6You have commited a crime. [\c3Angering a Badmin\c6]", 5);
					messageClient(%client, '', '\c6User \c3%1 \c6was given \c3%2\c6 demerits.', %target.name , %dems);
					%target.addDemerits(%dems);
				}
				else
					messageClient(%client, "\c6The name you entered could not be matched up to a person.");
			}
			else if(isObject(%client.player))
			{
				%target = containerRayCast(%client.player.getEyePoint(), vectorAdd(vectorScale(vectorNormalize(%client.player.getEyeVector()), 5), %client.player.getEyePoint()), $typeMasks::playerObjectType).client;
				
				if(isObject(%target))
				{
					commandToClient(%target, 'centerPrint', "\c6You have commited a crime. [\c3Angering a Badmin\c6]", 5);
					messageClient(%client, '', '\c6User \c3%1 \c6was given \c3%2\c6 demerits.', %target.name , %dems);
					%target.addDemerits(%dems);
				}
			}
			else
				messageClient(%client, '', "\c6Spawn first before you use this command or enter a valid player's name.");
		}
		else
			messageClient(%client, '', "\c6You must enter a valid amount of money to grant.");
	}
	else
		messageClient(%client, '', "\c6You must be admin to use the this command.");
}

