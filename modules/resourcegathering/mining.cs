$CityRPG::miningEnabled = 1;
// ============================================================
// Section 1 : Datablocks
// ============================================================
if(!isObject(CityRPGPickaxeItem))
{
	AddDamageType("Pickaxe",   '<bitmap:Add-Ons/GameMode_CityRPG/shapes/ci/pickaxe> %1',    '%2 <bitmap:Add-Ons/GameMode_CityRPG/shapes/ci/pickaxe> %1', 0.5, 1);
	
	// Section 1.1 : Pickaxe Datablocks
	datablock ProjectileData(CityRPGPickaxeProjectile)
	{	
		directDamage		= 15;
		directDamageType	= $DamageType::Pickaxe;
		radiusDamageType	= $DamageType::Pickaxe;
		
		muzzleVelocity		= 50;
		velInheritFactor	= 1;
	
		armingDelay			= 0;
		lifetime			= 100;
		fadeDelay			= 70;
		bounceElasticity	= 0;
		bounceFriction		= 0;
		isBallistic			= false;
		gravityMod 			= 0.0;
	
		hasLight			= false;
		lightRadius			= 3.0;
		lightColor			= "0 0 0.5";
	};
	
	datablock ItemData(CityRPGPickaxeItem)
	{
		category		= "Weapon";
		className		= "Weapon";
		
		shapeFile		= "Add-Ons/GameMode_CityRPG/shapes/pickaxe.2.dts";
		mass			= 1;
		density 		= 0.2;
		elasticity		= 0.2;
		friction		= 0.6;

		uiName			= "Pickaxe";
		iconName		= "Add-Ons/GameMode_CityRPG/shapes/ItemIcons/pickaxe";
		doColorShift	= false;
	
		image			= CityRPGPickaxeImage;
		canDrop			= true;
		
		
		// CityRPG Properties
		noSpawn			= true;
	};
	
	datablock ShapeBaseImageData(CityRPGPickaxeImage)
	{
		// SpaceCasts
		raycastWeaponRange = 6;
		raycastWeaponTargets = $TypeMasks::All;
		raycastDirectDamage = 0;
		raycastDirectDamageType = $DamageType::Pickaxe;
		raycastExplosionProjectile = hammerProjectile;
		raycastExplosionSound = hammerHitSound;
		
		shapeFile		= "Add-Ons/GameMode_CityRPG/shapes/pickaxe.2.dts";
		mountPoint		= 0;
		eyeOffset		= "0.7 1.2 -0.9";
		offset			= "0 0 0";
		correctMuzzleVector = false;
		className		= "WeaponImage";
		
		item			= CityRPGPickaxeItem;
		ammo			= " ";
		projectile		= CityRPGPickaxeProjectile;
		projectileType	= Projectile;
		
		melee			= true;
		doRetraction	= false;
		armReady		= true;
		
		doColorShift	= true;
		colorShiftColor = ".54 .27 .07 1";
	
		stateName[0]					= "Activate";
		stateTimeoutValue[0]			= 0.5;
		stateTransitionOnTimeout[0]		= "Ready";
	
		stateName[1]					= "Ready";
		stateTransitionOnTriggerDown[1]	= "PreFire";
		stateAllowImageChange[1]		= true;
	
		stateName[2]					= "PreFire";
		stateScript[2]					= "onPreFire";
		stateAllowImageChange[2]		= false;
		stateTimeoutValue[2]			= 0.1;
		stateTransitionOnTimeout[2]		= "Fire";
	
		stateName[3]					= "Fire";
		stateTransitionOnTimeout[3]		= "CheckFire";
		stateTimeoutValue[3]			= 0.5;
		stateFire[3]					= true;
		stateAllowImageChange[3]		= false;
		stateSequence[3]				= "Fire";
		stateScript[3]					= "onFire";
		stateWaitForTimeout[3]			= true;
	
		stateName[4]					= "CheckFire";
		stateTransitionOnTriggerUp[4]	= "StopFire";
		stateTransitionOnTriggerDown[4]	= "Fire";
	
		stateName[5]					= "StopFire";
		stateTransitionOnTimeout[5]		= "Ready";
		stateTimeoutValue[5]			= 0.2;
		stateAllowImageChange[5]		= false;
		stateWaitForTimeout[5]			= true;
		stateSequence[5]				= "StopFire";
		stateScript[5]					= "onStopFire";
	};
}

// Section 1.2 : Visual Functionality
function CityRPGPickaxeImage::onPreFire(%this, %obj, %slot)
{
	%obj.playthread(2, armAttack);
}

function CityRPGPickaxeImage::onStopFire(%this, %obj, %slot)
{	
	%obj.playthread(2, root);
}

function CityRPGPickaxeImage::onHitObject(%this, %obj, %slot, %col, %pos, %normal)
{
	if(%col.getClassName() $= "fxDTSBrick")
	{
		%brickData = %col.getDatablock();
		if(%brickData.isRock)
			%col.onMine(%obj.client);
	}
	
	parent::onHitObject(%this, %obj, %slot, %col, %pos, %normal);
}

function CityRPGPickaxeItem::onPickup(%this, %item, %obj)
{
	%item.delete();
}

function fxDTSBrick::isValuableOre(%this)
{
	if (%this.getDatablock().isRock == true && %this.getColorID() == 26)
	{
		echo("yes");
		return true;
	}
	return false;
}

function fxDTSBrick::onMine(%this, %client)
{
	if(isObject(%this) && isObject(%client))
	{
		if(%this.hasOre && %this.resources == 0)
			%this.resources = %this.getDatablock().resources;
		
		if(%this.resources > 0)
		{
			%this.hasOre = true;
			if(getRandom(1, 100) > (90 - mFloor((%client.getEducationLevel() / 6) * 4)))
			{
				%this.resources--;
								
				if(!%this.resources)
				{
					commandToClient(%client, 'centerPrint', "\c6You have emptied this rock of its resources!", 3);
					%this.adjustColorOnOreContent();
				}
				
				if (%this.isValuableOre())
				{
					%value = getRandom(5, 50);
					commandToClient(%client, 'centerPrint', "\c1Valuable ore obtained.", 1);
					messageClient(%client, '', "\c6You extracted ore from the rock worth \c3$" @ %value @ "\c6.");
					%client.addMoney(%value);
					return;
				}

				commandToClient(%client, 'centerPrint', "\c6Ore obtained.", 1);
				
				CityRPGData.getData(%client.bl_id).ore++;
				
				if(%client.getJobSO().name $= "Laborer")
					CityRPGData.getData(%client.bl_id).valueJobEXP++;
				
				%client.updateHUD();
			}
		}
	}
}

datablock fxDTSBrickData(CityRPGOreData)
{
	brickFile = "Add-Ons/GameMode_CityRPG/shapes/Bricks/crystal.blb";
	iconName = "Add-Ons/GameMode_CityRPG/shapes/BrickIcons/4x Cube";
	
	category = "CityRPG";
	subCategory = "CityRPG Resources";
	
	uiName = "Ore";
	
	isRock = true;
	hasOre = true;
	resources = 5;
	
	CityRPGBrickType = 4;
	CityRPGBrickAdmin = true;
};

function fxDTSBrick::adjustColorOnOreContent(%this)
{
	if(isObject(%this))
	{
		if(%this.getDatablock().isRock && %this.getDatablock().CityRPGBrickType == 4)
		{

			%this.hasOre = !%this.hasOre;
			
			if(!%this.hasOre)
			{
				%this.setColor(17);
				%this.schedule(getRandom(60000, 120000), "adjustColorOnOreContent");
			}
			else
			{
				%this.setColor(mFloor(getRandom(21,26)));
			}
		}
	}
}

datablock fxDTSBrickData(CityRPGSmallOreData)
{
	brickFile = "Add-Ons/GameMode_CityRPG/shapes/Bricks/Small Ore.blb";
	iconName = "Add-Ons/GameMode_CityRPG/shapes/BrickIcons/2x2";
	
	category = "CityRPG";
	subCategory = "CityRPG Resources";
	
	uiName = "SmallOre";
	
	isRock = true;
	hasOre = true;
	resources = 1;
	
	CityRPGBrickType = 4;
	CityRPGBrickAdmin = true;
};

package CityRPG_OrePackage
{
	function fxDTSBrick::onPlant(%brick)
	{	
		parent::onPlant(%brick);
		
		if(%brick.getDatablock().hasOre && %brick.getDatablock().CityRPGBrickType == 4)
		{
			%brick.hasOre = true;
			%brick.setColor(mFloor(getRandom(21,26)));
		}
	}
	
	function fxDTSBrick::setShapeFX(%this, %FX)
	{
		if(!%this.getDataBlock().isRock)
		{
			parent::setShapeFX(%this, %FX);
		}
	}
	
	function fxDTSBrick::setEmitter(%this, %emitter)
	{
		if(!%this.getDataBlock().isRock)
		{
			parent::setEmitter(%this, %emitter);
		}
	}
};
activatePackage(CityRPG_OrePackage);