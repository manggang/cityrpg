datablock AudioProfile(CityRPGFishingSpearDrawSound)
{
   filename    = "Add-Ons/GameMode_CityRPG/sounds/fishingspeardraw.wav";
   description = AudioClosest3d;
   preload = true;
};


datablock ExplosionData(CityRPGFishingSpearExplosion)
{
   lifeTimeMS = 300;

   soundProfile = swordHitSound;

   particleEmitter = SwordExplosionEmitter;
   particleDensity = 8;
   particleRadius = 0.2;

   faceViewer     = true;
   explosionScale = "1 1 1";

   shakeCamera = true;
   camShakeFreq = "12.0 14.0 12.0";
   camShakeAmp = "0.7 0.7 0.7";
   camShakeDuration = 0.35;
   camShakeRadius = 7.0;

   lightStartRadius = 1.5;
   lightEndRadius = 0;
   lightStartColor = "1.0 0.0 0.0";
   lightEndColor = "0 0 0";
};

datablock ItemData(CityRPGFishingSpearItem : swordItem)
{
	shapeFile = "Add-Ons/GameMode_CityRPG/shapes/fishingspearimage.dts";
	uiName = "Fishing Spear";
	doColorShift = false;
	colorShiftColor = "0.471 0.471 0.471 1.000";

	image = CityRPGFishingSpearImage;
	canDrop = true;
	iconName = "Add-Ons/GameMode_CityRPG/shapes/itemicons/fishingspear";
};

AddDamageType("FishingSpear",   '<bitmap:add-ons/Gamemode_CityRPG/shapes/CI/FishingSpear> %1',    '%2 <bitmap:add-ons/Gamemode_CityRPG/shapes/CI/FishingSpear> %1',0.75,1);

datablock ProjectileData(CityRPGFishingSpearProjectile)
{
   directDamage      = 20;
   directDamageType  = $DamageType::FishingSpear;
   radiusDamageType  = $DamageType::FishingSpear;
   explosion         = CityRPGFishingSpearExplosion;

   muzzleVelocity      = 65;
   velInheritFactor    = 1;

   armingDelay         = 0;
   lifetime            = 100;
   fadeDelay           = 70;
   bounceElasticity    = 0;
   bounceFriction      = 0;
   isBallistic         = false;
   gravityMod = 0.0;

   hasLight    = false;
   lightRadius = 3.0;
   lightColor  = "0 0 0.5";

   uiName = "FishingSpear Hit";
};

datablock ShapeBaseImageData(CityRPGFishingSpearImage)
{
   shapeFile = "Add-Ons/GameMode_CityRPG/shapes/FishingSpear.dts";
   
   mountPoint = 0;
   offset = "0 0 0";

   correctMuzzleVector = false;

   className = "WeaponImage";

   item = CityRPGFishingSpearItem;
   ammo = " ";
   projectile = CityRPGFishingSpearProjectile;
   projectileType = Projectile;


   melee = true;
   doRetraction = false;

   armReady = true;


   doColorShift = true;
   colorShiftColor = "0.471 0.471 0.471 1.000";

	stateName[0]                     = "Activate";
	stateTimeoutValue[0]             = 0.5;
	stateTransitionOnTimeout[0]      = "Ready";
	stateSound[0]                    = CityRPGFishingSpearDrawSound;

	stateName[1]                     = "Ready";
	stateTransitionOnTriggerDown[1]  = "PreFire";
	stateAllowImageChange[1]         = true;

	stateName[2]			= "PreFire";
	stateScript[2]                  = "onPreFire";
	stateAllowImageChange[2]        = false;
	stateTimeoutValue[2]            = 0.1;
	stateTransitionOnTimeout[2]     = "Fire";

	stateName[3]                    = "Fire";
	stateTransitionOnTimeout[3]     = "CheckFire";
	stateTimeoutValue[3]            = 0.2;
	stateFire[3]                    = true;
	stateAllowImageChange[3]        = false;
	stateSequence[3]                = "Fire";
	stateScript[3]                  = "onFire";
	stateWaitForTimeout[3]		= true;
	stateSequence[3]		 = "Stab";
	//stateTransitionOnTriggerUp[3]	= "StopFire";

	stateName[4]			= "CheckFire";
	stateTransitionOnTriggerUp[4]	= "StopFire";
	stateTransitionOnTriggerDown[4]	= "Fire";

	
	stateName[5]                    = "StopFire";
	stateTransitionOnTimeout[5]     = "Ready";
	stateTimeoutValue[5]            = 0.2;
	stateAllowImageChange[5]        = false;
	stateWaitForTimeout[5]		= true;
	stateSequence[5]                = "StopFire";
	stateScript[5]                  = "onStopFire";


};

function CityRPGFishingSpearImage::onStopFire(%this, %obj, %slot)
{	
	%obj.playthread(2, root);
}

function CityRPGFishingSpearProjectile::onCollision(%this,%obj,%col,%fade,%pos,%normal)
{
	parent::onCollision(%this,%obj,%col,%fade,%pos,%normal);
	if(%col.getClassName() $= "fxDTSBrick")
	{
		$InputTarget_["Self"] = %col;
		$InputTarget_["Player"] = %obj.client.player;
		$InputTarget_["Client"] = %obj.client;
		if($Server::LAN)
		{
			$InputTarget_["MiniGame"] = getMiniGameFromObject(%obj.client);
		}
		else
		{
			if(getMiniGameFromObject(%col) == getMiniGameFromObject(%obj.client))
			{
				$InputTarget_["MiniGame"] = getMiniGameFromObject(%col);
			}
			else
			{
				$InputTarget_["MiniGame"] = 0;
			}
		}
		%col.processInputEvent(OnFishingSpearHit,%obj.client);
	}
}


function CityRPGFishingSpearImage::onMount(%this, %obj, %slot)
{	
	%obj.hidenode("RHand");
}


function CityRPGFishingSpearImage::onUnMount(%this, %obj, %slot)
{	
	%obj.unhidenode("RHand");
}

function CityRPGFishingSpearImage::onHitObject(%this, %obj, %slot, %col, %pos, %normal)
{
	if(%col.getClassName() $= "fxDTSBrick")
	{
		%brickData = %col.getDatablock();
		if(%brickData.isFish)
			%col.onFished(%obj.client);
	}
	
	parent::onHitObject(%this, %obj, %slot, %col, %pos, %normal);
}

datablock itemData(CityRPGFishingRodItem : hammerItem)
{
	category = "Weapon";
	uiName = "Rod";
	image = CityRPGrodImage;
	colorShiftColor = "0.798039 0.698039 0.500000 1.000000";
	
	// CityRPG Properties
	noSpawn = true;
};

datablock shapeBaseImageData(CityRPGFishingRodImage : hammerImage)
{
	// SpaceCasts
	raycastWeaponRange = 12;
	raycastWeaponTargets = $TypeMasks::All;
	raycastDirectDamage = 1;
	raycastDirectDamageType = $DamageType::HammerDirect;
	raycastExplosionProjectile = hammerProjectile;
	raycastExplosionSound = hammerHitSound;
	
	item = CityRPGFishingRodItem;
	projectile = hammerProjectile;
	colorShiftColor = "0.798039 0.698039 0.500000 1.000000";
	showBricks = 0;
};

function CityRPGFishingRodImage::onPreFire(%this, %obj, %slot)
{
	%obj.playThread(2, "armAttack");
}

function CityRPGFishingRodImage::onStopFire(%this, %obj, %slot)
{
	%obj.playThread(2, "root");
}

function CityRPGFishingRodImage::onHitObject(%this, %obj, %slot, %col, %pos, %normal)
{
	if(%col.getClassName() $= "fxDTSBrick")
	{
		%brickData = %col.getDatablock();
		if(%brickData.isFish)
			%col.onFished(%obj.client);
	}
	
	parent::onHitObject(%this, %obj, %slot, %col, %pos, %normal);
}

function fxDTSBrick::onFished(%this, %client)
{
	if(isObject(%this) && isObject(%client))
	{
		if(%this.hasFish && %this.resources == 0)
			%this.resources = %this.getDatablock().resources;
		
		if(%this.resources > 0)
		{
			%this.hasFish = true;
			if(getRandom(1, 100) > (90 - mFloor((%client.getEducationLevel() / 6) * 4)))
			{
				%this.resources--;
				
				if(getRandom(1, 100) > 100 - %client.getEducationLevel())
					%boot = true;
				
				if(%boot)
				{
					%value = getRandom(5, 50);
					commandToClient(%client, 'centerPrint', "\c1Boot obtained.", 1);
					messageClient(%client, '', "\c6You extracted a boot from the lake worth \c3$" @ %value @ "\c6.");
					%client.addMoney(%value);
				}
				else
				{
					if(!%this.resources)
					{
						commandToClient(%client, 'centerPrint', "\c6You have emptied this lake of its resources!", 3);
						%this.adjustColorOnFishContent();
					}
					else
						commandToClient(%client, 'centerPrint', "\c6Fish obtained.", 1);
					
					CityRPGData.getData(%client.bl_id).fish++;
				}
				
				if(%client.getJobSO().name $= "Laborer")
					CityRPGData.getData(%client.bl_id).valueJobEXP++;
				
				%client.updateHUD();
			}
		}
	}
}

datablock fxDTSBrickData(CityRPGFishData)
{
	brickFile = "Add-Ons/GameMode_CityRPG/shapes/Bricks/32x32.blb";
	iconName = "Add-Ons/GameMode_CityRPG/shapes/BrickIcons/32x32";
	
	category = "CityRPG";
	subCategory = "CityRPG Resources";
	
	uiName = "Fish";
	
	isFish = true;
	hasFish = true;
	resources = 15;
	
	CityRPGBrickType = 4;
	CityRPGBrickAdmin = true;
};

function fxDTSBrick::adjustColorOnFishContent(%this)
{
	if(isObject(%this))
	{
		if(%this.getDatablock().isFish && %this.getDatablock().CityRPGBrickType == 4)
		{
			%this.hasFish = !%this.hasFish;
			%this.setColor(0);
			
			if(!%this.hasFish)
			{
				%this.schedule(getRandom(60000, 120000), "adjustColorOnFishContent");
			}
		}
	}
}

package CityRPG_FishPackage
{
	function fxDTSBrick::onPlant(%brick)
	{	
		parent::onPlant(%brick);
		
		if(%brick.getDatablock().hasFish && %brick.getDatablock().CityRPGBrickType == 4)
		{
			%brick.hasFish = true;
			%brick.setColor(0);
		}
	}
	
	function fxDTSBrick::setColor(%this, %color)
	{
		if(%this.getDataBlock().isFish)
		{
			if(!%this.hasFish)
			{
				parent::setColor(%this, $CityRPG::pref::resources::Fish::noFishColorID);
			}
			else
			{
				parent::setColor(%this, $CityRPG::pref::resources::Fish::hasFishColorID);
			}
		}
		else
		{
			parent::setColor(%this, %color);
		}
	}
	
	function fxDTSBrick::setColorFX(%this, %FX)
	{
		if(!%this.getDataBlock().isFish)
		{
			parent::setColorFX(%this, %FX);
		}
	}
	
	function fxDTSBrick::setShapeFX(%this, %FX)
	{
		if(!%this.getDataBlock().isFish)
		{
			parent::setShapeFX(%this, %FX);
		}
	}
	
	function fxDTSBrick::setEmitter(%this, %emitter)
	{
		if(!%this.getDataBlock().isFish)
		{
			parent::setEmitter(%this, %emitter);
		}
	}
};
activatePackage(CityRPG_FishPackage);