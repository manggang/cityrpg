if(!isObject(CityRPGLumberjackItem))
{
	datablock projectileData(CityRPGLumberjackProjectile : swordProjectile)
	{
		directDamage		= 15;
		directDamageType	= $DamageType::Sword;
		radiusDamageType	= $DamageType::Sword;
		
		muzzleVelocity		= 50;
		velInheritFactor	= 1;
	
		armingDelay			= 0;
		lifetime			= 100;
		fadeDelay			= 70;
		bounceElasticity	= 0;
		bounceFriction		= 0;
		isBallistic			= false;
		gravityMod 			= 0.0;
	
		hasLight			= false;
		lightRadius			= 3.0;
		lightColor			= "0 0 0.5";
	};
	
	datablock itemData(CityRPGLumberjackItem : swordItem)
	{
		uiName = "Lumberjack Axe";
		image = CityRPGLumberjackImage;
		shapeFile = "Add-Ons/GameMode_CityRPG/shapes/axe.dts";
		
		// CityRPG Properties
		noSpawn			= true;
	};
	
	datablock shapeBaseImageData(CityRPGLumberjackImage : swordImage)
	{
		// SpaceCasts
		raycastWeaponRange = 6;
		raycastWeaponTargets = $TypeMasks::All;
		raycastDirectDamage = 0;
		raycastDirectDamageType = $DamageType::Sword;
		raycastExplosionProjectile = swordProjectile;
		raycastExplosionSound = swordHitSound;
		
		item = CityRPgLumberjackItem;
		shapeFile = "Add-Ons/GameMode_CityRPG/shapes/axe.dts";
		projectile = CityRPGLumberjackProjectile;
		correctMuzzleVector = false;
		armReady = true;
		melee = true;
		doRetraction	= false;
		offset			= "0 0 0";
		eyeOffset = 0;
	};
}

function CityRPGPickaxeImage::onPreFire(%this, %obj, %slot)
{
	%obj.playthread(2, armAttack);
}

function CityRPGPickaxeImage::onStopFire(%this, %obj, %slot)
{	
	%obj.playthread(2, root);
}

function CityRPGLumberjackImage::onHitObject(%this, %obj, %slot, %col, %pos, %normal)
{
	if(%col.getClassName() $= "fxDTSBrick")
	{
		%brickData = %col.getDatablock();
		if(%brickData.isTree)
		{
			%col.onChop(%obj.client);
		}
	}
	
	parent::onHitObject(%this, %obj, %slot, %col, %pos, %normal);
}

function CityRPGLumberjackItem::onPickup(%this, %item, %obj)
{
	%item.delete();
}

function fxDTSBrick::onChop(%this, %client)
{
	if(%this.resources > 0 && !%this.isFakeDead())
	{
		if(getRandom(1, 100) > (95 - mFloor((%client.getEducationLevel() / 6) * 4)))
		{
			%this.resources--;
			
			if(%this.resources)
				commandToClient(%client, 'centerPrint' , "\c6Lumber obtained.", 1);
			else
			{
				commandToClient(%client, 'centerPrint' , "\c6Lumber obtained, however you killed the tree in the process.", 3);
				%this.fakeKillBrick(getRandom(-10, 10) SPC getRandom(-10, 10) SPC getRandom(0, 10), getRandom(120, 180));
				%this.resources = %this.getDatablock().resources;
			}
			
			CityRPGData.getData(%client.bl_id).lumber++;
			%client.updateHUD();
			
			if(%client.getJobSO().name $= "Laborer")
				CityRPGData.getData(%client.bl_id).valueJobEXP++;
		}
	}
}

datablock fxDTSBrickData(CityRPGTreeData : brickPineTreeData)
{
	category = "CityRPG";
	subCategory = "CityRPG Resources";
	
	uiName = "Lumber Tree";
	
	isTree = true;
	resources = 20;
	
	CityRPGBrickType = 4;
	CityRPGBrickAdmin = true;
};