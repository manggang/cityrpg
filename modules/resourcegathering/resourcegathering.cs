exec("./mining.cs");
exec("./woodcutting.cs");
exec("./fishing.cs");

$CityRPG::pref::resources::tree::regrowTime = 2.5;
$CityRPG::pref::resources::ore::noOreColorID = 39;
$CityRPG::pref::resources::ore::hasOreColorID = 35;
$CityRPG::pref::resources::fish::noOreColorID = 52;
$CityRPG::pref::resources::fish::hasOreColorID = 55;

function gameConnection::resetResources(%client)
{
	CityRPGData.getData(%client.bl_id).fish = 0;
	CityRPGData.getData(%client.bl_id).lumber = 0;
	CityRPGData.getData(%client.bl_id).ore = 0;
	%client.updateHUD();	
}

function gameConnection::buyResources(%client)
{
	%totalResources = CityRPGData.getData(%client.bl_id).ore + CityRPGData.getData(%client.bl_id).fish + CityRPGData.getData(%client.bl_id).lumber;
	
	if(mFloor(%totalResources * $CityRPG::prices::resourcePrice) > 0)
	{	
		%product = mFloor(%totalResources * $CityRPG::prices::resourcePrice);
		
		if(%client.getJobSO().name $= "Laborer")
		{
			%product *= getRandom(1.5 + (%client.getEducationLevel / 12), 2.0 + (%client.getEducationLevel / 12));
			%product = mFloor(%product);
		}
		
		if(!%client.getDaysLeftInJail())
		{
			%client.addMoney(%product);
			messageClient(%client, '', "\c6The state has bought all of your resources for \c3$" @ %product @ "\c6.");
		}
		else
		{
			%client.addMoneyToBank(%product);
			messageClient(%client, '', '\c6The state has set aside \c3$%1\c6 for when you get out of Prison.', %product);
		}
		
		CitySO.lumber += CityRPGData.getData(%client.bl_id).lumber;
		CitySO.ore += CityRPGData.getData(%client.bl_id).ore;
		%client.resetResources();
		%client.printCityResources();
	}
}

package CityRPG_ResourceGatheringPackage
{
	function CitySO::loadData(%so)
	{
		parent::loadData(%so);
		if(isFile("config/server/CityRPG/CityRPG/City.cs"))
		{
			exec("config/server/CityRPG/CityRPG/City.cs");
			%so.ore		= $CityRPG::citydata::ore;
			%so.lumber			= $CityRPG::citydata::lumber;
			%so.gems = $CityRPG::citydata::gems;
		}
		else
		{
			%so.ore = 0;
			%so.lumber = 0;
			%so.gems = 0;
		}
	}
	
	function CitySO::saveData(%so)
	{	
		parent::saveData(%so);
		$CityRPG::citydata::ore		= %so.ore;
		$CityRPG::citydata::lumber		= %so.lumber;
		export("$CityRPG::citydata::*", "config/server/CityRPG/CityRPG/City.cs");
	}
	
	function serverCmdCity(%client)
	{
		parent::serverCmdCity(%client);
		%client.printCityResources();
	}
	
	function gameConnection::printCityResources(%client)
	{
		messageClient(%client, '', "\c2City Resources:");
		messageClient(%client, '', "\c6Lumber: " @ CitySO.lumber);
		messageClient(%client, '', "\c6Ore: " @ CitySO.ore);
		messageClient(%client, '', "\c6Gems: " @ CitySO.gems);
	}
};
activatePackage(CityRPG_ResourceGatheringPackage);
