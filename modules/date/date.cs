
function CalendarSO::loadCalendar(%so)
{
	// Counters
	%so.numOfMonths = 12;
	%so.zbNumMonths = %so.numOfMonths - 1;
	
	// Names
	%so.nameOfMonth[0] = "January";
	%so.nameOfMonth[1] = "February";
	%so.nameOfMonth[2] = "March";
	%so.nameOfMonth[3] = "April";
	%so.nameOfMonth[4] = "May";
	%so.nameOfMonth[5] = "June";
	%so.nameOfMonth[6] = "July";
	%so.nameOfMonth[7] = "August";
	%so.nameOfMonth[8] = "September";
	%so.nameOfMonth[9] = "October";
	%so.nameOfMonth[10] = "November";
	%so.nameOfMonth[11] = "December";
	
	// Days
	%so.daysInMonth[0] = 31;
	%so.daysInMonth[1] = 28;
	%so.daysInMonth[2] = 31;
	%so.daysInMonth[3] = 30;
	%so.daysInMonth[4] = 31;
	%so.daysInMonth[5] = 30;
	%so.daysInMonth[6] = 31;
	%so.daysInMonth[7] = 31;
	%so.daysInMonth[8] = 30;
	%so.daysInMonth[9] = 31;
	%so.daysInMonth[10] = 30;
	%so.daysInMonth[11] = 31;
	
	// Holidays
	%so.holiday[1] = "\c2Happy New Year!";
	%so.holiday[45] = "\c0Happy valentine's day!";
	%so.holiday[91] = "\c2A\c1p\c2r\c1i\c2l \c0F\c3o\c0o\c3l\c0s \c7D\c6a\c7y\c6!";
	%so.holiday[265] = "\c1It's \c5Random\c3 Day\c4!\c2!";
	%so.holiday[304] = "\c7Happy halloween!";
	%so.holiday[350] = "\c0H\c3a\c2p\c1p\c5y\c6 Holidays\c7!";
	
	%so.season[60] = "Spring";
	%so.season[152] = "Summer";
	%so.season[244] = "Autumn";
	%so.season[335] = "Winter";
}

function CalendarSO::getDate(%so, %client)
{
	%ticks = %so.date;
	
	for(%a = 0; %ticks > %so.daysInMonth[%a % %so.numOfMonths]; %a++)
	{
		%ticks -= %so.daysInMonth[%a % %so.numOfMonths];
	}
	
	%year = mFloor(%a / %so.numOfMonths);
	
	// If the second number from last is a "1" (e.g. 12 or 516), the suffix will always be "th"
	if(strlen(%ticks) > 1 && getSubStr(%ticks, (strlen(%ticks) - 2), 1) $= "1")
	{
		%suffix = "th";
	}
	// If not, it can either be "st," "nd," "rd," or "th," depending on the last numeral.
	else
	{
		switch(getSubStr(%ticks, (strlen(%ticks) - 1), 1))
		{
			case 1: %suffix = "st";
			case 2: %suffix = "nd";
			case 3: %suffix = "rd";
			default: %suffix = "th";
		}
	}
	
	messageAll('', '\c6Today, on \c3%1\c6 the \c3%2\c6%4 of year \c3%3\c6, ...', %so.nameOfMonth[%so.getMonth()], %ticks, %year, %suffix);
	
	if(%so.holiday[%so.getCurrentDay()] !$= "")
	{
		messageAll('', "\c6 -" SPC %so.holiday[%so.getCurrentDay()]);
	}
	
	if (%so.season[%so.getCurrentDay()] !$= "")
	{
		messageAll('', "\c6It is now" SPC %so.season[%so.getCurrentDay()] SPC ".");
		%so.currentSeason = %so.season[%so.getCurrentDay()];
	}
		
	if(isObject(WeatherSO))
	{
		WeatherSO.todaysForecast();
	}
}

function CalendarSO::getMonth(%so)
{
	%ticks = %so.date;
	
	for(%a = 0; %ticks > %so.daysInMonth[%a % %so.numOfMonths]; %a++)
		%ticks -= %so.daysInMonth[%a % %so.numOfMonths];
	
	%month = %a % %so.numOfMonths;
	return %month;
}

function CalendarSO::dumpCalendar(%so)
{
	for(%a = 0; %so.daysInMonth[%a] !$= ""; %a++)
	{
		echo(%so.nameOfMonth[%a] SPC "has" SPC %so.daysInMonth[%a] SPC "days.");
	}
}

function CalendarSO::getYearLength(%so)
{
	for(%a = 0; %so.daysInMonth[%a] > 0; %a++)
	{
		%totalLength += %so.daysInMonth[%a];
	}
	
	return %totalLength;
}

function CalendarSO::getCurrentDay(%so)
{
	return (%so.date % %so.getYearLength());
}

function CalendarSO::getCurrentSeason(%so)
{
	return %so.currentSeason;
}

function CalendarSO::loadData(%so)
{
	if(isFile("config/server/CityRPG/CityRPG/Calendar.cs"))
	{
		exec("config/server/CityRPG/CityRPG/Calendar.cs");
		%so.date = $CityRPG::temp::calendar::datumdate;
	}
	else
	{
		%so.date = 0;
	}
	
	%so.loadCalendar();
}

function CalendarSO::saveData(%so)
{
	$CityRPG::temp::calendar::datum["date"]	= %so.date;
	export("$CityRPG::temp::calendar::*", "config/server/CityRPG/CityRPG/Calendar.cs");
}

if(!isObject(CalendarSO))
{
	new scriptObject(CalendarSO) { };
	CalendarSO.schedule(1, "loadData");
}
