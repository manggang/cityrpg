$CityRPG::pref::maxEducationLevel = 6;

function gameConnection::getEducationLevel(%client)
{
	return CityRPGData.getData(%client.bl_id).valueEducation;
}

function gameConnection::addEducationLevels(%client, %number)
{
	CityRPGData.getData(%client.bl_id).valueEducation += %number;
}

function gameConnection::setEducationLevel(%client, %value)
{
	if (%value >= 0)
	{
		CityRPGData.getData(%client.bl_id).valueEducation = %value;
	}
	else
	{
		CityRPGData.getData(%client.bl_id).valueEducation = %value;
	}
}

function gameConnection::isStudent(%client)
{
	return CityRPGData.getData(%client.bl_id).valuedaysLeftInSchool;
}

function serverCmdeducation(%client, %do)
{
	%price = ((%client.getEducationLevel() + 1) * 225);
	
	if(%do $= "accept")
	{
		if(%client.getEducationLevel() > $CityRPG::pref::maxEducationLevel)
		{
			messageClient(%client, '', "\c6The University of Blockland cannot further your education any more.");
			return;
		}
		
		if(%client.isStudent())
		{
			messageClient(%client, '', "\c6Don't get too far ahead of yourself. You're already enrolled for this course.");
			return;
		}
		
		if(!%client.subtractMoney(%price))
		{
			messageClient(%client, '', "\c6It costs \c3$" @ %price SPC "\c6to get enrolled. You do not have enough money.");
			return;
		}
		
		CitySO.addTaxMoney(%price);		
		CityRPGData.getData(%client.bl_id).daysLeftInSchool = CityRPGData.getData(%client.bl_id).valueEducation + 1;
			
		messageClient(%client, '', "\c6You are now enrolled.");
		%client.updateHUD();
	}
	else if(%do $= "level")
	{
		messageClient(%client, '', "\c6You have a level \c3" @ %client.getEducationLevel() @ "\c6 education.");
	}
	else
	{
		if(%client.getEducationLevel() < $CityRPG::pref::maxEducationLevel)
		{
			messageClient(%client, '', "\c6It will cost you \c3$" @ %price @ "\c6 to reach your next diploma.");
			messageClient(%client, '', "\c6 - Type \"\c3/education accept\c6\" to accept.");
			messageClient(%client, '', "\c6 - Type \"\c3/education level\c6\" to see your current diploma.");
		}
		else
		{
			messageClient(%client, '', "\c6You have a level \c3" @ %client.getEducationLevel() @ "\c6 education, the highest there is to offer.");
		}
	}
}

package CityRPG_EducationPackage
{
	function gameConnection::tick(%client)
	{
		parent::tick(%client);
		
		if(%client.isStudent())
		{
			CityRPGData.getData(%client.bl_id).valuedaysLeftInSchool--;
			
			if(!%client.isStudent())
			{
				%client.addEducationLevels(1);
				messageClient(%client, '', '\c6 - \c2You graduated\c6, receiving a level \c3%1\c6 diploma!', %client.getEducationLevel());
				
			}
			else
				messageClient(%client, '', '\c6 - Only \c3%1\c6 days left until you graduate.', CityRPGData.getData(%client.bl_id).valuedaysLeftInSchool);
		}
	}
};
activatePackage(CityRPG_EducationPackage);

