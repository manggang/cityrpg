exec("./marijuana.cs");
exec("./opium.cs");
exec("./knife.cs");

// Misc Drug Prefs
$CityRPG::drug::minSellSpeed = 1; // In seconds
$CityRPG::drug::maxSellSpeed = 10; // In seconds
$CityRPG::drug::minBuyAmt = 1; // Minimum grams of weed player is capable of selling // Grams 1 - 5 have special names
$CityRPG::drug::maxBuyAmt = 5; // Maximum ^
$CityRPG::drug::sellPrice = 10; // About the real value of a gram of weed in the US // The actual price randomly changes by a couple digits
$CityRPG::drug::maxdrugplants = 6;
$CityRPG::drug::sellTimes = 50;
$CityRPG::drug::demWorth = 3; // The amount of dems each gram is worth. If their grams are worth the wanted limit or higher, they can be jailed.

// Drug Evidence Prefs
$CityRPG::drug::evidenceWorth = 500; // How much someone can turn in drug evidence for

package CityRPG_DrugPackage
{
	function fxDTSBrick::onActivate(%brick, %obj, %client, %pos, %dir)
	{
		parent::onActivate(%brick, %obj, %client, %pos, %dir);
		if(%brick.getDataBlock().hasDrug == false)
	  	{
			if(%brick.getDataBlock().isDrug == true)
			{
		  		%drug = %brick.getID();
	      		if(%drug.watered == 0)
	  	  		{
				 	%drug.watered = 1;
					
					if(%client.bl_id == %brick.client.bl_id)
					{
						messageClient(%client,'',"\c6You watered your \c3" @ %brick.getDataBlock().uiName @ " \c6plant.");
					}
					else
					{
						messageClient(%client,'',"\c6You watered someones \c3" @ %brick.getDataBlock().uiName @ " \c6plant.");
					}
					
				 	%drug.startGrowing();
	  	  		}
	  	  		else
				{
					if(%drug.hasDrug)
						commandToClient(%client,'centerPrint',"\c6This \c3" @ %brick.getDataBlock().uiName @ " \c6plant is ready to be harvested!",1);
					else
		      			commandToClient(%client,'centerPrint',"\c6This plant is already watered.",1);
				}
			}
		}
	}
	
	function fxDTSBrick::onDeath(%brick)
	{
		if(%brick.getDataBlock().isDrug)
		{
			CityRPGData.getData(%brick.owner).valuedrugamount--;
			
			if(isObject(getBrickGroupFromObject(%brick).client))
			{
				getBrickGroupFromObject(%brick).client.updateHUD();
			}
		}
		parent::onDeath(%brick);
	}
};
activatePackage(CityRPG_DrugPackage);


function startSelling(%client)
{
	%drugname = %client.drugname;
	if(%drugname $= "Marijuana")
	{
		%amount = CityRPGData.getData(%client.bl_id).valueMarijuana;
	}

	else if(%drugname $= "Opium")
	{
		%amount = CityRPGData.getData(%client.bl_id).valueOpium;
	}
	
	if(%amount > 0)
	{
		%buymin = $CityRPG::drug::minBuyAmt;
		%buymax = $CityRPG::drug::maxBuyAmt;
		%grams = getRandom(%buymin,%buymax);
		
		if(%grams > %amount)
		{
			%grams = %amount;
		}
		else if(%grams == 0)
		{
			messageClient(%client,'',"\c6You're all out!");
			return;
		}
		
		%grams = mFloor(%grams);
		
		if(%drugname $= "marijuana")
		{
			%profit = $CityRPG::drugs::marijuana::basePrice;
		}
		
		else if(%drugname $= "opium")
		{
			%profit = $CityRPG::drugs::opium::basePrice;
		}
		
		%totalcash = %grams * %profit;
		
		%randomize = getRandom(1,2);
		if(%randomize == 1) { %totalcash -= getRandom(0.75,1); }
		else if(%randomize == 2) { %totalcash += getRandom(1,1.25); }
		%totalcash = mFloor(%totalcash);
		
		%client.addMoney(%totalcash);
		
		%slang = %grams;
		switch(%slang)
		{
			case 1:
				%slang = "a \c3gram\c6 of";
			case 2:
				%slang = "a \c3dimebag\c6 of";
			case 3:
				%slang = "\c3three grams\c6 of";
			case 4:
				%slang = "a \c3dub\c6 of";
			case 5:
				%slang = "\c3five grams\c6 of";
			default:
				%slang = "some";
		}
		%client.updateHUD();
		messageClient(%client,'',"\c6You sold " @ %slang @ " " @ %drugname @ " to a stranger for \c3$" @ %totalcash @"\c6.");
		
		if(%drugname $= "marijuana")
		{
			CityRPGData.getData(%client.bl_id).valuemarijuana -= %grams;
			CityRPGData.getData(%client.bl_id).valuetotaldrugs -= %grams;
		}
		
		else if(%drugname $= "opium")
		{
			CityRPGData.getData(%client.bl_id).valueopium -= %grams;
			CityRPGData.getData(%client.bl_id).valuetotaldrugs -= %grams;
		}
	}
	else
	messageClient(%client,'',"\c6You're all out!");
	return;
}


function CityRPG_DoDrugLoop(%loop2,%client)
{
	%drugtime = (((getRandom($CityRPG::drug::minSellSpeed,$CityRPG::drug::maxSellSpeed)) * 1000) / CityRPGData.dataCount);
	
	if(isObject(%client = findClientByBL_ID(CityRPGData.data[%loop2].ID)))
		%client.drugtick();
		
	if(%client.selling)
	{
		startSelling(%client);
	}
	
	if(%loop2 < CityRPGData.dataCount)
		schedule(%drugtime, false, "CityRPG_DoDrugLoop", (%loop2 + 1));
}

function Drug_Tick(%client)
{
	CityRPGData.lastDrugTickOn = $Sim::DTime;
	
	CityRPG_DoDrugLoop(0);
	
	if(CityRPGData.scheduleDrug)
		cancel(CityRPGData.scheduleDrug);

	CityRPGData.scheduleDrug = schedule(((getRandom($CityRPG::drug::minSellSpeed,$CityRPG::drug::maxSellSpeed)) * 1000), false, "Drug_Tick");
	
	startSelling(%client);
}

Drug_Tick();

function servercmddrugamount(%client)
{
	messageClient(%client,'',CityRPGData.getData(%client.bl_id).valuedrugamount);
}

function fxDTSBrick::bagPlant(%col)
{
	%col.schedule(0, "delete");
	
	CityRPGData.getData(%col.owner).valuedrugamount--;
			
	if(isObject(getBrickGroupFromObject(%col).client))
	{
		getBrickGroupFromObject(%col).client.updateHUD();
	}
}

function fxDTSBrick::startGrowing()
{
	%drugtime = ((%this.growthTime * 60000) / 8);
	
	%this.canchange = true;
	%this.setEmitter(None);
	%this.currentColor = 54 + %this.ticksGrownFor;
	%this.setColor(54 + %this.ticksGrownFor);
	%this.canbecolored = false;
	%this.canchange = false;
	
	if(%this.ticksGrownFor < %this.growthTime)
	{
		%this.hasemitter = false;
		%this.ticksGrownFor++;
		%this.schedule(%drugtime, "startGrowing");
		return;
	}

	%drug.onFullyGrown();
}

function fxDTSBrick::onFullyGrown()
{
	%this.canChange = true;
	
	%this.health = 0;
	%this.hasDrug = true;
	%this.setColor(61);
	%this.cansetemitter = true;
	%this.emitter = "GrassEmitter";
	%this.setEmitter(GrassEmitter);
	%this.cansetemitter = false;
	%this.hasemitter = true;
	
	%this.canchange = false;
}

function fxDTSBrick::harvest(%client)
{	
	%brickData = %this.getDatablock();
	if(%this.hasDrug)
	{
		if(%this.health < %this.random)
		{
			%this.health++;
			%percentage = mFloor((%this.health / %this.random) * 100);
			
			// cool color effect
			if(%percentage >= 0 && %percentage < 10)
				%color = "<color:ff0000>";
			else if(%percentage >= 10 && %percentage < 20)
				%color = "<color:ff2200>";
			else if(%percentage >= 10 && %percentage < 30)
				%color = "<color:ff4400>";
			else if(%percentage >= 10 && %percentage < 40)
				%color = "<color:ff6600>";
			else if(%percentage >= 10 && %percentage < 50)
				%color = "<color:ff8800>";
			else if(%percentage >= 10 && %percentage < 60)
				%color = "<color:ffff00>";
			else if(%percentage >= 10 && %percentage < 70)
				%color = "<color:88ff00>";
			else if(%percentage >= 10 && %percentage < 80)
				%color = "<color:66ff00>";
			else if(%percentage >= 10 && %percentage < 90)
				%color = "<color:44ff00>";
			else if(%percentage >= 10 && %percentage < 100)
				%color = "<color:22ff00>";
			else if(%percentage == 100)
				%color = "<color:00ff00>";
				
			commandToClient(%client,'centerPrint',"\c3" @ %brickData.uiName @ " \c6harvested: %" @ %color @ "" @ %percentage,3);
		}
		else
		{
			if(%brickData.drugType $= "Opium")
			{
				%harvestamt = getRandom(%brickData.harvestMin, %brickData.harvestMax);
				CityRPGData.getData(%client.bl_id).valueopium += %harvestamt;
				CityRPGData.getData(%client.bl_id).valuetotaldrugs += %harvestamt;
				if(%client.bl_id == %this.owner)
				{
					commandToClient(%client,'centerPrint',"\c6You harvested \c3" @ %harvestamt @ "\c6 grams of \c3Opium\c6.",3);
				}
				else
				{
					commandToClient(%client,'centerPrint',"\c6You harvested \c3" @ %harvestamt @ "\c6 grams of someone elses \c3Opium\c6.",3);
				}
				
				%this.canchange = true;
				
				%this.currentColor = 45;
				%this.setColor(45);
				%this.health = 0;
				%this.ticksGrownFor = 0;
				%this.canbecolored = false;
				%this.watered = false;
				%this.random = getRandom(%brickData.harvestMin, %brickData.harvestMax);
				%this.hasDrug = false;
				%this.cansetemitter = true;
				%this.setEmitter(None);
				%this.cansetemitter = false;
				
				%this.canchange = false;

				%client.updateHUD();
			}
			else if(%brickData.drugType $= "Marijuana")
			{
				%harvestamt = getRandom(%brickData.harvestMin, %brickData.harvestMax);
				CityRPGData.getData(%client.bl_id).valuemarijuana += %harvestamt;
				CityRPGData.getData(%client.bl_id).valuetotaldrugs += %harvestamt;
				if(%client.bl_id == %this.owner)
				{
					commandToClient(%client,'centerPrint',"\c6You harvested \c3" @ %harvestamt @ "\c6 grams of \c3Marijuana\c6.",3);
				}
				else
				{
					commandToClient(%client,'centerPrint',"\c6You harvested \c3" @ %harvestamt @ "\c6 grams of someone elses \c3Marijuana\c6.",3);
				}
				
				%this.canchange = true;
				%this.currentColor = 45;
				%this.setColor(45);
				%this.health = 0;
				%this.canbecolored = false;
				%this.watered = false;
				%this.random = getRandom(%brickData.harvestMin, %brickData.harvestMax);
				%this.hasDrug = false;
				%this.cansetemitter = true;
				%this.setEmitter(None);
				%this.cansetemitter = false;
				%this.canchange = false;

				%client.updateHUD();

			}
		}
	}
	//else
	//commandToClient(%client,'centerPrint',"\c6This plant isn't ready to harvest yet!",1);
}

function fxDtsBrick::onDrugPlaced(%owner)
{
	CityRPGData.getData(%owner.bl_id).valuedrugamount++;
	%this.setColor(45);
	%this.owner = %owner.bl_id;

	if(%this.getDataBlock().drugType $= "marijuana")
	{
		%this.random = getRandom(%this.getDatablock().harvestMin, %this.getDatablock().harvestMax);
		messageClient(%owner, '', "\c6You have paid \c3$" @ mFloor(%this.getDatablock().price) @ "\c6 to plant a \c3Marijuana\c6 brick.");
	}
	else if(%drug.getDataBlock().drugType $= "opium")
	{
		%this.random = getRandom(%this.getDatablock().harvestMin, %this.getDatablock().harvestMax);
		messageClient(%owner, '', "\c6You have paid \c3$" @ mFloor(%this.getDatablock().price) @ "\c6 to plant an \c3Opium\c6 brick.");
	}
	
	%this.setEmitter("None");
	%this.emitter = "GrassEmitter";
}

function servercmdmydrugs(%client)
{
	messageClient(%client,'',"\c6Your marijuana in grams :" @ CityRPGData.getData(%client.bl_id).valueMarijuana);
	messageClient(%client,'',"\c6Your opium in grams: " @ CityRPGData.getData(%client.bl_id).valueopium);
	messageClient(%client,'',"\c6Your total drugs in grams: " @ CityRPGData.getData(%client.bl_id).valuetotaldrugs);
}

function servercmddrughelp(%client)
{
	messageClient(%client,'',"\c6- \c3How to grow drugs for dummies\c6 -");
	messageClient(%client,'',"\c3Step 1\c6: Navigate to the City RPG tab in the brick menu");
	messageClient(%client,'',"\c3Step 2\c6: Scroll down until you find the drug bricks");
	messageClient(%client,'',"\c3Step 3\c6: Select a drug and place it in your City RPG Lot");
	messageClient(%client,'',"\c3Step 4\c6: Click your drug brick to water it");
	messageClient(%client,'',"\c3Step 5\c6: Wait a few in-game days");
	messageClient(%client,'',"\c3Step 6\c6: Find/buy a knife");
	messageClient(%client,'',"\c3Step 7\c6: Harvest your drug brick with your knife");
	messageClient(%client,'',"\c3Step 8\c6: Find a drug sell brick placed around the city");
	messageClient(%client,'',"\c3Step 9\c6: Don't get caught!");
	messageClient(%client,'',"\c6---");
	messageClient(%client,'',"\c3Tip\c6: Having a lot of drugs on you and getting batoned will get you jail time!");
	messageClient(%client,'',"\c3Tip\c6: Cops can baton your crops and turn them in as evidence, so hide them well!");
}
				  

// more drugs
function gameConnection::drugtick(%client)
{
	if(%client.selling)
	{
		startSelling(%client);
	}
	%client.updateHUD();
}

datablock ParticleData(DrugsmokeParticle)
{
   dragCoefficient      = 1.0;
   gravityCoefficient   = -0.2;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 1.0;
   lifetimeMS           = 8000;
   lifetimeVarianceMS   = 300;
   useInvAlpha          = true;
   textureName          = "Add-Ons/Gamemode_CityRPG/shapes/cloud";
   colors[0]     = "1.0 1.0 1.0 1.0";
   colors[1]     = "1.0 1.0 1.0 1.0";
   colors[2]     = "1.0 1.0 1.0 0.0";
   sizes[0]      = 1.5;
   sizes[1]      = 1.5;
   sizes[2]      = 1.5;
   times[0]      = 0.0;
   times[1]      = 0.2;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(DrugsmokeEmitter)
{
   ejectionPeriodMS = 20;
   periodVarianceMS = 0;
   ejectionVelocity = 0.2;
   ejectionOffset   = 1.5;
   velocityVariance = 0.19;
   thetaMin         = 0;
   thetaMax         = 30;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvance = false;
   particles = "DrugsmokeParticle";

   uiName = "Drugsmoke";
};
datablock ShapeBaseImageData(DrugsmokeImage)
{
   shapeFile = "base/data/shapes/empty.dts";

	mountPoint = $HeadSlot;

	stateName[0]					= "Ready";
	stateTransitionOnTimeout[0]		= "FireA";
	stateTimeoutValue[0]			= 0.06;

	stateName[1]					= "FireA";
	stateTransitionOnTimeout[1]		= "Done";
	stateWaitForTimeout[1]			= True;
	stateTimeoutValue[1]			= 0.9;
	stateEmitter[1]					= DrugsmokeEmitter;
	stateEmitterTime[1]				= 0.9;

	stateName[2]					= "Done";
	stateScript[2]					= "onDone";
};

datablock AudioProfile(Smokingsound)
{
   filename    = "Add-Ons/Gamemode_CityRPG/sounds/smokedaweed.wav";
   description = AudioClose3d;
   preload = true;
};

function servercmdusemarijuana(%client)
{
    %time = getSimTime();
    if(%client.lastTimeusemarijuana $= "" || %time - %client.lastTimeusemarijuana > 5000)
	{
		if(isObject(%client.player))
		{
			if(CityRPGData.getData(%client.bl_id).valueMarijuana > 0)
			{
				serverplay3d(Smokingsound,%client.player.getHackPosition() SPC "0 0 1 0");
			
				echo(%client.name @ " smoke");
				%client.player.setWhiteout(1);
				%client.player.emote(DrugsmokeImage);
				messageClient(%client,'',"\c6Used \c31 gram \c6of \c3marijuana\c6.");
				CityRPGData.getData(%client.bl_id).valueMarijuana--;
				%client.lastTimeusemarijuana = %time;
			}
			else
			messageClient(%client,'',"\c6You don't have any.");
		}	
		else
		messageClient(%client,'',"\c6You must spawn first.");
	}
}

function DrugsmokeImage::onDone(%this,%obj,%slot)
{
	%obj.unMountImage(%slot);
}