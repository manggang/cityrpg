$CityRPG::drugs::opium::basePrice = getRandom(12,16); // Price per gram

datablock fxDTSBrickData(CityRPGOpiumData : CityRPGMariData)
{
	iconName = "Add-Ons/GameMode_CityRPG/shapes/BrickIcons/opium";
	
	uiName = "Opium";
	drugType = "Opium";

	price = 3000;
	harvestMin = 11;
	harvestMax = 18;
};