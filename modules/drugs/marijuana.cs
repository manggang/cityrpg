$CityRPG::drugs::marijuana::basePrice = getRandom(9,11); // Price per gram

datablock fxDTSBrickData(CityRPGMariData)
{
	brickFile = "Add-Ons/GameMode_CityRPG/shapes/Bricks/brick_6x6.blb";
	iconName = "Add-Ons/GameMode_CityRPG/shapes/BrickIcons/cannabis";
	
	category = "CityRPG";
	subCategory = "CityRPG Drugs";
	
	uiName = "Marijuana";
	drugType = "Marijuana";
	
	owner = 0;
	
	canchange = false;
	cansetemitter = false;
	canbecolored = false;
	
	emitter = "GrassEmitter";
	isDrug = true;
	hasDrug = false;
	ticksGrownFor = 0;
	health = 100;
	orighealth = 100;
	
	watered = 0;
	price = 1800;
	harvestMin = 9;
	harvestMax = 14;
	growthTime = 8;
	
	CityRPGBrickType = 420;
	CityRPGBrickAdmin = false;
};