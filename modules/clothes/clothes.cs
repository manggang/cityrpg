
function gameConnection::applyForcedBodyColors(%client)
{
	if(isObject(CityRPGData.getData(%client.bl_id)))
	{
		if(%client.getDaysLeftInJail())
			%outfit = "none none none none jumpsuit jumpsuit skin jumpsuit jumpsuit";
		else if(%client.getJobSO().outfit !$= "")
			%outfit = %client.getJobSO().outfit;
		else
			%outfit = CityRPGData.getData(%client.bl_id).valueOutfit;
	}
	
	if(%outfit !$= "")
	{
		%client.accentColor		= ClothesSO.getColor(%client, getWord(%outfit, 0));
		%client.hatColor		= ClothesSO.getColor(%client, getWord(%outfit, 1));
		
		%client.packColor		= ClothesSO.getColor(%client, getWord(%outfit, 2));
		%client.secondPackColor		= ClothesSO.getColor(%client, getWord(%outfit, 3));
		
		%client.chestColor		= ClothesSO.getColor(%client, getWord(%outfit, 4));
		
		%client.rarmColor		= ClothesSO.getColor(%client, getWord(%outfit, 5));
		%client.larmColor		= ClothesSO.getColor(%client, getWord(%outfit, 5));
		%client.rhandColor		= ClothesSO.getColor(%client, getWord(%outfit, 6));
		%client.lhandColor		= ClothesSO.getColor(%client, getWord(%outfit, 6));
		
		%client.hipColor		= ClothesSO.getColor(%client, getWord(%outfit, 7));
		
		%client.rlegColor		= ClothesSO.getColor(%client, getWord(%outfit, 8));
		%client.llegColor		= ClothesSO.getColor(%client, getWord(%outfit, 8));
		
		%client.applyBodyColors();
	}
}

function serverCmdUpdateBodyColors(%client, %headColor)
{
	// The only thing we want from this command is the facial color, which determines skin color in the clothing mod.
	%client.headColor = %headColor;
	%client.applyForcedBodyColors();
}

function gameConnection::applyForcedBodyParts(%client)
{
	if(isObject(CityRPGData.getData(%client.bl_id)))
	{
		if(%client.getDaysLeftInJail())
			%outfit = "none none none none jumpsuit jumpsuit skin jumpsuit jumpsuit";
		else if(%client.getJobSO().outfit !$= "")
			%outfit = %client.getJobSO().outfit;
		else
			%outfit = CityRPGData.getData(%client.bl_id).valueOutfit;
	}
	
	if(%outfit !$= "")
	{
		%client.accent		= ClothesSO.getNode(%client, getWord(%outfit, 0));
		%client.hat		= ClothesSO.getNode(%client, getWord(%outfit, 1));
		
		%client.pack		= ClothesSO.getNode(%client, getWord(%outfit, 2));
		%client.secondPack	= ClothesSO.getNode(%client, getWord(%outfit, 3));
		
		%client.chest		= ClothesSO.getNode(%client, getWord(%outfit, 4));
		
		%client.rarm		= ClothesSO.getNode(%client, getWord(%outfit, 5));
		%client.larm		= ClothesSO.getNode(%client, getWord(%outfit, 5));
		%client.rhand		= ClothesSO.getNode(%client, getWord(%outfit, 6));
		%client.lhand		= ClothesSO.getNode(%client, getWord(%outfit, 6));
		
		%client.hip		= ClothesSO.getNode(%client, getWord(%outfit, 7));
		
		%client.rleg		= ClothesSO.getNode(%client, getWord(%outfit, 8));
		%client.lleg		= ClothesSO.getNode(%client, getWord(%outfit, 8));
		
		%client.faceName 	= ClothesSO.getDecal(%client, "face", getWord(%outfit, 9));
		%client.decalName 	= ClothesSO.getDecal(%client, "chest", getWord(%outfit, 10));
		
		%client.applyBodyParts();
	}
}


function ClothesSO::loadClothes(%so)
{
	// Clothing Data
	%so.color["none"]		= "1 1 1 1";
	%so.node["none"]		= "0";
	
	// Outfits
	// Outfits use index instead of names.
	// Do not repeat indexes.
	// This is the order they appear in the GUI.
	%so.str[1]	= "none none none none whitet whitet skin bluejeans blackshoes default default";
	%so.uiName[1]	= "Default";
	%so.sellName[1]	= "Default Suit";

	%so.str[2]	= "none brownhat keep keep greenshirt greenshirt keep greyPants blackshoes default default";
	%so.uiName[2]	= "Basic";
	%so.sellName[2]	= "Basic Outfit";
	
	%so.str[3]	= "keep skullcap keep keep blackshirt blackshirt blackgloves blackPants blackshoes default default";
	%so.uiName[3]	= "Gimp";
	%so.sellName[3]	= "Gimp Suit";
	
	%so.str[4]	= "none none none none whitet redsleeve keep brightbluePants blueshoes default default";
	%so.uiName[4]	= "Blockhead";
	%so.sellName[4]	= "Blockhead Clothes";

	%so.str[5]	= "keep keep keep keep greenshirt greenshirt keep brownPants blackshoes default worm-sweater";
	%so.uiName[5]	= "Nerd";
	%so.sellName[5]	= "Nerd Suit";

	%so.str[6]	= "keep keep keep keep blackshirt blackshirt keep blackPants blackshoes default Mod-Suit";
	%so.uiName[6]	= "Business";
	%so.sellName[6]	= "Business Suit";

	%so.str[7]	= "keep keep keep keep blueshirt blueshirt keep bluePants blackshoes default Mod-Suit";
	%so.uiName[7]	= "Council";
	%so.sellName[7]	= "Council Suit";

	%so.str[8]	= "keep keep keep keep skingen skingen skingen skingen skingen default default";
	%so.uiName[8]	= "Naked";
	%so.sellName[8]	= "B-Day Suit";

	// Hats
	%so.color["brownhat"]	= "0.329 0.196 0.000 1.000";
	%so.node["brownhat"]	= "4";
	%so.str["brownhat"]	= "keep this keep keep keep keep keep keep keep";
	
	%so.color["piratehat"]	= "0.078 0.078 0.078 1";
	%so.node["piratehat"]	= "5";
	%so.str["piratehat"]	= "keep this keep keep keep keep keep keep keep";
	
	%so.color["copHat"]	= "0 0.141176 0.333333 1";
	%so.node["copHat"]	= "6";
	%so.str["copHat"]	= "keep this keep keep keep keep keep keep keep";
	
	%so.color["skullcap"]	= "0.200 0.200 0.200 1.000";
	%so.node["skullcap"]	= "7";
	%so.str["skullcap"]	= "keep this keep keep keep keep keep keep keep";
	
	%so.color["aokicap"]	= "0.990654 0.962617 0.686916 1";
	%so.node["aokicap"]	= "7";
	%so.str["aokicap"]	= "keep this keep keep keep keep keep keep keep";
	
	%so.color["ndHat"]	= "0.200 0.200 0.200 1.000";
	%so.node["ndHat"]	= "6";
	%so.str["ndHat"]	= "keep this keep keep keep keep keep keep keep";
	
	// Gloves
	%so.color["blackgloves"] = "0.200 0.200 0.200 1.000";
	%so.node["blackgloves"]	= "0";
	%so.str["blackgloves"]	= "keep keep keep keep keep keep this keep keep";
	
	// Shirts
	%so.color["pinkt"]	= "1 0.75 0.79 1";
	%so.node["pinkt"]	= "gender";
	%so.str["pinkt"]	= "keep keep keep keep this this keep keep keep";
	
	%so.color["aokishirt"]	= "0.621495 0.859813 0.976636 1";
	%so.node["aokishirt"]	= "gender";
	%so.str["aokishirt"]	= "keep keep keep keep this this keep keep keep";
	
	%so.color["aokisleeve"]	= "0.934579 0.257009 0.266355 1";
	%so.node["aokisleeve"]	= "gender";
	%so.str["aokisleeve"]	= "keep keep keep keep this this keep keep keep";
	
	%so.color["pinktank"]	= "1 0.07 0.57 1";
	%so.node["pinktank"]	= "gender";
	%so.str["pinktank"]	= "keep keep keep keep this skingen keep keep keep";
	
	%so.color["whitet"]	= "1 1 1 1";
	%so.node["whitet"]	= "gender";
	%so.str["whitet"]	= "keep keep keep keep this this keep keep keep";
	
	%so.color["copShirt"]	= "0 0.141176 0.333333 1";
	%so.node["copShirt"]	= "gender";
	%so.str["copShirt"]	= "keep keep keep keep this this keep keep keep";
	
	%so.color["jumpsuit"]	= "1 0.617 0 1";
	%so.node["jumpsuit"]	= "gender";
	%so.str["jumpsuit"]	= "keep keep keep keep this this keep this this";
	
	%so.color["blackshirt"]	= "0.200 0.200 0.200 1.000";
	%so.node["blackshirt"]	= "gender";
	%so.str["blackshirt"]	= "keep keep keep keep this this keep keep keep";
	
	%so.color["brownshirt"]	= "0.329 0.196 0.000 1.000";
	%so.node["brownshirt"]	= "gender";
	%so.str["brownshirt"]	= "keep keep keep keep this this keep keep keep";
	
	%so.color["greenshirt"]	= "0.00 0.262 0.00 1";
	%so.node["greenshirt"]	= "gender";
	%so.str["greenshirt"]	= "keep keep keep keep this this keep keep keep";

	%so.color["blueshirt"]	= "0.0 0.141 0.333 1";
	%so.node["blueshirt"]	= "gender";
	%so.str["blueshirt"]	= "keep keep keep keep this this keep keep keep";

	// Pants
	%so.color["bluejeans"]	= "0 0.141 0.333 1";
	%so.node["bluejeans"]	= "0";
	%so.str["bluejeans"]	= "keep keep keep keep keep keep keep this keep";
	
	%so.color["aokipants"]	= "0.621495 0.859813 0.976636 1";
	%so.node["aokipants"]	= "0";
	%so.str["aokipants"]	= "keep keep keep keep keep keep keep this keep";
	
	%so.color["blackPants"] = "0.200 0.200 0.200 1.000";
	%so.node["blackPants"]	= "0";
	%so.str["blackPants"]	= "keep keep keep keep keep keep keep this keep";
	
	%so.color["brownPants"] = "0.329 0.196 0.000 1.000";
	%so.node["brownPants"]	= "0";
	%so.str["brownPants"]	= "keep keep keep keep keep keep keep this keep";
	
	%so.color["greyPants"] = "0.300 0.300 0.300 1.000";
	%so.node["greyPants"]	= "0";
	%so.str["greyPants"]	= "keep keep keep keep keep keep keep this keep";

	%so.color["brightbluePants"] = "0.200 0.000 0.800 1.000";
	%so.node["brightbluePants"]	= "0";
	%so.str["brightbluePants"]	= "keep keep keep keep keep keep keep this keep";

	%so.color["bluePants"] = "0.0 0.141 0.333 1";
	%so.node["bluePants"]	= "0";
	%so.str["bluePants"]	= "keep keep keep keep keep keep keep this keep";

	// Shoes
	%so.color["blackshoes"]	= "0.200 0.200 0.200 1.000";
	%so.node["blackshoes"]	= "0";
	%so.str["blackshoes"]	= "keep keep keep keep keep keep keep keep this";

	%so.color["aokishoes"]	= "0.121495 0.598131 0.831776 1";
	%so.node["aokishoes"]	= "0";
	%so.str["aokishoes"]	= "keep keep keep keep keep keep keep keep this";

	%so.color["brownshoes"]	= "0.329 0.196 0.000 1.000";
	%so.node["brownshoes"]	= "0";
	%so.str["brownshoes"]	= "keep keep keep keep keep keep keep keep this";

	%so.color["blueshoes"]	= "0.200 0.000 0.800 1.000";
	%so.node["blueshoes"]	= "0";
	%so.str["blueshoes"]	= "keep keep keep keep keep keep keep keep this";

	//Misc
	%so.color["redsleeve"] = "0.900 0.220 0.000 1.000";
	%so.node["redsleeve"] = "gender";
	%so.color["redsleeve"] = "keep keep keep keep this this keep keep keep";
}

function ClothesSO::postEvents(%so)
{
	%str = "list";
	
	for(%a = 1; %so.str[%a] !$= ""; %a++)
		%str = %str SPC %so.uiName[%a] SPC %a;
	
	if(%str !$= "")
	{
		registerOutputEvent("fxDTSBrick", "sellClothes", %str TAB "int 0 500 1");
		
		%clientCount = ClientGroup.getCount();
		for(%b = 0; %b < %clientCount; %b++)
		{
			%subClient = ClientGroup.getObject(%b);
			serverCmdRequestEventTables(%subClient);
			messageClient(%subClient, '', "\c6Your Event Tables have been updated. If you do not know what that means, ignore this message.");
		}
	}
}

function ClothesSO::getColor(%so, %client, %item)
{
	if(%item $= "skin" || %item $= "skingen")
		return %client.headColor;
	else
	{
		%color = %so.color[%item];
		return %color;
	}
}

function ClothesSO::getNode(%so, %client, %item)
{
	if(%item $= "skin")
		return 0;
	else if(%item $= "skingen")
		return (%client.gender $= "Male" ? 0 : 1);
	else
	{
		%node = %so.node[%item];
	
		if(%node $= "gender" || %node $= "skingen")
			return (%client.gender $= "Male" ? 0 : 1);
		else
			return %node;
	}
}

function ClothesSO::getDecal(%so, %client, %segment, %item)
{
	if(%item $= "" || %item $= "default")
	{
		if(%segment $= "face")
			return "smiley";
		else if(%segment $= "chest")
			return "AAA-none";
	}
	else
		return %item;
}

function ClothesSO::giveItem(%so, %client, %item)
{
	if(strLen(%so.str[%item]) && isObject(%client))
	{
		%outfit = CityRPGData.getData(%client.bl_id).valueOutfit;
		
		for(%a = 0; %a < getWordCount(%outfit); %a++)
		{
			if(getWord(%so.str[%item], %a) $= "keep")
				%newOutfit = (%newOutfit $= "" ? getWord(%outfit, %a) : %newOutfit SPC getWord(%outfit, %a));
			else if(getWord(%so.str[%item], %a) $= "this")
				%newOutfit = (%newOutfit $= "" ? %item : %newOutfit SPC %item);
			else
				%newOutfit = (%newOutfit $= "" ? getWord(%so.str[%item], %a) : %newOutfit SPC getWord(%so.str[%item], %a));
		}
		
		CityRPGData.getData(%client.bl_id).valueOutfit = %newOutfit;
		%client.applyBodyParts();
		%client.applyBodyColors();
	}
}

if(!isObject(ClothesSO))
{
	new scriptObject(ClothesSO) { };
	ClothesSO.schedule(1, "loadClothes");
	ClothesSO.schedule(1, "postEvents");
}