$CityRPG::foodPortion[1] = "Small";
$CityRPG::foodPortion[2] = "Medium";
$CityRPG::foodPortion[3] = "Large";
$CityRPG::foodPortion[4] = "Extra_Large";
$CityRPG::foodPortion[5] = "Super_Sized";
$CityRPG::foodPortion[6] = "Americanized";

$CityRPG::hunger::stateCount = 0;

$CityRPG::hunger::color[0] = "FFAA00";
$CityRPG::hunger::state[0] = "Whale-Like";

$CityRPG::hunger::color[$CityRPG::hunger::stateCount++] = "00FF00";
$CityRPG::hunger::state[$CityRPG::hunger::stateCount] = "Bursting";

$CityRPG::hunger::color[$CityRPG::hunger::stateCount++] = "22FF00";
$CityRPG::hunger::state[$CityRPG::hunger::stateCount] = "Obese";

$CityRPG::hunger::color[$CityRPG::hunger::stateCount++] = "44FF00";
$CityRPG::hunger::state[$CityRPG::hunger:stateCount] = "Engorged";

$CityRPG::hunger::color[$CityRPG::hunger::stateCount++] = "66FF00";
$CityRPG::hunger::state[$CityRPG::hunger::stateCount] = "Bloated";

$CityRPG::hunger::color[$CityRPG::hunger::stateCount++] = "88FF00";
$CityRPG::hunger::state[$CityRPG::hunger::stateCount] = "Stuffed";

$CityRPG::hunger::color[$CityRPG::hunger::stateCount++] = "AAFF00";
$CityRPG::hunger::state[$CityRPG::hunger::stateCount] = "Overfed";

$CityRPG::hunger::color[$CityRPG::hunger::stateCount++] = "DDFF00";
$CityRPG::hunger::state[$CityRPG::hunger::stateCount] = "Full-up";

$CityRPG::hunger::color[$CityRPG::hunger::stateCount++] = "FFFF00";
$CityRPG::hunger::state[$CityRPG::hunger::stateCount] = "Satisfied";

$CityRPG::hunger::color[$CityRPG::hunger::stateCount++] = "FFBB00";
$CityRPG::hunger::state[$CityRPG::hunger::stateCount] = "Content";

$CityRPG::hunger::color[$CityRPG::hunger::stateCount++] = "FF6600";
$CityRPG::hunger::state[$CityRPG::hunger::stateCount] = "Hungry";

$CityRPG::hunger::color[$CityRPG::hunger::stateCount++] = "FF0000";
$CityRPG::hunger::state[$CityRPG::hunger::stateCount] = "Starving";

AddDamageType("Starvation", '\c6 - %1 died of starvation.', '\c6 - %1 died of starvation.', 0.5, 1);

package CityRPG_HungerPackage
{
	function gameConnection::onDeath(%client, %killerPlayer, %killer, %damageType, %unknownA)
	{
		if (%damageType == $DamageType::starvation)
		{
			%client.setHunger(8);
		}
		parent::onDeath(%client, %killerPlayer, %killer, %damageType, %unknownA);
	}	
		
	function player::setScale(%this, %scale, %client)
	{
		if(isObject(%this.client))
		{
			if(%this.client.getHunger() <= 0)
			{
				%scale = "1.225 1.225 1";
			}
		
			else if(%this.client.getHunger() >= 5)
			{
				%scale = "0.75 0.75 1";
			}
			else
			{
				%scale = "1 1 1";
			}
		}
		
		parent::setScale(%this, %scale);
	}
	
	function gameConnection::tick(%client)
	{
		CityRPG_HungerTick(%client);
		parent::tick(%client);
	}
};
activatePackage(CityRPG_HungerPackage);

function CityRPG_HungerTick(%client)
{
	// Code that only affects people whom have spawned.
	if(%client.hasSpawnedOnce)
	{
		// ==== food ==== \\
		if((CalendarSO.date % 2) == 0)
		{
			%client.addHunger(1);
			
			if(isObject(%client.player))
				%client.player.setScale("1 1 1");
			
			if(%client.getHunger() == $CityRPG::hunger::statecount - 1)
				commandToClient(%client, 'messageBoxOK', "You are starving to death!", "You're close to starvation!\nIf you do not eat before the next tick, you will die!");
			else if(%client.getHunger() == $CityRPG::hunger::statecount)
			{
				if(isObject(%client.player))
				{
					%client.player.damage(%client.player, "0 0 0", (%client.player.getDatablock().maxDamage * 3), $DamageType::Starvation);
				}
				return;
			}
		}
	}	
}

function gameConnection::getHunger(%client)
{
	return mFloor(CityRPGData.getData(%client.bl_id).valueHunger);
}

function gameConnection::setHunger(%client, %amount)
{
	if (%amount < 0)
	{
		CityRPG.getData(%client.bl_id).valueHunger = 0;
	}
	
	if (%amount <= $CityRPG::hunger::statecount)
	{
		CityRPGData.getData(%client.bl_id).valueHunger = %amount;
	}
	else
	{
		CityRPGData.getData(%client.bl_id).valueHunger = $CityRPG::hunger::statecount;
	}
	%client.updateHUD();
}

function gameConnection::addHunger(%client, %amount)
{
	if (CityRPGData.getData(%client.bl_id).valueHunger >= $CityRPG::hunger::statecount)
	{
		return false;
	}
	
	CityRPGData.getData(%client.bl_id).valueHunger += %amount;
	if (CityRPGData.getData(%client.bl_id).valueHunger >=$CityRPG::hunger::statecount)
	{
		CityRPGData.getData(%client.bl_id).valueHunger = $CityRPG::hunger::statecount;
	}
	%client.updateHUD();
	return true;
}

function gameConnection::subtractHunger(%client, %amount)
{
	if (CityRPGData.getData(%client.bl_id).valueHunger <= 0)
	{
		return false;
	}
	
	CityRPGData.getData(%client.bl_id).valueHunger -= %amount;
	if (CityRPGData.getData(%client.bl_id).valueHunger <= 0)
	{
		CityRPGData.getData(%client.bl_id).valueHunger = 0;
	}
	%client.updateHUD();
	return true;
}