function gameConnection::sellFood(%client, %sellerID, %servingID, %foodName, %price, %profit)
{			
	if(!$jobs[CityRPGData.getData(%sellerID).valueJobID].canSellFood)
	{
		messageClient(%client, '', "\c6This vendor is not licensed to sell food.");
		return;
	}
	
	if(%client.getHunger() <= 0)
	{
		messageClient(%client, '', "\c6You are too full to even think about buying any more food.");
		return;
	}
	
	if(!transferMoney(%client.bl_id, %sellerID, %price, "Food Transaction"))
	{
		messageClient(%client, '', "\c6You don't have enough money to buy this food.");
		return;
	}
		
	%portionName = strreplace($CityRPG::foodPortion[%servingID], "_", " ");

	messageClient(%client, '', '\c6You consume %1 \c3%2\c6 serving of \c3%3\c6.', CityRPG_DetectVowel(%portionName), %portionName, %foodName);	
	%client.subtractHunger(%servingID);	
	
	%client.player.serviceOrigin.onTransferSuccess(%client);
	CityRPG_ResetPlayerService(%client);
}

function fxDTSBrick::sellFood(%brick, %portion, %food, %markup, %client)
{
	if(isObject(%client.player) && !%client.player.serviceOrigin  && isObject(%brick))
	{
		%client.player.serviceType = "food";
		%client.player.serviceItem = %food;
		%client.player.serviceSize = %portion;
		%client.player.serviceFee = (5 * %portion - mFloor(%portion * 0.75)) +  %markup;
		%client.player.serviceMarkup = %markup;
		%client.player.serviceOrigin = %brick;
		
		messageClient(%client,'','\c6A service is offering to feed you %1 \c3%2\c6 portion of \c3%3\c6 for \c3$%4\c6.', CityRPG_DetectVowel($CityRPG::foodPortion[%portion]), strreplace($CityRPG::foodPortion[%portion], "_", " "), %food, %client.player.serviceFee);
		messageClient(%client,'',"\c6Accept with \c3/yes\c6, decline with \c3/no\c6.");
	}
	else if(%client.player.serviceOrigin && %client.player.serviceOrigin != %brick)
		messageClient(%client, '', "\c6You already have a charge request from another service! Type \c3/no\c6 to reject it.");
}

function CityRPG_AssembleFoodEvents()
{
	for(%a = 1; $CityRPG::foodPortion[%a] !$= ""; %a++)
	{
		%sellFood_Portions = %sellFood_Portions SPC $CityRPG::foodPortion[%a] SPC %a;
	}
	registerOutputEvent("fxDTSBrick", "sellFood", "list" @ %sellFood_Portions TAB "string 45 100" TAB "int 1 50 1");
}
CityRPG_AssembleFoodEvents();
	
		