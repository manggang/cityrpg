if(isObject(ClothesSO))
{
	exec("./clothes.cs");
}

exec("./food.cs");
exec("./items.cs");
exec("./zones.cs");
exec("./weapons.cs");

function fxDTSBrick::onTransferSuccess(%brick, %client)
{
	$inputTarget_self	= %brick;
	$inputTarget_player	= %client.player;
	$inputTarget_client	= %client;
	
	%brick.processInputEvent("onTransferSuccess", %client);
}

function fxDTSBrick::onTransferDecline(%brick, %client)
{
	$inputTarget_self	= %brick;
	$inputTarget_client	= %client;
	
	// Repeated Service Offer Hack
	for(%i = 0; %i < %brick.numEvents; %i++)
	{
		if(%brick.eventInput[%i] $= "onTransferDecline" && (%brick.eventOutput[%i] $= "requestFunds" || %brick.eventOutput[%i] $= "sellItem" || %brick.eventOutput[%i] $= "sellFood"))
			%brick.eventEnabled[%i] = false;
	}
	
	%brick.processInputEvent("onTransferDecline", %client);
}

function fxDTSBrick::requestFunds(%brick, %serviceName, %fund, %client)
{	
	if(isObject(%client.player) && !%client.player.serviceOrigin  && isObject(%brick))
	{		
		%client.player.serviceOrigin = %brick;
		%client.player.serviceFee = %fund;
		%client.player.serviceType = "service";
		
		messageClient(%client,'',"\c6Service \"\c3" @ %serviceName @ "\c6\" requests \c3$" @ %fund SPC "\c6.");
		messageClient(%client,'',"\c6Accept with \c3/yes\c6, decline with \c3/no\c6.");
	}
	else if(%client.player.serviceOrigin && %client.player.serviceOrigin != %brick)
	{
		messageClient(%client, '', "\c6You already have a charge request from another service! Type \c3/no\c6 to reject it.");
	}
}

function serverCmdYes(%client)
{
	if(!isObject(%client.player) || !isObject(%client.player.serviceOrigin))
	{
		messageClient(%client, '', "\c6You have no active transfers that you may accept!");
		return;
	}
	
	if(mFloor(VectorDist(%client.player.serviceOrigin.getPosition(), %client.player.getPosition())) > 16)
	{
		messageClient(%client, '', "\c6You are too far away from the service to purchase it!");
		CityRPG_ResetPlayerService(%client);
		return;
	}
	
	if(%client.getMoney() <= %client.player.serviceFee)
	{
		messageClient(%client, '', "\c6You cannot afford this service.");
		CityRPG_ResetPlayerService(%client);
		return;
	}
	
	%ownerBL_ID = %client.player.serviceOrigin.getGroup().bl_id;
	switch$(%client.player.serviceType)
	{
		case "service":
			transferMoney(%client.bl_id, %ownerBL_ID, %client.player.serviceFee, "Service Transaction");

			messageClient(%client, '', "\c6You have accepted the service fee of \c3$" @ %client.player.serviceFee @ "\c6!");
					
			if(%client.player.serviceOrigin.getGroup().client)
				%client.player.serviceOrigin.onTransferSuccess(%client);
						
		case "food":
			%client.sellFood(%ownerBL_ID, %client.player.serviceSize, %client.player.serviceItem, %client.player.serviceFee, %client.player.serviceMarkup);
			
		case "gun":
			%client.sellGun(%ownerBL_ID, %client.player.serviceItem, %client.player.serviceFee, %client.player.serviceMarkup);
			
		case "item":
			%client.sellItem(%ownerBL_ID, %client.player.serviceItem, %client.player.serviceFee, %client.player.serviceMarkup);
						
		case "zone":
			%client.sellZone(%ownerBL_ID, %client.player.serviceOrigin, %client.player.serviceFee);
						
		case "clothes":
			%client.sellClothes(%ownerBL_ID, %client.player.serviceOrigin, %client.player.serviceItem, %client.player.serviceFee);
	}
	CityRPG_ResetPlayerService(%client);
}

function serverCmdNo(%client)
{
	if(isObject(%client.player.serviceOrigin))
	{
		messageClient(%client, '', "\c6You have rejected the service fee!");
		
		%client.player.serviceOrigin.onTransferDecline(%client);		
		CityRPG_ResetPlayerService(%client);
	}
	else
		messageClient(%client, '', "\c6You have no active transfers that you may decline!");
}

function CityRPG_ResetPlayerService(%client)
{
	%client.player.serviceType = "";
	%client.player.serviceFee = "";
	%client.player.serviceMarkup = "";
	%client.player.serviceItem = "";
	%client.player.serviceSize = "";
	%client.player.serviceOrigin = "";
}



