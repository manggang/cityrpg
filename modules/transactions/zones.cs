function gameConnection::sellZone(%client, %sellerID, %brick, %price)
{
	if(%client.brickGroup.lotsOwned < $CityRPG::pref::realestate::maxLots)
	{
		messageClient(%client, '', "\c6You already own enough lots.");
		return;
	}
	
	if(isObject(%brick) && %brick.getClassName() $= "fxDTSBrick" && transferMoney(%client.bl_id, %sellerID, %price, "Zone Transaction"))
	{
		%brick.setDatablock(%brick.getDatablock().CityRPGMatchingLot);
		%client.brickGroup.add(%brick);
		%brick.handleCityRPGBrickCreation();
		
		messageClient(%client, '', '\c6You have purchased a \c3%1\c6 for $%2', %brick.getDatablock().uiName, %price);
		
		%client.player.serviceOrigin.onTransferSuccess(%client);
		CityRPG_ResetPlayerService(%client);
	}
}
