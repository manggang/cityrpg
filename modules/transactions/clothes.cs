function gameConnection::sellClothes(%client, %sellerID, %brick, %item, %price)
{
	if(!$jobs[CityRPGData.getData(%client.player.serviceOrigin.getGroup().bl_id).valueJobID].canSellClothes)
	{
		messageClient(%client, '', "\c6This vendor is not licensed to sell clothes.");
		return;
	}
	
	if(!isObject(%client.player))
	{
		return;
	}

	if(!transferMoney(%client.bl_id, %sellerID, %price, "Clothes Transaction"))
	{
		messageClient(%client, '', "\c6You don't have enough money to buy these clothes.");
		return;
	}
	
	messageClient(%client, '', "\c6Enjoy the new look!");	
	ClothesSO.giveItem(%client, %item);

	%client.applyForcedBodyColors();
	%client.applyForcedBodyParts();
	%client.player.serviceOrigin.onTransferSuccess(%client);
	CityRPG_ResetPlayerService(%client);
}

function fxDTSBrick::sellClothes(%brick, %item, %markup, %client)
{
	if(isObject(%client.player) && !%client.player.serviceOrigin  && isObject(%brick))
	{
		%client.player.serviceType = "clothes";
		%client.player.serviceItem = %item;
		%client.player.serviceFee = %markup;
		%client.player.serviceMarkup = %markup;
		%client.player.serviceOrigin = %brick;
		
		messageClient(%client,'', '\c6A clothing service is offering to dress you in %1 \c3%2 \c6for \c3$%3\c6.', CityRPG_DetectVowel(ClothesSO.sellName[%item]), ClothesSO.sellName[%item], %client.player.serviceFee);
		messageClient(%client,'', "\c6Accept with \c3/yes\c6, decline with \c3/no\c6.");
	}
	else if(%client.player.serviceOrigin && %client.player.serviceOrigin != %brick)
		messageClient(%client, '', "\c6You already have a charge request from another service! Type \c3/no\c6 to reject it.");
}
