$CityRPG::itemCount = 0;
$CityRPG::items = 0;

function addSellableItem(%addonName, %itemName, %price, %mineralRequirement)
{
	%error = ForceRequiredAddOn(%addonName);
	if(%error == $Error::AddOn_NotFound)
	{
	   error("ERROR: Gamemode_CityRPG - add-on " @ %addonName @ " not found");
	   return;
	}
	
	$CityRPG::items[$CityRPG::itemCount] = new ScriptObject()
	{
		itemName = %itemName;
		price = %price;
		mineralRequirement = %mineralRequirement;
	};
	$CityRPG::itemCount++;
}

addSellableItem("Item_Key", "keyItem", 5, 0);
addSellableItem("Weapon_Knife", "knifeItem", 5, 0);

function gameConnection::sellItem(%client, %sellerID, %itemID, %price, %profit)
{
	if(!$jobs[CityRPGData.getData(%client.player.serviceOrigin.getGroup().bl_id).valueJobID].canSellItems)
	{			
		messageClient(%client, '', "\c6This vendor is not licensed to sell items.");
		return;
	}
	
	if(!isObject(%client.player))
	{
		return;
	}

	%freespot = %client.player.getFreeInventorySlot();

	if(!%freeSpot)
	{
		messageClient(%client, '', "\c6You don't have enough space to carry this item!");
		return;
	}
	
	if(!transferMoney(%client.bl_id, %sellerID, %price, "Item Transaction"))
	{
		messageClient(%client, '', "\c6You don't have enough money to buy this item.");
		return;
	}
	
	if($CityRPG::miningEnabled)
	{
		CitySO.ore -= $CityRPG::items[%itemID].mineralRequirement;
	}

	%client.player.tool[%freeSpot] = $CityRPG::items[%itemID].itemName.getID();
	messageClient(%client, 'MsgItemPickup', "", %freeSpot, %client.player.tool[%freeSpot]);
	
	messageClient(%client, '', "\c6You have accepted the item's fee of \c3$" @ %price @ "\c6!");
	
	%client.player.serviceOrigin.onTransferSuccess(%client);
	CityRPG_ResetPlayerService(%client);
}

function fxDTSBrick::sellItem(%brick, %item, %markup, %client)
{
	if (%client.player.serviceOrigin)
	{
		messageClient(%client, '', "\c6You already have a charge request from a service! Type \c3/no\c6 to reject it.");
		return;
	}

	%name = $CityRPG::items[%item].itemName.uiName;
	
	if($CityRPG::miningEnabled && CitySO.ore < $CityRPG::items[%item].mineralRequirement)
	{
		messageClient(%client, '', '\c6A service is trying to offer you %1 \c3%2\c6, but the city needs \c3%3\c6 more ore to produce it!', CityRPG_DetectVowel(%name), %name, ($CityRPG::items[%item].mineralRequirement - CitySO.ore));
		return;
	}
	
	%client.player.serviceType = "item";
	%client.player.serviceItem = %item;
	%client.player.serviceFee = $CityRPG::items[%item].price + %markup;
	%client.player.serviceMarkup = %markup;
	%client.player.serviceOrigin = %brick;
	
	messageClient(%client,'',"\c6A service is offering to sell you one \c3" @ %name SPC "\c6for \c3$" @ %client.player.serviceFee SPC "\c6.");
	messageClient(%client,'',"\c6Accept with \c3/yes\c6, decline with \c3/no\c6.");		
}

function CityRPG_AssembleItemEvents()
{
	for(%i = 0; %i < $CityRPG::itemCount; %i++)
	{
		%sellItem_List = %sellItem_List SPC strreplace($CityRPG::items[%i].itemName.uiName, " ", "") SPC %i;
	}
	
	registerOutputEvent("fxDTSBrick", "sellItem", "list" @ %sellItem_List TAB "int 0 500 1");
}
CityRPG_AssembleItemEvents();