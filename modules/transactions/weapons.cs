$CityRPG::gunCount = 0;
$CityRPG::guns = 0;

function addSellableGun(%addonName, %itemName, %price, %mineralRequirement)
{
	%error = ForceRequiredAddOn(%addonName);
	if(%error == $Error::AddOn_NotFound)
	{
	   error("ERROR: Gamemode_CityRPG - add-on " @ %addonName @ " not found");
	   return;
	}
	
	$CityRPG::guns[$CityRPG::gunCount] = new ScriptObject()
	{
		itemName = %itemName;
		price = %price;
		mineralRequirement = %mineralRequirement;
	};
	$CityRPG::gunCount++;
}

addSellableGun("Weapon_Gun", "gunItem", 60, 30);
addSellableGun("Weapon_Guns_Akimbo", "akimboGunItem", 130, 65);
addSellableGun("Weapon_Shotgun", "shotgunItem", 200, 100);
addSellableGun("Weapon_Rocket_Launcher", "rocketLauncherItem", 350, 175);
addSellableGun("Weapon_Sniper_Rifle", "sniperRifleItem", 500, 250);

function gameConnection::sellGun(%client, %sellerID, %itemID, %price, %profit)
{
	if(!$jobs[CityRPGData.getData(%sellerID).valueJobID].canSellGuns)
	{			
		messageClient(%client, '', "\c6This vendor is not licensed to sell guns.");
		return;
	}
	
	if(!isObject(%client.player))
	{
		return;
	}

	%freespot = %client.player.getFreeInventorySlot();

	if(!%freeSpot)
	{
		messageClient(%client, '', "\c6You don't have enough space to carry this item!");
		return;
	}
	
	if(!transferMoney(%client.bl_id, %sellerID, %price, "Item Transaction"))
	{
		messageClient(%client, '', "\c6You don't have enough money to buy this item.");
		return;
	}
	
	if($CityRPG::miningEnabled)
	{
		CitySO.ore -= $CityRPG::guns[%itemID].mineralRequirement;
	}

	%client.player.tool[%freeSpot] = $CityRPG::guns[%itemID].itemName.getID();
	messageClient(%client, 'MsgItemPickup', "", %freeSpot, %client.player.tool[%freeSpot]);
	
	messageClient(%client, '', "\c6You have accepted the item's fee of \c3$" @ %price @ "\c6!");

	%client.player.serviceOrigin.onTransferSuccess(%client);
	CityRPG_ResetPlayerService(%client);
}

function fxDTSBrick::sellGun(%brick, %item, %markup, %client)
{
	if (%client.player.serviceOrigin)
	{
		messageClient(%client, '', "\c6You already have a charge request from a service! Type \c3/no\c6 to reject it.");
		return;
	}

	%name = $CityRPG::guns[%item].itemName.uiName;
	
	if($CityRPG::miningEnabled && CitySO.ore < $CityRPG::guns[%item].mineralRequirement)
	{
		messageClient(%client, '', '\c6A service is trying to offer you %1 \c3%2\c6, but the city needs \c3%3\c6 more ore to produce it!', CityRPG_DetectVowel(%name), %name, ($CityRPG::guns[%item].mineralRequirement - CitySO.ore));
		return;
	}
	
	%client.player.serviceType = "gun";
	%client.player.serviceItem = %item;
	%client.player.serviceFee = $CityRPG::guns[%item].price + %markup;
	%client.player.serviceMarkup = %markup;
	%client.player.serviceOrigin = %brick;
	
	messageClient(%client,'',"\c6A service is offering to sell you one \c3" @ %name SPC "\c6for \c3$" @ %client.player.serviceFee SPC "\c6.");
	messageClient(%client,'',"\c6Accept with \c3/yes\c6, decline with \c3/no\c6.");		
}


function CityRPG_AssembleGunEvents()
{
	for(%i = 0; %i < $CityRPG::gunCount; %i++)
	{
		%sellItem_List = %sellItem_List SPC strreplace($CityRPG::guns[%i].itemName.uiName, " ", "") SPC %i;
	}
	registerOutputEvent("fxDTSBrick", "sellGun", "list" @ %sellItem_List TAB "int 0 500 1");
}
CityRPG_AssembleGunEvents();