// ============================================================
// Project				:	peopleRP
// Author				:	Iban
// Description			:	Bounty Brick Code File
// ============================================================
// Table of Contents
// 1. Brick Data
// 2. Trigger Data
// ============================================================

// ============================================================
// Section 1 : Brick Data
// ============================================================
datablock fxDTSBrickData(CityRPGBountyBrickData : brick2x4FData)
{
	category = "CityRPG";
	subCategory = "CityRPG Infoblock";
	
	uiName = "Bounty Brick";
	
	CityRPGBrickType = 2;
	CityRPGBrickAdmin = true;
	
	triggerDatablock = CityRPGInputTriggerData;
	triggerSize = "2 4 1";
	trigger = 0;
};

// ============================================================
// Section 2 : Trigger Data
// ============================================================
function CityRPGBountyBrickData::parseData(%this, %brick, %client, %triggerStatus, %text)
{
	if(%triggerStatus !$= "")
	{
		if(%triggerStatus == true && %client.stage $= "")
		{
			messageClient(%client, '', "\c6Welcome to the hit office. Type out the number which corrosponds to your selection.");
			messageClient(%client, '', "\c0Note:\c6 Placing a bounty (as a non-official) is criminal activity.");
			messageClient(%client, '', "\c31 \c6- View bounties.");
			messageClient(%client, '', "\c32 \c6- Place a bounty.");
			
			%client.stage = 0;
		}
		
		if(%triggerStatus == false && %client.stage !$= "")
		{
			messageClient(%client, '', "\c6Thanks, come again.");
			
			%client.stage = "";
		}
		
		return;
	}
	
	%input = strLwr(%text);
	
	if(mFloor(%client.stage) == 0)
	{
		if(strReplace(%input, "1", "") !$= %input || strReplace(%input, "one", "") !$= %input)
		{
			%noCriminals = true;
			
			%clientCount = ClientGroup.getCount();
			for(%a = 0; %a < %clientCount; %a++)
			{
				%criminal = clientGroup.getObject(%a);
				
				if(CityRPGData.getData(%criminal.bl_id).valueBounty > 0)
				{
					messageClient(%client, '', "\c3" @ %criminal.name SPC "\c6- \c3$" @ CityRPGData.getData(%criminal.bl_id).valueBounty);
					
					%noCriminals = false;
				}
			}
			
			if(%noCriminals)
			{
				messageClient(%client, '', "\c6There are no wanted people online.");
			}
			
			%brick.trigger.getDatablock().onLeaveTrigger(%brick.trigger, (isObject(%client.player) ? %client.player : 0));
			
			return;
		}
		
		if(strReplace(%input, "2", "") !$= %input || strReplace(%input, "two", "") !$= %input)
		{
			%client.stage = 1.1;
			
			messageClient(%client, '', "\c6Who do you want to put a hit on? (ID or Name)");
			
			return;
		}
		
		messageClient(%client, '', "\c3" @ %text SPC "\c6is not a valid option!");
		
		%brick.trigger.getDatablock().onLeaveTrigger(%brick.trigger, (isObject(%client.player) ? %client.player : 0));
		
		return;
	}
	
	if(mFloor(%client.stage) == 1)
	{
		if(%client.stage == 1.1)
		{
			if(!findClientByName(%input) && !findClientByBL_ID(mFloor(%input)))
			{
				messageClient(%client, '', "\c6Please enter a valid name or ID of the person you want killed.");
			}
			else if(findClientByName(%input) || findClientByBL_ID(mFloor(%input)))
			{
				%hunted = (findClientByName(%input) ? findClientByName(%input) : findClientByBL_ID(mFloor(%input)));
				
				if(%hunted != %client)
				{
					messageClient(%client, '', "\c6Alright, so you want a hit on \c3" @ %hunted.name @ "\c6.");
					messageClient(%client, '', "\c6How much are you wanting to place?");
					
					%client.stage = 1.2;
					%client.stage["hunted"] = %hunted;
				}
				else
				{
					messageClient(%client, '', "\c6What? Do you have a death wish?\n\c6You cant place a bounty on yourself.");
					
					%brick.trigger.getDatablock().onLeaveTrigger(%brick.trigger, (isObject(%client.player) ? %client.player : 0));
				}
			}
			return;
		}
		
		if(%client.stage == 1.2)
		{
			if(mFloor(%input) < 1)
			{
				messageClient(%client, '', "\c6Please enter a valid amount of money to place on the victim.");
				
				return;
			}
			
			if(%client.getMoney() - mFloor(%input) < 0)
			{
				if(%client.getMoney() < 1)
				{
					messageClient(%client, '', "\c6You don't have that much money to place.");
					
					%brick.trigger.getDatablock().onLeaveTrigger(%brick.trigger, (isObject(%client.player) ? %client.player : 0));
					
					return;
				}
				
				%input = %client.getMoney();
			}
			
			if(mFloor(%input) <= $CityRPG::pref::demerits::minBounty)
			{
				messageClient(%client, '', "\c6Sorry Pal, we don't accept chump-change.\n\c6You need at least \c3$" @ $CityRPG::pref::demerits::minBounty @ "\c6 to place a bounty.");	
				return;
			}
			
			%bounty = mFloor(%input) ;
			
			messageAll('', "\c3" @ %client.name @ "\c6 has placed a \c3$" @ %bounty @ "\c6 bounty on \c3" @ %client.stage["hunted"].name @"\c6's head!");
			if(!%client.getJobSO().canOfferBounty)
			{
				commandToClient(%client, 'centerPrint', "\c6You have commited a crime. [\c3Placing an Illegal Hit\c6]", 1);
				%client.addDemerits($CityRPG::demerits::bountyPlacing);
			}
			
			%brick.trigger.getDatablock().onLeaveTrigger(%brick.trigger, (isObject(%client.player) ? %client.player : 0));
			
			CityRPGData.getData(%client.stage["hunted"].bl_id).valueBounty += %bounty;
			%client.subtractMoney(%input);
		}
		return;
	}
}