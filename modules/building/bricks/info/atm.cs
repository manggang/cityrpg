// ============================================================
// Project				:	CityRPG
// Author				:	Iban & Jookia
// Description				:	ATM Brick Code File
// ============================================================
// Table of Contents
// 1. Brick Data
// 2. Trigger Data
// ============================================================

// ============================================================
// Section 1 : Brick Data
// ============================================================
datablock fxDTSBrickData(CityRPGATMBrickData : brick2x4FData)
{
	category = "CityRPG";
	subCategory = "CityRPG Infoblock";
	
	uiName = "ATM Brick";
	
 	CityRPGBrickType = 2;
	CityRPGBrickAdmin = false;
	
	triggerDatablock = CityRPGInputTriggerData;
	triggerSize = "2 4 1";
	trigger = 0;
	
	hackable = 1;
};

// ============================================================
// Section 2 : Trigger Data
// ============================================================
function CityRPGATMBrickData::parseData(%this, %brick, %client, %triggerStatus, %text)
{
	if(%triggerStatus !$= "")
	{
		if(%triggerStatus == true && %client.stage $= "")
		{
			messageClient(%client, '', "\c6ATM Online.");
			if(%client.getMoneyInBank() > 0)
			{
				messageClient(%client, '', "\c6- You have \c3$" @ %client.getMoneyInBank() SPC "\c6in your account.");
			}
			
			messageClient(%client, '', "\c31 \c6- Withdraw Money.");
			messageClient(%client, '', "\c32 \c6- Hack ATM.");
			
			%client.stage = 0;
		}
		
		if(%triggerStatus == false && %client.stage !$= "")
		{
			messageClient(%client, '', "\c6ATM Ofline.");
			
			%client.stage = "";
		}
		
		return;
	}
	
	%input = strLwr(%text);
	
	if(mFloor(%client.stage) == 0)
	{
		if(strReplace(%input, "1", "") !$= %input || strReplace(%input, "one", "") !$= %input)
		{
			%client.stage = 1.1;
			
			messageClient(%client, '', "\c6Enter the amount of money you wish to withdraw.");
			
			return;
		}

		if(strReplace(%input, "2", "") !$= %input || strReplace(%input, "two", "") !$= %input)
		{
			%client.stage = 1.2;
			if(%client.getEducationLevel() >= $CityRPG::pref::hack::requiredEducation)
			{
				%stealchance = getRandom(1,2);
				%caughtchance = getRandom(1,4);
				%lockoutchance = getRandom(1,3);
				%beencaught = 0;
				
				%clientCount = ClientGroup.getCount();
				for(%a = 0; %a < %clientCount; %a++)
				{
					%subClient = ClientGroup.getObject(%a);
					if(isObject(%subClient.player) && isObject(%client.player) && %subClient != %client)
					{
						if(VectorDist(%subClient.player.getPosition(), %client.player.getPosition()) <= 30)
						{
							if(isObject($jobs[1]) != 1)
								%beencaught = 0;
							else if(isObject($jobs[1]) != 16)
								%beencaught = 0;
							else if(isObject($jobs[1]) != 17)
								%beencaught = 0;
							else if(isObject($jobs[1]) != 18)
								%beencaught = 0;
							else
								%beencaught = 1;
						}
					}
				}
				if(%caughtchance == 1)
				{
					messageAll('',"\c3" @ %client.name @ "\c6 has been caught attempting to hack an ATM!");
					CityRPGData.getData(%client.bl_id).valueDemerits += $CityRPG::pref::hack::demerits;
					return;
				}
				else if(%this.hackable)
				{
					if(%stealchance == 1)
					{
						if(%lockoutchance != 1)
						{
							%stolen = getRandom($CityRPG::pref::hack::stealmin,$CityRPG::pref::hack::stealmax);
							messageClient(%client,'',"\c6You managed to steal \c3$" @ %stolen @ "\c6 from the ATM.");
							%client.addMoney(%stolen);
							if(%caughtchance == 1)
							{
								messageAll('',"\c3" @ %client.name @ "\c6 has been caught hacking an ATM!");
								CityRPGData.getData(%client.bl_id).valueDemerits += $CityRPG::pref::hack::demerits;
							}
						}
						else
						{
							%this.hackable = 0;
							messageClient(%client,'',"\c6Your attempt failed, and you have been locked out of the machine.");
							%revivetime = $CityRPG::pref::hack::revivetime * 60000;
							schedule(%revivetime, 0, %this.hackable++);
						}
					}
					else
					{
						messageClient(%client,'',"\c6Your software malfunctioned. The machine is still hackable.");
					}
				}
				else
				{
					messageClient(%client,'',"\c6You are locked out of this machine.");
				}
			}
			else
			{
				messageClient(%client,'',"\c6Your education level must be \c3" @ $CityRPG::pref::hack::requiredEducation @ "\c6.");
			}
			return;
		}
		
		messageClient(%client, '', "\c3" @ %text SPC "\c6is not a valid option!");
		
		return;
	}
	
	if(mFloor(%client.stage) == 1)
	{
		if(%client.stage == 1.1)
		{
			if(mFloor(%input) < 1)
			{
				messageClient(%client, '', "\c6Error.");
				
				return;
			}
			
			if(%client.getMoneyInBank() - mFloor(%input) < 0)
			{
				if(CityRPGData.getData(%client.bl_id).valueATM < 1)
				{
					messageClient(%client, '', "\c6Insufficient funds.");
					
					%brick.trigger.getDatablock().onLeaveTrigger(%brick.trigger, (isObject(%client.player) ? %client.player : 0));
					
					return;
				}
				
				%input = CityRPGData.getData(%client.bl_id).valueATM;
			}
			
			messageClient(%client, '', "\c6You have withdrawn \c3$" @ mFloor(%input) @ "\c6.");
			
			%brick.trigger.getDatablock().onLeaveTrigger(%brick.trigger, (isObject(%client.player) ? %client.player : 0));
			
			%client.subtractMoneyFromBank(%input);
			%client.addMoney(%input);
		}
		return;
	}
}