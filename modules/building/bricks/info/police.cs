// ============================================================
// Project				:	peopleRP
// Author				:	Iban
// Description			:	Police Brick Code File
// ============================================================
// Table of Contents
// 1. Brick Data
// 2. Trigger Data
// ============================================================

// ============================================================
// Section 1 : Brick Data
// ============================================================
datablock fxDTSBrickData(CityRPGPoliceBrickData : brick2x4FData)
{
	category = "CityRPG";
	subCategory = "CityRPG Infoblock";
	
	uiName = "Police Brick";
	
	CityRPGBrickType = 2;
	CityRPGBrickAdmin = true;
	
	triggerDatablock = CityRPGInputTriggerData;
	triggerSize = "2 4 1";
	trigger = 0;
};

// ============================================================
// Section 2 : Trigger Data
// ============================================================
function CityRPGPoliceBrickData::parseData(%this, %brick, %client, %triggerStatus, %text)
{
	if(%triggerStatus !$= "")
	{
		if(%triggerStatus == true)
		{
			if(%client.getDemerits())
			{
				%yourDemerits = %client.getDemerits();
				%totalPrice = mFloor(%client.getDemerits() * $CityRPG::pref::demerits::demeritCost);
				%demsYouCanAfford = mFloor(%client.getMoney() / $CityRPG::pref::demerits::demeritCost);
				%demsYouCanBuy = (%demsYouCanAfford > %yourDemerits ? %yourDemerits : %demsYouCanAfford);
				%demCost = mFloor(%demsYouCanBuy * $CityRPG::pref::demerits::demeritCost);
			}
			
			messageClient(%client, '', "\c6Welcome! Please type the number corresponding to the options below.");
			
			if(%client.getDemerits() > 0)
			{
				messageClient(%client, '', "\c6Also, you have \c3" @ CityRPGData.getData(%client.bl_id).valueDemerits SPC "\c6demerits.");
			}
			
			messageClient(%client, '', "\c31 \c6- View Online Criminals");
			
			if(%client.getDemerits())
			{
				if(%demsYouCanBuy >= %yourDemerits)
				{
					messageClient(%client, '', "\c32 \c6- Pay off Demerits (\c3$" @ %demCost @ "\c6)");
				}
				else
				{
					messageClient(%client, '', "\c32 \c6- Pay Partial Demerits (\c3" @ %demsYouCanBuy @ "\c6 out of \c3" @ %yourDemerits @ "\c6 for \c3$" @ %demCost @ "\c6)");
				}
			}
		}
		
		if(%triggerStatus == false)
		{
			messageClient(%client, '', "\c6Thanks, come again.");
		}
		
		return;
	}
	
	%input = strLwr(%text);
	
	if(strReplace(%input, "1", "") !$= %input || strReplace(%input, "one", "") !$= %input)
	{
		%noCriminals = true;
		
		%clientCount = ClientGroup.getCount();
		for(%a = 0; %a < %clientCount; %a++)
		{
			%criminal = clientGroup.getObject(%a);
			
			if(%criminal.getDemerits() >= $CityRPG::pref::demerits::wantedLevel)
			{
				messageClient(%client, '', "\c3" @ %criminal.name SPC "\c6- \c3" @ %criminal.getDemerits());
				
				%noCriminals = false;
			}
		}
		
		if(%noCriminals)
		{
			messageClient(%client, '', "\c6There are no criminals online.");
		}
		
		%brick.trigger.getDatablock().onLeaveTrigger(%brick.trigger, (isObject(%client.player) ? %client.player : 0));
		
		return;
	}
	
	if((strReplace(%input, "2", "") !$= %input || strReplace(%input, "two", "") !$= %input) && %client.getDemerits() > 0)
	{
		%yourDemerits = %client.getDemerits();
		%totalPrice = mFloor(%client.getDemerits()* $CityRPG::pref::demerits::demeritCost);
		%demsYouCanAfford = mFloor(%client.getMoney() / $CityRPG::pref::demerits::demeritCost);
		%demsYouCanBuy = (%demsYouCanAfford > %yourDemerits ? %yourDemerits : %demsYouCanAfford);
		%demCost = mFloor(%demsYouCanBuy * $CityRPG::pref::demerits::demeritCost);
		
		if(%demsYouCanBuy <= 0)
		{
			messageClient(%client, '', "\c6You cant afford to pay off any demerits!");
			
			%brick.trigger.getDatablock().onLeaveTrigger(%brick.trigger, (isObject(%client.player) ? %client.player : 0));
			
			return;
		}
		
		if(%client.getMoney() - %demCost < 0)
		{
			messageClient(%client, '', "\c6You don't have enough money to do that.");
			
			return;
		}
		
		%client.subtractMoney(%demCost);
		%client.subtractDemerits(%demsYouCanBuy);
		
		messageClient(%client, '', "\c6You have paid \c3$" @ %demCost @ "\c6. You now have\c3" SPC (%client.getDemerits() ? %client.getDemerits() : "no") SPC "\c6demerits.");
		
		%brick.trigger.getDatablock().onLeaveTrigger(%brick.trigger, (isObject(%client.player) ? %client.player : 0));
		
		return;
	}
	
	messageClient(%client, '', "\c3" @ %text SPC "\c6is not a valid option!");

	return;
}