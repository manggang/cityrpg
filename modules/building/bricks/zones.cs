datablock fxDTSBrickData(CityRPGSmallZoneBrickData : brick16x16FData)
{
	iconName = "Add-Ons/GameMode_CityRPG/shapes/BrickIcons/16x16ZoneIcon";
	
	category = "CityRPG";
	subCategory = "CityRPG Zones";
	
	uiName = "Small Zone";

	CityRPGBrickAdmin = true;
	CityRPGMatchingLot = CityRPGSmallLotBrickData;
};

datablock fxDTSBrickData(CityRPGHalfSmallZoneBrickData : brick16x32FData)
{
	iconName = "Add-Ons/GameMode_CityRPG/shapes/BrickIcons/16x32ZoneIcon";
	
	category = "CityRPG";
	subCategory = "CityRPG Zones";
	
	uiName = "Half-Small Zone";
	
	CityRPGBrickAdmin = true;
	CityRPGMatchingLot = CityRPGHalfSmallLotBrickData;
};

datablock fxDTSBrickData(CityRPGMediumZoneBrickData : brick32x32FData)
{
	iconName = "Add-Ons/GameMode_CityRPG/shapes/BrickIcons/32x32ZoneIcon";
	
	category = "CityRPG";
	subCategory = "CityRPG Zones";
	
	uiName = "Medium Zone";
	
	CityRPGBrickAdmin = true;
	CityRPGMatchingLot = CityRPGMediumLotBrickData;
};

datablock fxDTSBrickData(CityRPGLargeZoneBrickData : brick64x64FData)
{
	iconName = "Add-Ons/GameMode_CityRPG/shapes/BrickIcons/64x64ZoneIcon";
	
	category = "CityRPG";
	subCategory = "CityRPG Zones";
	
	uiName = "Large Zone";
	
	CityRPGBrickAdmin = true;
	CityRPGMatchingLot = CityRPGLargeLotBrickData;
};
