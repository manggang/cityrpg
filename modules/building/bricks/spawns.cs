
datablock fxDtsBrickData(CityRPGPersonalSpawnBrickData : brickSpawnPointData)
{
	category = "CityRPG";
	subCategory = "CityRPG Spawns";
	
	uiName = "Personal Spawn";
	
	specialBrickType = "";
	
	CityRPGBrickType = 3;
	CityRPGBrickAdmin = false;
	
	spawnData = "personalSpawn";
};

datablock fxDtsBrickData(CityRPGJailSpawnBrickData : brickSpawnPointData)
{
	category = "CityRPG";
	subCategory = "CityRPG Spawns";
	
	uiName = "Jail Spawn";
	
	specialBrickType = "";
	
	CityRPGBrickType = 3;
	CityRPGBrickAdmin = true;
	
	spawnData = "jailSpawn";
};

datablock fxDTSBrickData(CityRPGPermaSpawnData : brick2x2FData)
{
	category = "CityRPG";
	subCategory = "CityRPG Infoblock";
	
	uiName = "Permaspawn Brick";

	CityRPGBrickAdmin = true;
	CityRPGPermaspawn = 1;
};

datablock fxDTSBrickData(CityRPGPoliceVehicleData : brickVehicleSpawnData)
{
	category = "CityRPG";
	subCategory = "CityRPG Spawns";
	uiName = "Police Vehicle Spawn";
	CityRPGBrickAdmin = true;
};

datablock fxDTSBrickData(CityRPGCrimeVehicleData : brickVehicleSpawnData)
{
	category = "CityRPG";
	subCategory = "CityRPG Spawns";
	uiName = "Crime Vehicle Spawn";
	CityRPGBrickAdmin = true;
};
