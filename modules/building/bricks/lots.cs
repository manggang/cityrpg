datablock triggerData(CityRPGLotTriggerData)
{
	tickPeriodMS = 500;
	parent = 0;
};

datablock fxDTSBrickData(CityRPGSmallLotBrickData : brick16x16FData)
{
	iconName = "Add-Ons/GameMode_CityRPG/shapes/BrickIcons/16x16LotIcon";
	
	category = "CityRPG";
	subCategory = "CityRPG Lots";
	
	uiName = "Small Lot";
	
	CityRPGBrickType = 1;
	CityRPGBrickAdmin = false;
	
	triggerDatablock = CityRPGLotTriggerData;
	triggerSize = "16 16 48";
	trigger = 0;
	
	initialPrice = 500;
	taxAmount = 10;
};

datablock fxDTSBrickData(CityRPGHalfSmallLotBrickData : brick16x32FData)
{
	iconName = "Add-Ons/GameMode_CityRPG/shapes/BrickIcons/16x32LotIcon";
	
	category = "CityRPG";
	subCategory = "CityRPG Lots";
	
	uiName = "Half-Small Lot";
	
	CityRPGBrickType = 1;
	CityRPGBrickAdmin = false;
	
	triggerDatablock = CityRPGLotTriggerData;
	triggerSize = "16 32 48";
	trigger = 0;
	
	initialPrice = 1000;
	taxAmount = 20;
};

datablock fxDTSBrickData(CityRPGMediumLotBrickData : brick32x32FData)
{
	iconName = "Add-Ons/GameMode_CityRPG/shapes/BrickIcons/32x32LotIcon";
	
	category = "CityRPG";
	subCategory = "CityRPG Lots";
	
	uiName = "Medium Lot";
	
	CityRPGBrickType = 1;
	CityRPGBrickAdmin = false;
	
	triggerDatablock = CityRPGLotTriggerData;
	triggerSize = "32 32 64";
	trigger = 0;
	
	initialPrice = 2000;
	taxAmount = 40;
};

datablock fxDTSBrickData(CityRPGLargeLotBrickData : brick64x64FData)
{
	iconName = "Add-Ons/GameMode_CityRPG/shapes/BrickIcons/64x64LotIcon";
	
	category = "CityRPG";
	subCategory = "CityRPG Lots";
	
	uiName = "Large Lot";
	
	CityRPGBrickType = 1;
	CityRPGBrickAdmin = false;
	
	triggerDatablock = CityRPGLotTriggerData;
	triggerSize = "64 64 128";
	trigger = 0;
	
	initialPrice = 8000;
	taxAmount = 80;
};

datablock fxDTSBrickData(CityRPGSmalltLotBrickData : brick16x16FData)
{
	iconName = "Add-Ons/GameMode_CityRPG/shapes/BrickIcons/16x16LotIcon";
	
	category = "CityRPG";
	subCategory = "Taxless Lots";
	
	uiName = "Small Taxless Lot";
	
	CityRPGBrickType = 1;
	CityRPGBrickAdmin = false;
	
	triggerDatablock = CityRPGLotTriggerData;
	triggerSize = "16 16 96";
	trigger = 0;
	
	initialPrice = 750;
	taxAmount = 0;
};

datablock fxDTSBrickData(CityRPGHalfSmalltLotBrickData : brick16x32FData)
{
	iconName = "Add-Ons/GameMode_CityRPG/shapes/BrickIcons/16x32LotIcon";
	
	category = "CityRPG";
	subCategory = "Taxless Lots";
	
	uiName = "Half-Small Taxless Lot";
	
	CityRPGBrickType = 1;
	CityRPGBrickAdmin = false;
	
	triggerDatablock = CityRPGLotTriggerData;
	triggerSize = "16 32 96";
	trigger = 0;
	
	initialPrice = 1500;
	taxAmount = 0;
};

datablock fxDTSBrickData(CityRPGMediumtLotBrickData : brick32x32FData)
{
	iconName = "Add-Ons/GameMode_CityRPG/shapes/BrickIcons/32x32LotIcon";
	
	category = "CityRPG";
	subCategory = "Taxless Lots";
	
	uiName = "Medium Taxless Lot";
	
	CityRPGBrickType = 1;
	CityRPGBrickAdmin = false;
	
	triggerDatablock = CityRPGLotTriggerData;
	triggerSize = "32 32 128";
	trigger = 0;
	
	initialPrice = 3000;
	taxAmount = 0;
};

datablock fxDTSBrickData(CityRPGLargetLotBrickData : brick64x64FData)
{
	iconName = "Add-Ons/GameMode_CityRPG/shapes/BrickIcons/64x64LotIcon";
	
	category = "CityRPG";
	subCategory = "Taxless Lots";
	
	uiName = "Large Taxless Lot";
	
	CityRPGBrickType = 1;
	CityRPGBrickAdmin = false;
	
	triggerDatablock = CityRPGLotTriggerData;
	triggerSize = "64 64 256";
	trigger = 0;
	
	initialPrice = 12000;
	taxAmount = 0;
};
