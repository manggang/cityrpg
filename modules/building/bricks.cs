exec("./bricks/info/atm.cs");
exec("./bricks/info/bank.cs");
exec("./bricks/info/education.cs");
exec("./bricks/info/police.cs");
exec("./bricks/info/bounty.cs");
exec("./bricks/info/labor.cs");
exec("./bricks/info/realestate.cs");
exec("./bricks/info/drugsell.cs");
exec("./bricks/info/criminalbank.cs");

exec("./bricks/lots.cs");
exec("./bricks/zones.cs");

exec("./bricks/spawns.cs");

datablock triggerData(CityRPGInputTriggerData)
{
	tickPeriodMS = 500;
	parent = 0;
};

if (isObject(DoorSO))
{
	DoorSO.addDoorTypeFromFile("Add-Ons/GameMode_CityRPG/bricks/doors/unbreakable.cs");
	doorBrickUnbreakable.unbreakable = true;
	doorBrickUnbreakable.CityRPGBrickAdmin = true;
}
