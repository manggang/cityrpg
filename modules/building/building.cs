exec("./bricks.cs");

$CityRPG::pref::realestate::maxLots = 4;
$CityRPG::pref::brickCost = 2;

package CityRPG_BuildingPackage
{
	function fxDTSBrick::onPlant(%brick)
	{		
		Parent::onPlant(%brick);
		CityRPG_onBrickPlant(%brick);
	}

	function fxDTSBrick::onLoadPlant(%brick)
	{		
		Parent::onLoadPlant(%brick);
		if(%brick == $LastLoadedBrick)
		{
			switch(%brick.getDatablock().CityRPGBrickType)
			{
			case 1:
				%brick.schedule(1, "handleCityRPGBrickCreation");
			case 2:
				%brick.schedule(1, "handleCityRPGBrickCreation");
			case 3:
				$CityRPG::temp::spawnPoints = ($CityRPG::temp::spawnPoints $= "") ? %brick : $CityRPG::temp::spawnPoints SPC %brick;
			case 4:
				%brick.resources = %brick.getDatablock().resources;
			case 6:
				%brick.schedule(1, "handleCityRPGBrickCreation");
			case 10:
				%brick.schedule(1, "handleCityRPGBrickCreation");
			}
			return;
		}
	}
	
	function CityRPG_onBrickPlant(%brick)
	{
		if(isObject(%brick))
		{
			if(isObject(%client = %brick.getGroup().client))
			{
				if(%brick.getDatablock().CityRPGBrickAdmin &&!%client.isAdmin)
				{
					CityRPG_BrickPlantFailed(%brick, "You must be an admin to plant this brick.");
					%client.player.kill();
					return;
				}
				
				if(mFloor(getWord(%brick.rotation, 3)) == 90)
				{
					%boxSize = getWord(%brick.getDatablock().brickSizeY, 1) / 2.5 SPC getWord(%brick.getDatablock().brickSizeX, 0) / 2.5 SPC getWord(%brick.getDatablock().brickSizeZ, 2) / 2.5;
				}
				else
				{
					%boxSize = getWord(%brick.getDatablock().brickSizeX, 1) / 2.5 SPC getWord(%brick.getDatablock().brickSizeY, 0) / 2.5 SPC getWord(%brick.getDatablock().brickSizeZ, 2) / 2.5;
				}
				
				initContainerBoxSearch(%brick.getWorldBoxCenter(), %boxSize, $typeMasks::triggerObjectType);
				
				while(isObject(%trigger = containerSearchNext()))
				{
					if(%trigger.getDatablock() == CityRPGLotTriggerData.getID())
					{
						%lotTrigger = %trigger;
					}
				}
				
				if(%lotTrigger && %brick.getDatablock().CityRPGBrickType != 1)
				{				
					%lotTriggerMinX = getWord(%lotTrigger.getWorldBox(), 0);
					%lotTriggerMinY = getWord(%lotTrigger.getWorldBox(), 1);
					%lotTriggerMinZ = getWord(%lotTrigger.getWorldBox(), 2);
					
					%lotTriggerMaxX = getWord(%lotTrigger.getWorldBox(), 3);
					%lotTriggerMaxY = getWord(%lotTrigger.getWorldBox(), 4);
					%lotTriggerMaxZ = getWord(%lotTrigger.getWorldBox(), 5);
					
					%brickMinX = getWord(%brick.getWorldBox(), 0) + 0.0016;
					%brickMinY = getWord(%brick.getWorldBox(), 1) + 0.0013;
					%brickMinZ = getWord(%brick.getWorldBox(), 2) + 0.00126;
					
					%brickMaxX = getWord(%brick.getWorldBox(), 3) - 0.0016;
					%brickMaxY = getWord(%brick.getWorldBox(), 4) - 0.0013;
					%brickMaxZ = getWord(%brick.getWorldBox(), 5) - 0.00126;

					
					if(%brickMinX <= %lotTriggerMinX || %brickMinY <= %lotTriggerMinY || %brickMinZ <= %lotTriggerMinZ)
					{
						CityRPG_BrickPlantFailed(%brick, "???");
						return;
					}
					
					if(%brickMaxX >= %lotTriggerMaxX || %brickMaxY >= %lotTriggerMaxY || %brickMaxZ >= %lotTriggerMaxZ)
					{
						CityRPG_BrickPlantFailed(%brick, "???");
						return;
					}
				}

				switch(%brick.getDatablock().CityRPGBrickType)
				{
				case 1:						
					if(%lotTrigger)
					{
						CityRPG_BrickPlantFailed(%brick, "Only Chuck Norris can put a lot on top of another lot.");
						return;
					}

					if(%client.brickGroup.lotsOwned >= $CityRPG::pref::realestate::maxLots)
					{
						CityRPG_BrickPlantFailed(%brick, "You already own the maximum number of lots.");
						return;
					}
					
					if(!%client.subtractMoney(mFloor(%brick.getDatablock().initialPrice)))
					{
						CityRPG_BrickPlantFailed(%brick, "\c6You need at least \c3$" @ mFloor(%brick.getDatablock().initialPrice) SPC "\c6in order to plant this lot!");
						return;
					}
					
					CitySO.addTaxMoney(%brick.getDatablock().initialPrice);
					
					messageClient(%client, '', "\c6You have paid \c3$" @ mFloor(%brick.getDatablock().initialPrice) @ "\c6 to plant this lot.");
					%brick.schedule(0, "handleCityRPGBrickCreation");
				case 2:
					%brick.schedule(0, "handleCityRPGBrickCreation");
				case 3:
					$CityRPG::temp::spawnPoints = ($CityRPG::temp::spawnPoints $= "") ? %brick : $CityRPG::temp::spawnPoints SPC %brick;
				case 4:
					if(%brick.getDatablock().resources)
						%brick.resources = %brick.getDatablock().resources;
				case 420:									
					if(!%lotTrigger && !%client.isAdmin)
					{
						CityRPG_BrickPlantFailed(%brick, "You cannot plant a brick outside of a lot.\n\c6Use a lot brick to start your build!");
						return;
					}

					if(CityRPGData.getData(%client.bl_id).valuedrugamount >= $CityRPG::drug::maxdrugplants)
					{
						CityRPG_BrickPlantFailed(%brick, "\c6You have met the limit of drugs you can plant.");
						return;
					}
					
					if(!%client.subtractMoney(mFloor(%brick.getDatablock().price)))
					{
						CityRPG_BrickPlantFailed(%brick, "\c6You need at least \c3$" @ mFloor(%brick.getDatablock().price) SPC "\c6in order to plant this.");
						return;
					}																	
					onDrugPlaced(%client);
				default:			
					if(!%lotTrigger && !%client.isAdmin)
					{
						CityRPG_BrickPlantFailed(%brick, "You cannot plant a brick outside of a lot.\n\c6Use a lot brick to start your build!");
						return;
					}
					
					if (CitySO.lumber > 0)
					{
						CitySO.lumber--;
					}
					else if (CityRPGData.getData(%client.bl_id).lumber > 0)
					{
						CityRPGData.getData(%client.bl_id).lumber--;
					}
					else
					{
						if (!%client.subtractMoney($CityRPG::pref::brickCost))
						{
							CityRPG_BrickPlantFailed(%brick, "\c6You need at least \c3$" @ $CityRPG::pref::brickCost SPC "\c6in order to build a brick!");
							return;
						}
						CitySO.addTaxMoney($CityRPG::pref::brickCost);
					}
					
					if(%brick.getDatablock().getID() == brickVehicleSpawnData.getID())
					{
						if(!%client.subtractMoney(mFloor($CityRPG::prices::vehicleSpawn)))
						{
							CityRPG_BrickPlantFailed(%brick, "\c6You need at least \c3$" @ mFloor($CityRPG::prices::vehicleSpawn) SPC "\c6in order to plant this vehicle spawn!");
							return;
						}
						
						CitySO.addTaxMoney($CityRPG::prices::vehicleSpawn);
						commandToClient(%client, 'centerPrint', "\c6You have paid \c3$" @ mFloor($CityRPG::prices::vehicleSpawn) @ "\c6 to plant this vehicle spawn.", 3);
					}	
				}
			}
		}
	}
	
	function fxDTSBrick::onRemove(%brick)
	{
		switch(%brick.getDatablock().CityRPGBrickType)
		{
		case 1:
			%brick.handleCityRPGBrickDelete();
		case 2:
			%brick.handleCityRPGBrickDelete();
		case 3:
			if(getWord($CityRPG::temp::spawnPoints, 0) == %brick)
				$CityRPG::temp::spawnPoints = strReplace($CityRPG::temp::spawnPoints, %brick @ " ", "");
			else
				$CityRPG::temp::spawnPoints = strReplace($CityRPG::temp::spawnPoints, " " @ %brick, "");
		}
		
		parent::onRemove(%brick);
	}
};
activatePackage(CityRPG_BuildingPackage);

function CityRPG_BrickPlantFailed(%brick, %message)
{
	%brick.schedule(0, "delete");
	commandToClient(%brick.getGroup().client, 'centerPrint', %message, 3);
}					

function fxDTSBrick::handleCityRPGBrickCreation(%brick, %data)
{
	if(!isObject(%brick.trigger))
	{
		%datablock = %brick.getDatablock();
		
		%trigX = getWord(%datablock.triggerSize, 0);
		%trigY = getWord(%datablock.triggerSize, 1);
		%trigZ = getWord(%datablock.triggerSize, 2);
		
		if(mFloor(getWord(%brick.rotation, 3)) == 90)
			%scale = (%trigY / 2) SPC (%trigX / 2) SPC (%trigZ / 2);
		else
			%scale = (%trigX / 2) SPC (%trigY / 2) SPC (%trigZ / 2);
			
		
		%brick.trigger = new trigger()
		{
			datablock = %datablock.triggerDatablock;
			position = getWords(%brick.getWorldBoxCenter(), 0, 1) SPC getWord(%brick.getWorldBoxCenter(), 2) + ((getWord(%datablock.triggerSize, 2) / 4) + (%datablock.brickSizeZ * 0.1));
			rotation = "1 0 0 0";
			scale = %scale;
			polyhedron = "-0.5 -0.5 -0.5 1 0 0 0 1 0 0 0 1";
			parent = %brick;
		};
		
		%boxSize = getWord(%scale, 0) / 2.5 SPC getWord(%scale, 1) / 2.5 SPC getWord(%scale, 2) / 2.5;
		
		initContainerBoxSearch(%brick.trigger.getWorldBoxCenter(), %boxSize, $typeMasks::playerObjectType);
		
		while(isObject(%player = containerSearchNext()))
			%brick.trigger.getDatablock().onEnterTrigger(%brick.trigger, %player);
		
		if(%brick.getDatablock().CityRPGBrickType == 1)
		{
			getBrickGroupFromObject(%brick).taxes += %brick.getDatablock().taxAmount;
			getBrickGroupFromObject(%brick).lotsOwned++;
			
			if(isObject(getBrickGroupFromObject(%brick).client))
				getBrickGroupFromObject(%brick).client.updateHUD();
		}
	}
}

function fxDTSBrick::handleCityRPGBrickDelete(%brick, %data)
{
	if(isObject(%brick.trigger))
	{
		%clientCount = ClientGroup.getCount();
		for(%a = 0; %a < %clientCount; %a++)
		{
			%subClient = ClientGroup.getObject(%a);
			if(isObject(%subClient.player) && %subClient.CityRPGTrigger == %brick.trigger)
				%brick.trigger.getDatablock().onLeaveTrigger(%brick.trigger, clientGroup.getObject(%a).player, true);
		}
		
		%boxSize = getWord(%brick.trigger.scale, 0) / 2.5 SPC getWord(%brick.trigger.scale, 1) / 2.5 SPC getWord(%brick.trigger.scale, 2) / 2.5;
		
		initContainerBoxSearch(%brick.trigger.getWorldBoxCenter(), %boxSize, $typeMasks::playerObjectType);
		
		while(isObject(%player = containerSearchNext()))
			%brick.trigger.getDatablock().onLeaveTrigger(%brick.trigger, %player);
		
		%brick.trigger.delete();
		
		if(%brick.getDatablock().CityRPGBrickType == 1)
		{
			getBrickGroupFromObject(%brick).taxes -= %brick.getDatablock().taxAmount;
			getBrickGroupFromObject(%brick).lotsOwned--;
			
			if(isObject(getBrickGroupFromObject(%brick).client))
				getBrickGroupFromObject(%brick).clien.updateHUD();
			
			if(%brick.getName() !$= "")
			{
				%data = CityRPGData.getData(getBrickGroupFromObject(%brick).bl_id).valueLotData;
				
				%newData = getWord(%data, 0) - 1;
				
				for(%a = 1; %a < getWordCount(%data); %a++)
				{
					if(%cancelNext)
					{
						%cancelNext = false;
						continue;
					}
					
					%newData = %newData SPC getWord(%data, %a);
					
					if(%a % 2 == 0)
						continue;
					
					%newBrick = "_" @ getWord(%data, %a);
					if(isObject(%newBrick) && %newBrick $= %brick.getName())
					{
						%found = true;
						
						%newData = getWords(%newData, 0, getWordCount(%newData) - 2);
						
						%cancelNext = true;
					}
				}
				
				if(%found)
				{
					CityRPGData.getData(getBrickGroupFromObject(%brick).bl_id).valueLotData = %newData;
				}
			}
		}
	}	
}

function fxDTSBrick::transferLot(%brick, %targetBG)
{
	%ownerBG = getBrickGroupFromObject(%brick);
	
	if(%brick.tran || !isObject(%brick))
		return;
	
	if(isObject(%ownerBG))
		%ownerBG.remove(%brick);
	
	%targetBG.add(%brick);
	%brick.tran = 1;
	%brick.transferLot(%targetBG);
	
	for(%i = 0; %i < %brick.getNumUpBricks(); %i++)
	{
		%target = %brick.getUpBrick(%i);
		if(isObject(%target))
			%target.transferLot(%targetBG);
	}
}
