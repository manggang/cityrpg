datablock itemData(CityRPGWateringCanItem : hammerItem)
{
	category = "Weapon";
	uiName = "Watering Can";
	
	shapeFile		= "Add-Ons/GameMode_CityRPG/shapes/wateringcan.dts";
	image = CityRPGWateringCanImage;
	colorShiftColor = "0 0.7 0.4 1.000000";
};

datablock shapeBaseImageData(CityRPGWateringCanImage : hammerImage)
{
	item = CityRPGWateringCanItem;
	projectile = hammerProjectile;
	shapeFile		= "Add-Ons/GameMode_CityRPG/shapes/wateringcan.dts";
	colorShiftColor = "0 0.7 0.4 1.000000";
	showBricks = 0;
};

function CityRPGWateringCanImage::onPreFire(%this, %obj, %slot)
{
	%obj.playThread(2, "armAttack");
}

function CityRPGWateringCanImage::onStopFire(%this, %obj, %slot)
{
	%obj.playThread(2, "root");
}

function CityRPGWateringCanImage::onHitObject(%this, %obj, %slot, %col, %pos, %normal)
{
}