datablock fxDTSBrickData(CityRPGCornData)
{
	brickFile = "Add-Ons/GameMode_CityRPG/shapes/Bricks/plantstageone.blb";
	iconName = "Add-Ons/GameMode_CityRPG/shapes/BrickIcons/cannabis";
	
	uiName = "Corn";
	
	category = "CityRPG";
	subCategory = "Farming";
};

datablock fxDTSBrickData(CityRPGCornData2)
{
	brickFile = "Add-Ons/GameMode_CityRPG/shapes/Bricks/plantstagetwo.blb";
};

datablock fxDTSBrickData(CityRPGCornData3)
{
	brickFile = "Add-Ons/GameMode_CityRPG/shapes/Bricks/plantstagethree.blb";
};

datablock fxDTSBrickData(CityRPGCornData4)
{
	brickFile = "Add-Ons/GameMode_CityRPG/shapes/Bricks/plantstagefour.blb";
};

function CityRPGCornData::onPlant(%brick)
{
	%brick.setColliding(false);
}