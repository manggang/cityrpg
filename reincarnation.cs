function serverCmdReincarnate(%client, %do)
{
	%cost = 100000 * (CityRPGData.getData(%client.bl_id).timesReincarnated + 1);
	
	if(%do $= "accept")
	{
		if(%client.getMoney() + %client.getMoneyInBank() >= %cost && %client.getEducationLevel() >= $CityRPG::pref::maxEducationLevel)
		{
			%oldEducationValue = %client.getEducationLevel();
			
			CityRPGData.removeData(%client.bl_id);
			CityRPGData.addData(%client.bl_id);
			
			CityRPGData.getData(%client.bl_id).timesReincarnated += 1;
			%client.setEducationLevel(%oldEducationValue + 1);
			
			if(isObject(%client.player))
			{
				%client.spawnPlayer();
			}
			
			messageAllExcept(%client, '', '\c3%1\c6 has been reincarnated!', %client.name);
			messageClient(%client, '', "\c6You have been reincarnated.");
		}
	}
	else
	{
		messageClient(%client, '', "\c6Reincarnation is a way to get more fun out of the game by continuing to make progress after you have everything there is to get.");
		messageClient(%client, '', "\c6You need at least $" @ %cost @ " and the maximum natural education (" @ $CityRPG::pref::maxEducationLevel @ ") to reincarnate yourself. Your account will almost completely reset.");
		messageClient(%client, '', "\c6The perks of doing this are...");
		messageClient(%client, '', "\c6 - You will start with an education 1 level higher.");
		messageClient(%client, '', "\c6 - Your name will be yellow by default and orange if you are wanted.");
		messageClient(%client, '', "\c6Type \c3/reincarnate accept\c6 to start anew!");
	}
}
