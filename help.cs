function printHelpHint(%client)
{
	messageClient(%client, '', "\c6You can type /help to access information about the game.\c6");
}

package CityRPG_HelpPackage
{
	function serverCmdmessageSent(%client, %text)
	{
		if(getSubStr(%text, 0, 1) $= "^")
			messageClient(%client, '', "\c5This system does not use Jookia's carets. Please use a standard / infront of all commands.");
		
		if(strstr(%text, "help") >= 0)
			printHelpHint(%client);
		
		if(strstr(%text, "how do") >= 0)
			printHelpHint(%client);
		
		if(strstr(%text, "job'") >= 0)
			printHelpHint(%client);
		
		if(strstr(%text, "education'") >= 0)
			printHelpHint(%client);
		
		parent::serverCmdmessageSent(%client, %text);
	}
};
activatePackage(CityRPG_HelpPackage);

//--------------------
// Purpose:	Automated documentation to assist new players.
//--------------------

function Hellen::onAdd(%this)
{
	if(%this.getName() $= "")
	{
		echo("Hellen::onAdd(): This database has no name! Deleting database..");
		
		%this.schedule(0, "delete");
		
		return false;
	}
	
	%this.loadData();
	
	return true;
}

function Hellen::onRemove(%this)
{
	for(%a = 1; %a <= %this.sectionCount; %a++)
	{
		%this.section[%a].delete();
	}
	
	return true;
}

function Hellen::loadData(%this)
{
	if(!isFile("./help.hlp"))
	{
		echo(%this.getName() @ "::onAdd(): Help file not found!");
		
		return false;
	}
	
	%file = new fileObject();
	%file.openForRead(findFirstFile("./help.hlp"));
	
	%this.section[(%this.sectionCount = 1)] = new scriptObject();
	%this.sectionName[%this.sectionCount] = "main";
	
	%section = %this.section[%this.sectionCount];
	
	%section.lineCount = 0;
	
	while(!%file.isEOF())
	{
		%line = %file.readLine();
		
		if(getSubStr(%line, 0, 2) $= "//")
		{
			continue;
		}
		
		if(getSubStr(%line, 0, 1) $= "[")
		{
			for(%a = 1; %a <= %this.sectionCount; %a++)
			{
				if(strLwr(%this.sectionName[%a]) $= strLwr(getSubStr(%line, 1, strLen(%line) - 2)))
				{
					%continue = true;
					
					%section = %this.section[%a];
					
					break;
				}
			}
			
			if(%continue)
			{
				%continue = false;
				
				continue;
			}
			
			%this.section[%this.sectionCount++] = new scriptObject();
			%this.sectionName[%this.sectionCount] = getSubStr(%line, 1, strLen(%line) - 2);
			
			%section = %this.section[%this.sectionCount];
			
			%section.lineCount = 0;
			
			continue;
		}
		
		%section.lineCount++;
		%section.line[%section.lineCount] = %line;
	}
	
	%file.close();
	%file.delete();
	
	return true;
}

function Hellen::displayHelp(%this, %client, %section)
{
	for(%a = 1; %a <= %this.sectionCount; %a++)
	{
		if(strLwr(%this.sectionName[%a]) $= strLwr(%section))
		{
			%section = %this.section[%a];
			
			break;
		}
	}
	
	if(!isObject(%section) || !isObject(%client))
	{
		return false;
	}
	
	for(%b = 1; %b <= %section.lineCount; %b++)
	{
		eval("messageClient(" @ %client @ ", '', \"" @ %section.line[%b] @ "\");");
	}
}

function serverCmdhelp(%client, %section, %term)
{	
	if(!isObject(CityRPGHelp))
	{
		echo("CityRPG :: Creating new Hellen on Request.");
		new scriptObject(CityRPGHelp)
		{
			class = Hellen;
		};
	}
	
	if(%section $= "")
	{
		CityRPGHelp.displayHelp(%client, "main");
		
		if(%client.isAdmin)
			CityRPGHelp.displayHelp(%client, "main_adminline");
	}
	else
	{
		if(%term)
			messageClient(%client, '', "\c6You must specify a term. Usage: \c3/help [section] [term]\c6.");
		else
		{
			%query = %section @ "_" @ %term;
			
			if(%section $= "list")
			{
				if(%term $= "jobs")
				{
					messageClient(%client, '', "\c6Type \c3/job\c6 then one of the jobs below to become that job.");
					
					for(%a = 1; %a <= $jobCount; %a++)
					{
						if($jobs[%a].maxPlayers < 0)
						{
							messageClient(%client, '', "\c3" @ $jobs[%a].name SPC "\c6- Inital Investment: \c3" @ $jobs[%a].initialInvestment SPC "- \c6Pay: \c3" @ $jobs[%a].pay SPC "- \c6Required Education: \c3" @ $jobs[%a].requiredEducation);
						}
						else
						{
							messageClient(%client, '', "\c3" @ $jobs[%a].name SPC "\c6- Inital Investment: \c3" @ $jobs[%a].initialInvestment SPC "- \c6Pay: \c3" @ $jobs[%a].pay SPC "- \c6Required Education: \c3"
							@ $jobs[%a].requiredEducation SPC "- \c6 Players:" SPC getNumberOfPlayersWithJob($jobs[%a].name) @ "/" @ $jobs[%a].maxPlayers);
						}
						messageClient(%client, '', $jobs[%a].description);
					}
				}
				else if(%term $= "lots")
				{
					messageClient(%client, '', "\c6This is a list of lots you can plant.");
					
					for(%a = 0; %a < datablockGroup.getCount(); %a++)
					{
						%datablock = datablockGroup.getObject(%a);
						
						if(%datablock.CityRPGBrickType == 1)
						{
							messageClient(%client, '', "\c3" @ %datablock.uiName SPC "\c6- Size: \c3" @ %datablock.brickSizeX @ "x" @ %datablock.brickSizeY SPC "\c6Cost: \c3" @ %datablock.initialPrice SPC "\c6Tax: \c3" SPC %datablock.taxAmount);
						}
					}
				}
				else if(%term $= "items")
				{
					messageClient(%client, '', "\c6This is a list of item prices.");
					
					for(%a = 0; isObject($CityRPG::items[%a]); %a++)
					{
						messageClient(%client, '', "\c3" @ $CityRPG::items[%a].itemName.uiName SPC "\c6- \c3$" @ $CityRPG::items[%a].price);
					}
				}
				else if(%term $= "food")
				{
					messageClient(%client, '', "\c6This is a list of all food stuff.");
					for(%a = 0; isObject($CityRPG::foodPortion[%a]); %a++)
					{
						messageClient(%client, '', '\c3%1\c6 - \c3$%2', $CityRPG::foodPortion[%a], mFloor((%a) * 4));
					}
				}
				else
					messageClient(%client, '', "\c6You did not request a real list.");
			}
			else
			{
				if(!CityRPGHelp.displayHelp(%client, %query))
				{
					messageClient(%client, '', "\c6Help doc '\c3" @ %query @ "\c6' does not exist\c6!");
				}
			}
		}
	}
}