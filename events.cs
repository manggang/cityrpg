// ============================================================
// Banned Events
// ============================================================
// $CityRPG::bannedEvent[nameSpace, name] = true; results in ban
// Note: This system is archaic.

// AIPlayer
$CityRPG::bannedEvent["AIPlayer", "setPlayerScale"] = true;

// fxDTSBrick
$CityRPG::bannedEvent["fxDTSBrick", "spawnExplosion"] = true;
$CityRPG::bannedEvent["fxDTSBrick", "spawnProjectile"] = true;

// gameConnection
$CityRPG::bannedEvent["gameConnection", "bottomPrint"] = true;
$CityRPG::bannedEvent["gameConnection", "incScore"] = true;
$CityRPG::bannedEvent["gameConnection", "instantRespawn"] = true;

// Player
$CityRPG::bannedEvent["Player", "addHealth"] = true;
$CityRPG::bannedEvent["Player", "addVelocity"] = true;
$CityRPG::bannedEvent["Player", "changeDatablock"] = true;
$CityRPG::bannedEvent["Player", "clearTools"] = true;
$CityRPG::bannedEvent["Player", "kill"] = true;
$CityRPG::bannedEvent["Player", "setHealth"] = true;
$CityRPG::bannedEvent["Player", "setPlayerScale"] = true;
$CityRPG::bannedEvent["Player", "spawnExplosion"] = true;
$CityRPG::bannedEvent["Player", "spawnProjectile"] = true;
$CityRPG::bannedEvent["Player", "setVelocity"] = true;

package CityRPG_EventPackage
{	
	// ============================================================
	// Banned Events
	// ============================================================
	// fxDTSBrick Events
	function fxDTSBrick::spawnExplosion(%this, %explosion, %scale, %client)
	{
		if(!isObject(%client))
			parent::spawnExplosion(%this, %explosion, %scale, %client);
	}
	
	function fxDTSBrick::spawnProjectile(%this, %dir, %proj, %rand, %scale, %client)
	{
		if(!isObject(%client))
			parent::spawnProjectile(%this, %dir, %proj, %rand, %scale, %client);
	}
	
	// Player Events
	function Player::addHealth(%this, %aug, %client)
	{
		if(!isObject(%client))
			parent::addHealth(%this, %aug, %client);
	}
	
	function Player::addVelocity(%this, %vel, %client)
	{
		if(!isObject(%client))
			parent::addVelocity(%this, %vel, %client);
	}
	
	function Player::changeDatablock(%this, %db, %client)
	{
		if(!isObject(%client))
			parent::changeDatablock(%this, %db, %client);
	}
	
	function Player::clearTools(%this, %client)
	{
		if(!isObject(%client))
			parent::clearTools(%this, %client);
	}
	
	function Player::kill(%this, %client)
	{
		if(!isObject(%client))
			parent::kill(%this, %client);
	}
	
	function Player::setHealth(%this, %hp, %client)
	{
		if(!isObject(%client))
			parent::setHealth(%this, %hp, %client);
	}
	
	function Player::spawnExplosion(%this, %explosion, %scale, %client)
	{
		if(!isObject(%client))
			parent::spawnExplosion(%this, %explosion, %scale, %client);
	}
	
	function Player::spawnProjectile(%this, %dir, %proj, %rand, %scale, %client)
	{
		if(!isObject(%client))
			parent::spawnProjectile(%this, %dir, %proj, %rand, %scale, %client);
	}
	
	// AIPlayer Events
	function AIPlayer::setPlayerScale(%this, %scale, %client)
	{
		if(!isObject(%client))
			parent::setPlayerScale(%this, %scale, %client);
	}
	
	// gameConnection Events
	function gameConnection::incScore(%client, %aug, %eventActivator)
	{
		if(!isObject(%eventActivator))
			parent::incScore(%client, %aug, %eventActivator);
	}
	
	function gameConnection::instantRespawn(%client, %eventActivator)
	{
		if(!isObject(%eventActivator))
			parent::instantRespawn(%client, %eventActivator);
	}

	function gameConnection::ChatMsgAll(%brick,%client)
	{
		//do nothing
	}

	function gameConnection::centerPrintAll(%brick,%client)
	{
		//do nothing
	}
	
	function gameConnection::bottomPrintAll(%brick,%client)
	{
		//do nothing
	}
};
activatePackage(CityRPG_EventPackage);

function CityRPG_AssembleEvents()
{
	// Basic Input
	registerInputEvent("fxDTSBrick", "onEnterLot", "Self fxDTSBrick" TAB "Player player" TAB "Client gameConnection");
	registerInputEvent("fxDTSBrick", "onLeaveLot", "Self fxDTSBrick" TAB "Player player" TAB "Client gameConnection");
	registerInputEvent("fxDTSBrick", "onTransferSuccess", "Self fxDTSBrick" TAB "Player Player" TAB "Client GameConnection");
	registerInputEvent("fxDTSBrick", "onTransferDecline", "Self fxDTSBrick" TAB "Client GameConnection");
	registerInputEvent("fxDTSBrick", "onJobTestPass", "Self fxDTSBrick" TAB "Player Player" TAB "Client GameConnection");
	registerInputEvent("fxDTSBrick", "onJobTestFail", "Self fxDTSBrick" TAB "Player Player" TAB "Client GameConnection");
	
	// Basic Output
	registerOutputEvent("fxDTSBrick", "requestFunds", "string 80 200" TAB "int 1 9000 1");
	registerOutputEvent("GameConnection", "MessageBoxOK", "string 30 50" TAB "string 80 500");
	
	for(%b = 1; isObject($jobs[%b]); %b++)
	{
		if(strlen($jobs[%b].name) > 7)
			%jobName = getSubStr($jobs[%b].name, 0, 6) @ ".";
		else
			%jobName = $jobs[%b].name;
		
		%doJobTest_List = %doJobTest_List SPC strreplace(%jobName, " ", "") SPC %b;
	}
	registerOutputEvent("fxDTSBrick", "doJobTest", "list NONE 0" @ %doJobTest_List TAB "list NONE 0" @ %doJobTest_List TAB "bool");

	%clientCount = ClientGroup.getCount();
	for(%d = 0; %d < %clientCount; %d++)
	{
		%subClient = ClientGroup.getObject(%d);
		serverCmdRequestEventTables(%subClient);
		messageClient(%subClient, '', "\c6Your Event Tables have been updated. If you do not know what that means, ignore this message.");
	}
}
CityRPG_AssembleEvents();

function fxDTSBrick::onEnterLot(%brick, %obj)
{
	$inputTarget_self = %brick;
	
	$inputTarget_client = %obj.client;
	$inputTarget_player = %obj.client.player;
	
	$inputTarget_miniGame = (isObject(getMiniGameFromObject(%obj.client))) ? getMiniGameFromObject(%obj.client) : 0;
	
	%brick.processInputEvent("OnEnterLot", %obj.client);
}

function fxDTSBrick::onLeaveLot(%brick, %obj)
{
	$inputTarget_self = %brick;
	
	$inputTarget_client = %obj.client;
	$inputTarget_player = %obj.client.player;
	
	$inputTarget_miniGame = (isObject(getMiniGameFromObject(%obj.client))) ? getMiniGameFromObject(%obj.client) : 0;
	
	%brick.processInputEvent("OnLeaveLot", %obj.client);
}

function fxDTSBrick::onJobTestPass(%brick, %client)
{
	$inputTarget_self	= %brick;
	$inputTarget_player	= %client.player;
	$inputTarget_client	= %client;
	
	%brick.processInputEvent("onJobTestPass", %client);
}

function fxDTSBrick::onJobTestFail(%brick, %client)
{
	$inputTarget_self	= %brick;
	$inputTarget_player	= %client.player;
	$inputTarget_client	= %client;
	
	%brick.processInputEvent("onJobTestFail", %client);
}

function fxDTSBrick::doJobTest(%brick, %job, %job2, %convicts, %client)
{
	%convictStatus = %client.getDaysLeftInJail();
	
	if(!%job && !%job2 && (%convicts ? (!%convictStatus ? true : false) : true))
		%brick.onJobTestPass(%client);
	else if((CityRPGData.getData(%client.bl_id).valueJobID == %job || CityRPGData.getData(%client.bl_id).valueJobID == %job2) &&
		(%convicts ? (!%convictStatus ? true : false) : true))
		%brick.onJobTestPass(%client);
	else
		%brick.onJobTestFail(%client);
}
