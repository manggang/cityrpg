function gameConnection::getMoney(%client)
{
	return mFloor(CityRPGData.getData(%client.bl_id).valueMoney);
}

function gameConnection::getMoneyInBank(%client)
{
	return mFloor(CityRPGData.getData(%client.bl_id).valueBank);
}

function gameConnection::getCashString(%client)
{
	if(%client.getMoney() >= 0)
		%money = "\c3$" @ %client.getMoney();
	else
		%money = "\c0(-$" @ %client.getMoney() @ ")";
	
	return %money;
}

function gameConnection::addMoneyToBank(%client, %amount, %reason)
{
	%amount = mFloor(%amount);
	if (%amount >= 0)
	{
		CityRPGData.getData(%client.bl_id).valueBank += %amount;
		if (%reason !$= "")
		{
			messageClient(%client, '', '\c6Bank: \c2$%1\c6 has been deposited into your account. Reason: %2', %amount, %reason);
		}
		return true;
	}
	return false;
}

function addMoneyToBank(%blID, %amount, %reason)
{
	%amount = mFloor(%amount);
	if (%amount >= 0)
	{
		CityRPGData.getData(%blID).valueBank += %amount;
		if (%reason !$= "" && findClientByBL_ID(%blID))
		{
			messageClient(findClientByBL_ID(%blID), '', '\c6Bank: \c2$%1\c6 has been deposited into your account. Reason: %2', %amount, %reason);
		}
		return true;
	}
	return false;
}

function gameConnection::subtractMoneyFromBank(%client, %amount, %reason)
{
	%amount = mFloor(%amount);
	if (%amount >= 0)
	{
		CityRPGData.getData(%client.bl_id).valueBank -= %amount;
		if (%reason !$= "")
		{
			messageClient(%client, '', '\c6Bank: \c2$%1\c6 has been withdrawn from your account. Reason: %2', %amount, %reason);
		}
		return true;
	}
	return false;
}

function subtractMoneyFromBank(%blID, %amount, %reason)
{
	%amount = mFloor(%amount);
	if (%amount >= 0)
	{
		CityRPGData.getData(%blID).valueBank -= %amount;
		if (%reason !$= "" && findClientByBL_ID(%blID))
		{
			messageClient(findClientByBL_ID(%blID), '', '\c6Bank: \c2$%1\c6 has been withdrawn from your account. Reason: %2', %amount, %reason);
		}
		return true;
	}
	return false;
}

function gameConnection::transferMoney(%client, %target, %amount, %reason)
{
	%amount = mFloor(%amount);
	
	if (%client.getMoney() - %amount <= 0)
	{
		return false;
	}
	
	if (%amount < 0)
	{
		return false;
	}

	%client.subtractMoneyFromBank(%amount, %reason);
	%amount = CitySO::takeSalesTax(%amount);
	%target.addMoneyToBank(%amount, %reason);
	return true;
}

function transferMoney(%sourceID, %targetID, %amount, %reason)
{
	if (CityRPGData.getData(%sourceID).valueMoney - %amount < 0)
	{
		return false;
	}
	
	if (%amount < 0)
	{
		return false;
	}
	
	%amount = mFloor(%amount);
	if (%amount > 0)
	{
		subtractMoneyFromBank(%sourceID, %amount, %reason);
		%amount = CitySO::takeSalesTax(%amount);
		addMoneyToBank(%targetID, %amount, %reason);
	}
	return true;
}

function gameConnection::setMoneyInBank(%client, %amount)
{
	CityRPGData.getData(%client.bl_id).valueBank = %amount;
}

function gameConnection::setMoney(%client, %amount)
{
	CityRPGData.getData(%client.bl_id).valueMoney = mFloor(amount);
	%client.updateHUD();
}

function gameConnection::addMoney(%client, %amount)
{
	%amount = mFloor(%amount);
	if (%amount >= 0)
	{
		CityRPGData.getData(%client.bl_id).valueMoney += %amount;
		%client.updateHUD();
		return true;
	}
	return false;
}

function gameConnection::subtractMoney(%client, %amount)
{
	%amount = mFloor(%amount);
	if (%client.getMoney() - %amount >= 0 && %amount >= 0)
	{
		CityRPGData.getData(%client.bl_id).valueMoney -= %amount;
		%client.updateHUD();
		return true;
	}
	return false;
}

function serverCmdgiveMoney(%client, %amount, %name)
{
	%amount = mFloor(%amount);
	
	if(%amount <= 0)
	{
		messageClient(%client, '', "\c6You must enter a valid amount of money to give.");
	}
	
	if(%client.getMoney() - %amount <= 0)
	{
		messageClient(%client, '', "\c6You don't have that much money to give.");
	}
	
	if(!isObject(%client.player))
	{
		messageClient(%client, '', "\c6Spawn first before you use this command.");
	}
	
	if(%name !$= "")
	{
		%target = findclientbyname(%name);
	}
   	else
	{
		%target = containerRayCast(%client.player.getEyePoint(), vectorAdd(vectorScale(vectorNormalize(%client.player.getEyeVector()), 5), %client.player.getEyePoint()), $typeMasks::playerObjectType,%client.player).client;
	}
	
	if(isObject(%target))
	{
		messageClient(%client, '', "\c6You give \c3$" @ %amount SPC "\c6to \c3" @ %target.name @ "\c6.");
		messageClient(%target, '', "\c3" @ %client.name SPC "\c6has given you \c3$" @ %amount @ "\c6.");
					
		%client.subtractMoney(%amount);
		%target.addMoney(%amount);
	}
	else
		messageClient(%client, '', "\c6You must be looking at and be in a reasonable distance of the player in order to give them money. \nYou can also type in the person's name after the amount.");
}


// Section 5.2 : Admin Commands
function serverCmdgrantMoney(%client, %money, %name)
{
	if(!%client.isAdmin)
	{
		messageClient(%client, '', "\c6You must be admin to use the this command.");
	}
	
	%money = mFloor(%money);
		
	if(%money <= 0)
	{
		messageClient(%client, '', "\c6You must enter a valid amount of money to grant.");
	}
	
	if(%name !$= "")
	{
		%target = findClientByName(%name);
		
		if(!isObject(%target))
		{
			messageClient(%client, "\c6The name you entered could not be matched up to a person.");
		}
		
		if(%target != %client)
		{
			messageClient(%client, '', "\c6You grant \c3$" @ %money SPC "\c6to \c3" @ %target.name @ "\c6.");
			messageClient(%target, '', "\c3An admin has granted you \c3$" @ %money @ "\c6.");
		}
		else
			messageClient(%client, '', "\c6You grant yourself \c3$" @ %money @ "\c6.");
			
		%target.addMoney(%money);
	}
	else
	{
		%target = containerRayCast(%client.player.getEyePoint(), vectorAdd(vectorScale(vectorNormalize(%client.player.getEyeVector()), 5), %client.player.getEyePoint()), $typeMasks::playerObjectType).client;
		
		if(isObject(%target))
		{
			messageClient(%client, '', "\c6You grant \c3$" @ %money SPC "\c6to \c3" @ %target.name @ "\c6.");
			messageClient(%target, '', "\c3An admin has granted you \c3$" @ %money @ "\c6.");
					
			%target.addMoney(%money);
		}
	}
}

function serverCmddeductMoney(%client, %amount)
{
	if(!%client.isAdmin)
	{
		messageClient(%client, '', "\c6You must be admin to use the this command.");
	}
	
	%amount = mFloor(%amount);
		
	if(%amount <= 0)
	{
		messageClient(%client, '', "\c6You must enter a valid amount of money to take away.");
	}
	
	if(%name !$= "")
	{
		if(isObject(%target = findClientByName(%name)))
		{
			if(%target != %client)
			{
				messageClient(%client, '', "\c6You deduct \c3$" @ %amount SPC "\c6from \c3" @ %target.name @ "\c6.");
				messageClient(%target, '', "\c3" @ %client.name @ "\c6 has deducted \c3$" @ %amount @ "\c6 from you.");
			}
			else
				messageClient(%client, '', "\c6You have deducted \c3$" @ %amount @ "\c6 from yourself.");
					
			%target.subtractMoney(%amount);
			if(%target.getMoney() < 0)
			{
				%target.addMoneyToBank(%target.getMoney());
				%target.setMoney(0);
						
				if(%target.getMoneyInBank() < 0)
				{
					%target.setMoneyInBank(0);
				}
			}
		}
		else
			messageClient(%client, "\c6The name you entered could not be matched up to a person.");
	}
	else if(isObject(%client.player))
	{
		%target = containerRayCast(%client.player.getEyePoint(), vectorAdd(vectorScale(vectorNormalize(%client.player.getEyeVector()), 5), %client.player.getEyePoint()), $typeMasks::playerObjectType).client;
				
		if(isObject(%target))
		{
			if(%target != %client)
			{
				messageClient(%client, '', "\c6You deduct \c3$" @ %amount SPC "\c6from \c3" @ %target.name @ "\c6.");
				messageClient(%target, '', "\c3" @ %client.name @ "\c6 has deducted \c3$" @ %amount @ "\c6 from you.");
			}
			else
				messageClient(%client, '', "\c6You have deducted \c3$" @ %amount @ "\c6 from yourself.");
				
			%target.subtractMoney(%amount);
			if(%target.getMoney() < 0)
			{
				CityRPGData.getData(%target.bl_id).valueBank += %target.getMoney();
				%target.setMoney(0);
					
				if(CityRPGData.getData(%target.bl_id).valueBank < 0)
				{
					CityRPGData.getData(%target.bl_id).valueBank = 0;
				}
			}
		}
	}
	else
		messageClient(%client, '', "\c6Spawn first before you use this command or enter a valid player's name.");
}

// ============================================================
// Section 1 : Package Data
// ============================================================
package CityRPG_Cash
{
	// Section 1.1 : Drop Money
	function gameConnection::onDeath(%client, %killerPlayer, %killer, %damageType, %unknownA)
	{		
		if(%client.getMoney())
		{
			%cash = new Item()
			{
				datablock = cashItem;
				canPickup = false;
				value = %client.getMoney();
			};
			
			%cash.setTransform(setWord(%client.player.getTransform(), 2, getWord(%client.player.getTransform(), 2) + 2));
			%cash.setVelocity(VectorScale(%client.player.getEyeVector(), 10));
			
			MissionCleanup.add(%cash);
			%cash.setShapeName("$" @ %cash.value);
			
			%client.setMoney(0);
		}
		
		if (getSimTime() - %client.timeSinceLastDeath >= 120000)
		{	
			%hospitalBill = getRandom(300,6000);			
			if (%client.getMoneyInBank() <= %hospitalBill * 3)
			{
				if (%client.getMoneyInBank())
				{
					%newHospitalBill = %client.getMoneyInBank() / 3;
				}
				else
				{
					%newHospitalBill = 0;
				}
				
				messageClient(%client, '', "\c6Your hospital bill was \c2$" @ %hospitalBill @ "\c6 but was reduced to \c2$" @ mFloor(%newHospitalBill) @ "\c6 thanks to sliding scale fees.");
				%hospitalBill = %newHospitalBill;
			}
			
			%distributedMoney = CitySO.takeSalesTax(%hospitalBill);
			
			%medics = getNumberOfPlayersWithJob("Medic");
			if (%medics > 0)
			{
				%distributedMoney /= %medics;
				
				%clientCount = ClientGroup.getCount();
				for (%i = 1; %i < %clientCount; %i++)
				{
					if (ClientGroup.getObject(%i).getJobSO().name $= "Medic")
					{
						ClientGroup.getObject(%i).bonus += %distributedMoney;
					}
				}
			}
			
			%client.subtractMoneyFromBank(%hospitalBill, "hospital bill");
		}
	
		parent::onDeath(%client, %killerPlayer, %killer, %damageType, %unknownA);
	}
	
	// Section 1.2 : Money Pickup
	function Armor::onCollision(%this, %obj, %col, %thing, %other)
	{
		if(%col.getDatablock().getName() $= "CashItem")
		{
			if(isObject(%obj.client))
			{
				if(isObject(%col))
				{
					if(%obj.client.minigame)
						%col.minigame = %obj.client.minigame;
					
					%obj.client.addMoney(%col.value);
					messageClient(%obj.client, '', "\c6You have picked up \c3$" @ %col.value SPC "\c6off the ground.");
					%col.canPickup = false;
					%col.delete();
				}
				else
				{
					%col.delete();
					MissionCleanup.remove(%col);
				}
			}
		}
		
		if(isObject(%col))
			parent::onCollision(%this, %obj, %col, %thing, %other);
	}
	
	function CashItem::onAdd(%this, %item, %b, %c, %d, %e, %f, %g)
	{
		parent::onAdd(%this, %item, %b, %c, %d, %e, %f, %g);
		schedule($CityRPG::pref::moneyDieTime, 0, "eval", "if(isObject(" @ %item.getID() @ ")) { " @ %item.getID() @ ".delete(); }");
	}
};
activatePackage(CityRPG_Cash);

// ============================================================
// Section 2 : Money Datablock
// ============================================================
datablock ItemData(cashItem)
{
	category = "Weapon";
	className = "Weapon";
	
	shapeFile = "base/data/shapes/brickWeapon.dts";
	mass = 1;
	density = 0.2;
	elasticity = 0.2;
	friction = 0.6;
	
	doColorShift = true;
	colorShiftColor = "0 0.6 0 1";
	image = cashImage;
	candrop = true;
	canPickup = false;
};

datablock ShapeBaseImageData(cashImage)
{
	shapeFile = "base/data/shapes/brickWeapon.dts";
	
	
	doColorShift = true;
	colorShiftColor = cashItem.colorShiftColor;
	canPickup = false;
};